//
//  main.m
//  Together
//
//  Created by Talles  Borges  on 7/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
