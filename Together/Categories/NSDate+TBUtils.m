//
// Created by Talles  Borges  on 11/3/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "NSDate+TBUtils.h"


@implementation NSDate (TBUtils)
+ (instancetype)dateFromRailsString:(NSString *)dateString {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];

    return [df dateFromString:dateString];
}

@end