//
// Created by Talles  Borges  on 6/6/14.
// Copyright (c) 2014 DevMaker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (Utilidades)


+ (UIImage *)imageWithColor:(UIColor *)color;

- (NSString *)encodeToBase64String;

- (UIImage *)imageScaledToSize:(CGSize)newSize;

- (NSUInteger)sizeLengthJPG;

- (UIImage *)toJPGwithQuality:(int)quality;

- (UIImage *)compressedImage;

- (UIImage *)resizedImageWithSize:(CGSize)size;

- (UIImage *)makeRoundCornersWithRadius:(const CGFloat)RADIUS;
@end