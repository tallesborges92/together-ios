//
// Created by Talles  Borges  on 9/29/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UIAlertView+Utils.h"


@implementation UIAlertView (Utils)

+ (void)showAlertWithString:(NSString *)string {
    UIAlertView *alertView = [UIAlertView new];
    alertView.title = @"";
    alertView.message = string;
    [alertView addButtonWithTitle:@"OK"];
    [alertView show];
}

@end