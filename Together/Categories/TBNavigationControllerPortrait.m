//
// Created by Talles  Borges  on 12/4/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TBNavigationControllerPortrait.h"


@implementation TBNavigationControllerPortrait {

}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end