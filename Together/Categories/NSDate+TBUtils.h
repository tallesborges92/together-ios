//
// Created by Talles  Borges  on 11/3/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TBUtils)

+ (instancetype)dateFromRailsString:(NSString *)dateString;

@end