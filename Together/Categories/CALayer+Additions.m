//
// Created by Talles  Borges  on 10/21/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "CALayer+Additions.h"


@implementation CALayer (Additions)

-(void)setBorderIBColor:(UIColor*)color
{
    self.borderColor = color.CGColor;
}

-(UIColor*)borderIBColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

-(void)setShadowIBColor:(UIColor*)color
{
    self.shadowColor = color.CGColor;
}

-(UIColor*)shadowIBColor
{
    return [UIColor colorWithCGColor:self.shadowColor];
}


@end