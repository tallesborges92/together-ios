//
// Created by Talles  Borges  on 12/7/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UIButton+TBButtonFont.h"


@implementation UIButton (TBButtonFont)

-(void)setTitlePacifico:(NSString *)text {
    [self setAttributedTitle:[[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Pacifico" size:22]}]forState:UIControlStateNormal];
}

@end