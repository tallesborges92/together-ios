//
// Created by Talles  Borges  on 12/7/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIButton (TBButtonFont)
- (void)setTitlePacifico:(NSString *)text;
@end