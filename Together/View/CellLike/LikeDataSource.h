//
// Created by Talles  Borges  on 9/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LikeDataSource : NSObject <UITableViewDataSource>

@property NSArray *array;
@property UITableView *tableView;

- (instancetype)initWithArray:(NSArray *)array tableView:(UITableView *)tableView;

+ (instancetype)sourceWithArray:(NSArray *)array tableView:(UITableView *)tableView;


@end