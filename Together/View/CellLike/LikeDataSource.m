//
// Created by Talles  Borges  on 9/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "LikeDataSource.h"
#import "LikeTableViewCell.h"
#import "Like.h"


@implementation LikeDataSource {
}

- (instancetype)initWithArray:(NSArray *)array tableView:(UITableView *)tableView {
    self = [super init];
    if (self) {
        self.array = array;
        self.tableView = tableView;
        self.tableView.dataSource = self;
    }

    return self;
}

+ (instancetype)sourceWithArray:(NSArray *)array tableView:(UITableView *)tableView {
    return [[self alloc] initWithArray:array tableView:tableView];
}

#pragma mark - DataSouce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    LikeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    Like *like = _array[indexPath.row];

    cell.userNameLabel.text = like.user.name;
    [cell.userAvatarImageView sd_setImageWithURL:[NSURL URLWithString:like.user.avatarUrl]];

    return cell;
}


@end