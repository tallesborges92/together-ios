//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>
#import <DateTools/NSDate+DateTools.h>
#import "CommentDataSource.h"
#import "Comment.h"
#import "CommentTableViewCell.h"


@implementation CommentDataSource {

}

- (instancetype)initWithTableView:(UITableView *)tableView array:(NSArray *)array {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        self.tableView.dataSource = self;
        self.array = array;
    }

    return self;
}

+ (instancetype)sourceWithTableView:(UITableView *)tableView array:(NSArray *)array {
    return [[self alloc] initWithTableView:tableView array:array];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    Comment *comment = _array[indexPath.row];

    cell.commentLabel.text = comment.text;
    [cell.userAvatarImageView.layer setCornerRadius:15];
    [cell.userAvatarImageView.layer setMasksToBounds:YES];
    [cell.userAvatarImageView setImageWithURL:[NSURL URLWithString:comment.user.avatarUrl]];
    cell.userNameLabel.text = comment.user.name;
    cell.commentDateLabel.text = [comment.creationDate timeAgoSinceDate:comment.serverTime];

    return cell;
}

@end