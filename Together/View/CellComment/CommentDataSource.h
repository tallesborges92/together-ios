//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CommentDataSource : NSObject <UITableViewDataSource>

@property UITableView *tableView;
@property NSArray *array;

- (instancetype)initWithTableView:(UITableView *)tableView array:(NSArray *)array;

+ (instancetype)sourceWithTableView:(UITableView *)tableView array:(NSArray *)array;


@end