//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <UIImageEffects/UIImage+ImageEffects.h>
#import "CollectionViewCellUpload+Util.h"
#import "Scene.h"


@implementation CollectionViewCellUpload (Util)

- (void)configureWithUpload:(Upload *)upload {

    [self.imageViewUpload sd_setImageWithURL:[NSURL URLWithString:upload.scene.imageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.imageViewUpload setImage:[image applyDarkEffect]];
    }];
    [self.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:upload.user.avatarUrl]];
    [self.lblUsuario setText:upload.user.name];
    if ([upload.scene.upload.objectId isEqualToNumber:upload.objectId]) {
        [self.imgViewSelected setImage:[UIImage imageNamed:@"icon-check"]];
    } else {
        [self.imgViewSelected setImage:[UIImage new]];
    }
}

@end