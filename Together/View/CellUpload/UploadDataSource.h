//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UploadDataSource : NSObject <UICollectionViewDataSource>

@property NSArray *uploads;
@property UICollectionView *collectionView;

- (instancetype)initWithUploads:(NSArray *)uploads collectionView:(UICollectionView *)collectionView;

+ (instancetype)sourceWithUploads:(NSArray *)uploads collectionView:(UICollectionView *)collectionView;


@end