//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionViewCellUpload.h"
#import "Upload.h"

@interface CollectionViewCellUpload (Util)

-(void) configureWithUpload:(Upload *)upload;


@end