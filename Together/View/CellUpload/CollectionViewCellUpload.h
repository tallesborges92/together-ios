//
//  CollectionViewCellUpload.h
//  Together
//
//  Created by Talles  Borges  on 8/2/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCellUpload : UICollectionViewCell

#pragma mark - ImageView
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUpload;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSelected;

#pragma mark - Label
@property (weak, nonatomic) IBOutlet UILabel *lblUsuario;

@end
