//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadDataSource.h"
#import "ViewControllerScene.h"


@interface UploadDelegate : NSObject <UICollectionViewDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) UIViewController *viewController;
@property UploadDataSource *dataSource;
@property (nonatomic, copy) void (^selectUpload)(Upload *upload);

- (instancetype)initWithViewController:(UIViewController *)viewController dataSource:(UploadDataSource *)dataSource;

+ (instancetype)delegateWithViewController:(UIViewController *)viewController dataSource:(UploadDataSource *)dataSource;

@end