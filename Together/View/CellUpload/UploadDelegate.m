//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "OBTabBarController.h"
#import "UploadDelegate.h"
#import "Together.h"


@implementation UploadDelegate {
    UIActionSheet *_uploadActionSheet;
}

- (instancetype)initWithViewController:(UIViewController *)viewController dataSource:(UploadDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.viewController = (ViewControllerScene *) viewController;
        self.dataSource = dataSource;
        self.dataSource.collectionView.delegate = self;
    }

    return self;
}

+ (instancetype)delegateWithViewController:(UIViewController *)viewController dataSource:(UploadDataSource *)dataSource {
    return [[self alloc] initWithViewController:viewController dataSource:dataSource];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    Upload *upload = _dataSource.uploads[indexPath.row];

    BOOL isCreator = [[Together instance].user.objectId isEqualToNumber:upload.scene.project.user.objectId];

    if (isCreator) {
        _uploadActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:NSLocalizedString(@"play", nil), NSLocalizedString(@"select", nil), nil];
    }else {
        _uploadActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:NSLocalizedString(@"play", nil), nil];
    }

//    [_uploadActionSheet addButtonWithTitle:@"Reproduzir"];
//    if (isCreator) {
//        [_uploadActionSheet addButtonWithTitle:@"Selecionar"];
//    }
    _uploadActionSheet.tag = indexPath.row;
//
//
    [_uploadActionSheet showInView:self.viewController.view];
}

#pragma mark - ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    Upload *upload = _dataSource.uploads[actionSheet.tag];

    if (buttonIndex == 0) { // Reproduzir
        MPMoviePlayerViewController *playerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:upload.videoUrl]];
        [_viewController presentMoviePlayerViewControllerAnimated:playerViewController];
//        [_playerController setContentURL:[NSURL URLWithString:upload.videoUrl]];
//        [_playerController play];
    } else if (buttonIndex == 1 && actionSheet.numberOfButtons == 3) { // Selecionar Default
        _selectUpload(upload);
//        _scene.upload = upload;
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_viewController.view animated:YES];
//        hud.labelText = @"Salvando";
//        [_scene updateInWebfactionSuccess:^(Scene *sceneUpdated) {
//            _scene = sceneUpdated;
//            [self viewWillAppear:NO];
//            [hud hide:YES];
//        }                         failure:^{
//            [hud hide:YES];
//        }];
    }
//    else if (buttonIndex == 2) { // Download
//        if ([upload uploadFileExists]) {
//            NSLog(@"Arquivo ja baixado");
//        } else {
//            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_viewController.view animated:YES];
//            hud.labelText = @"Baixando video";
//            [upload downloadWithSuccess:^{
////                    [_tableViewUploads reloadData];
//                [hud hide:YES];
//            }                andFailure:^{
//                [hud hide:YES];
//            }];
//        }
//    }
}


@end