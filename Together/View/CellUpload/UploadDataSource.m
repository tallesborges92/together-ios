//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UploadDataSource.h"
#import "CollectionViewCellUpload.h"
#import "Upload.h"
#import "CollectionViewCellUpload+Util.h"


@implementation UploadDataSource {

}

- (instancetype)initWithUploads:(NSArray *)uploads collectionView:(UICollectionView *)collectionView {
    self = [super init];
    if (self) {
        self.uploads = uploads;
        self.collectionView = collectionView;
        collectionView.dataSource = self;

        UINib *cellUpload = [UINib nibWithNibName:@"CollectionViewCellUpload" bundle:nil];
        [_collectionView registerNib:cellUpload forCellWithReuseIdentifier:@"cellUpload"];
    }

    return self;
}

+ (instancetype)sourceWithUploads:(NSArray *)uploads collectionView:(UICollectionView *)collectionView {
    return [[self alloc] initWithUploads:uploads collectionView:collectionView];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _uploads.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Upload *upload = _uploads[indexPath.row];

    CollectionViewCellUpload *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellUpload" forIndexPath:indexPath];

    [cell configureWithUpload:upload];

    return cell;
}


@end