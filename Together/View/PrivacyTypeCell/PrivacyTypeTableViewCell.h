//
//  PrivacyTypeTableViewCell.h
//  Together
//
//  Created by Talles  Borges  on 12/6/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyTypeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;

@end
