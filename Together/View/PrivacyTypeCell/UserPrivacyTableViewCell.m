//
//  UserPrivacyTableViewCell.m
//  Together
//
//  Created by Talles  Borges  on 12/6/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UserPrivacyTableViewCell.h"

@implementation UserPrivacyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        self.selectedImage.image = [UIImage imageNamed:@"privacy_icon_selected"];
    } else {
        self.selectedImage.image = [UIImage imageNamed:@"privacy_icon_unselected"];
    }

}

@end
