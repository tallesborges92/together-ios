//
//  TableViewCellProfileBar.h
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellProfileBar : UITableViewCell

@property (strong, nonatomic) IBOutlet UITabBar *tabBar;

@end
