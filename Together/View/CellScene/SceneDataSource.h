//
//  SceneDataSource.h
//  Together
//
//  Created by Talles  Borges  on 7/28/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SceneDataSource : NSObject <UICollectionViewDataSource>

@property NSArray *scenes;
@property UICollectionView *collectionView;

- (instancetype)initWithScenes:(NSArray *)scenes collectionView:(UICollectionView *)collectionView;
+ (instancetype)sourceWithScenes:(NSArray *)scenes collectionView:(UICollectionView *)collectionView;


@end
