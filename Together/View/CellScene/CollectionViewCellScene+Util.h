//
// Created by Talles  Borges  on 8/1/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionViewCellScene.h"
#import "Scene.h"

@interface CollectionViewCellScene (Util)


- (void)configureWithScene:(Scene *)scene;
@end