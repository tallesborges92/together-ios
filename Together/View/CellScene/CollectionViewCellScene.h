//
//  CollectionViewCellScene.h
//  Together
//
//  Created by Talles  Borges  on 7/28/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCellScene : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblSceneName;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewScene;

@end
