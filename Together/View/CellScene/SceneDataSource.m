//
//  SceneDataSource.m
//  Together
//
//  Created by Talles  Borges  on 7/28/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "SceneDataSource.h"
#import "CollectionViewCellScene.h"
#import "Scene.h"
#import "CollectionViewCellScene+Util.h"

@implementation SceneDataSource

- (instancetype)initWithScenes:(NSArray *)scenes collectionView:(UICollectionView *)collectionView {
    self = [super init];
    if (self) {
        self.scenes = scenes;
        self.collectionView = collectionView;
        _collectionView.dataSource = self;

        UINib *cellScene = [UINib nibWithNibName:@"CollectionViewCellScene" bundle:nil];
        [_collectionView registerNib:cellScene forCellWithReuseIdentifier:@"cellScene"];
    }

    return self;
}

+ (instancetype)sourceWithScenes:(NSArray *)scenes collectionView:(UICollectionView *)collectionView {
    return [[self alloc] initWithScenes:scenes collectionView:collectionView];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _scenes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Scene *scene = _scenes[indexPath.row];

    CollectionViewCellScene *cellScene = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellScene" forIndexPath:indexPath];

    [cellScene configureWithScene:scene];

    return cellScene;
}


@end
