//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "SceneDelegate.h"


@implementation SceneDelegate {

}

- (instancetype)initWithViewController:(UIViewController *)viewController dataSource:(SceneDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.viewController = (ViewControllerScene *) viewController;
        self.dataSource = dataSource;
    }

    return self;
}

+ (instancetype)delegateWithViewController:(UIViewController *)viewController dataSource:(SceneDataSource *)dataSource {
    return [[self alloc] initWithViewController:viewController dataSource:dataSource];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    Scene *scene = _dataSource.scenes[indexPath.row];

    ViewControllerScene *vc = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"viewControllerScene"];
    vc.scene = scene;

    [_viewController.navigationController pushViewController:vc animated:YES];
}


@end