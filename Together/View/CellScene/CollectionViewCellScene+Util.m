//
// Created by Talles  Borges  on 8/1/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "CollectionViewCellScene+Util.h"
#import "UIImageView+WebCache.h"


@implementation CollectionViewCellScene (Util)

-(void) configureWithScene:(Scene *)scene {
    if ([scene.imageUrl isEqualToString:@""]) {
        [self.imgViewScene setImage:[UIImage imageNamed:@"imgCena"]];
    } else {
        [self.imgViewScene sd_setImageWithURL:[NSURL URLWithString:scene.imageUrl]];
    }
    self.lblSceneName.text = scene.name;
}
@end