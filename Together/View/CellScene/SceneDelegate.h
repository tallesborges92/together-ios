//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SceneDataSource.h"
#import "ViewControllerScene.h"


@interface SceneDelegate : NSObject <UICollectionViewDelegate>

@property (weak, nonatomic) ViewControllerScene *viewController;
@property SceneDataSource *dataSource;

- (instancetype)initWithViewController:(UIViewController *)viewController dataSource:(SceneDataSource *)dataSource;

+ (instancetype)delegateWithViewController:(UIViewController *)viewController dataSource:(SceneDataSource *)dataSource;


@end