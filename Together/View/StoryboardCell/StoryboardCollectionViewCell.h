//
//  StoryboardCollectionViewCell.h
//  Together
//
//  Created by Talles  Borges  on 12/4/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryboardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *storyboardImage;

@end
