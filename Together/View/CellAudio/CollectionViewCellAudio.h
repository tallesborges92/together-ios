//
//  CollectionViewCellAudio.h
//  Together
//
//  Created by Talles  Borges  on 7/28/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCellAudio : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblAudio;

@end
