//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <DateTools/NSDate+DateTools.h>
#import "TableViewCellProject+Util.h"
#import "UIImageView+AFNetworking.h"


@implementation TableViewCellProject (Util)

+ (CGFloat)defaultHeightCell {
    return 395;
}

-(void) configureWithProject:(Project *)project {
    self.project = project;

    [self.buttonPlay setHidden:NO];
    [self.imageThumb setHidden:NO];
    [self.playerController pause];

    [self.lblNomeProjeto setText:project.name];
    [self.lblDescricaoProjeto setText:project.descriptionText];
    [self.lblNomeUsuario setText:project.user.name];
    [self.imgViewAvatarUsuario setImageWithURL:[NSURL URLWithString:project.user.avatarUrl]];
    [self.imageThumb setImageWithURL:[NSURL URLWithString:project.imageUrl] placeholderImage:[UIImage imageNamed:@"bg_project_video"]];
    self.buttonPlay.hidden = [self.project.videoUrl isEqualToString:@""];
    [self.buttonPlay addTarget:self action:@selector(playProject) forControlEvents:UIControlEventTouchUpInside];

    [self.commentsButton setTitle:[NSString stringWithFormat:@"%@ %@",project.totalComments, NSLocalizedString(@"COMMENTS", nil)] forState:UIControlStateNormal];
    [self.totalLikesButton setTitle:[NSString stringWithFormat:@"%@ %@",project.totalLikes, NSLocalizedString(@"likes", nil)] forState:UIControlStateNormal];
    if (project.liked) {
        [self.likesButton setBackgroundImage:[UIImage imageNamed:@"btn_liked"] forState:UIControlStateNormal];
        [self.likesButton setTitleColor:[UIColor colorWithRed:0.816f green:0.008f blue:0.106f alpha:1.00f] forState:UIControlStateNormal];
        [self.likesButton setTitle:NSLocalizedString(@"unlike", nil) forState:UIControlStateNormal];
    } else {
        [self.likesButton setTitle:NSLocalizedString(@"like", nil) forState:UIControlStateNormal];
        [self.likesButton setBackgroundImage:[UIImage imageNamed:@"btn_like"] forState:UIControlStateNormal];
        [self.likesButton setTitleColor:[UIColor colorWithRed:0.329f green:0.412f blue:0.475f alpha:1.00f] forState:UIControlStateNormal];
    }

    self.creationDateLabel.text = [project.creationDate timeAgoSinceDate:project.serverTime];

    [self.scenesCollectionView reloadData];
}

- (void)playProject {
    [self.buttonPlay setHidden:YES];
    [self.imageThumb setHidden:YES];

    [self.playerController setContentURL:[NSURL URLWithString:self.project.videoUrl]];
    [self.playerController play];

    NSLog(@"_project = %@", self.project);
}

@end