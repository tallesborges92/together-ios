//
//  ProjectSceneCollectionViewCell.h
//  Together
//
//  Created by Talles  Borges  on 9/29/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectSceneCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *sceneImageView;

@end
