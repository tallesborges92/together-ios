//
//  ProjectSceneCollectionViewCell.m
//  Together
//
//  Created by Talles  Borges  on 9/29/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ProjectSceneCollectionViewCell.h"

@implementation ProjectSceneCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _sceneImageView.layer.masksToBounds = YES;
    _sceneImageView.layer.cornerRadius = 10;
    _sceneImageView.layer.borderColor = [UIColor redColor].CGColor;
    _sceneImageView.layer.borderWidth = 0.5;
}


@end
