//
//  TableViewCellProject.h
//  Together
//
//  Created by Talles  Borges  on 7/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Project.h"


@protocol TableViewCellProjectDelegate <NSObject>

@optional
-(void) updateProject:(Project *)project;
-(UIViewController *) viewControllerResponse;

@end

@interface TableViewCellProject : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate, UIActionSheetDelegate, UIDocumentInteractionControllerDelegate>

#pragma mark - Model
@property (weak, nonatomic) Project *project;
@property (weak, nonatomic) id <TableViewCellProjectDelegate> delegate;
@property (weak, nonatomic) UIViewController *viewController;

#pragma mark - Buttons
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;


#pragma mark - View
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAvatarUsuario;
@property (weak, nonatomic) IBOutlet UIView *viewPlayer;
@property (weak, nonatomic) IBOutlet UIView *projectBackgroundView;

#pragma mark - Label
@property (weak, nonatomic) IBOutlet UILabel *lblNomeUsuario;
@property (weak, nonatomic) IBOutlet UILabel *lblNomeProjeto;
@property (weak, nonatomic) IBOutlet UILabel *lblDescricaoProjeto;
@property (weak, nonatomic) IBOutlet UILabel *creationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;


#pragma mark - Scenes
@property (weak, nonatomic) IBOutlet UICollectionView *scenesCollectionView;

#pragma mark - UIImage
@property (weak, nonatomic) IBOutlet UIImageView *imageThumb;

#pragma mark - UIButton
@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;
@property (weak, nonatomic) IBOutlet UIButton *totalLikesButton;


#pragma mark - Player
@property (nonatomic, strong) MPMoviePlayerController *playerController;

#pragma mark - Actions

- (IBAction)commentsPressed:(id)sender;
- (IBAction)likePressed:(id)sender;
- (IBAction)projectPressed:(id)sender;
- (IBAction)actionPressed:(id)sender;
- (IBAction)userPressed:(id)sender;
- (IBAction)totalLikesPressed:(id)sender;
- (IBAction)scenePressed:(id)sender;




@end
