//
//  TableViewCellProject.m
//  Together
//
//  Created by Talles  Borges  on 7/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "TableViewCellProject.h"
#import "ViewControllerComment.h"
#import "Project+Webfaction.h"
#import "Together.h"
#import "ViewControllerProject.h"
#import "ProjectSceneCollectionViewCell.h"
#import "Scene.h"
#import "UIImageView+WebCache.h"
#import "UIAlertView+Utils.h"
#import "User+WebService.h"
#import "TableViewControllerProfile.h"
#import "UsersTableViewController.h"
#import "TBNavigationControllerPortrait.h"
#import "Project+Downloader.h"
#import "Project+FileUtils.h"

@implementation TableViewCellProject {
    UIDocumentInteractionController *_interactionController;
}

- (void)awakeFromNib
{
    // Initialization code
    _playerController = [[MPMoviePlayerController alloc] init];

    [_playerController.view setFrame:_viewPlayer.bounds];
    [_playerController setControlStyle:MPMovieControlStyleEmbedded];
    [_viewPlayer addSubview:_playerController.view];

    [_imgViewAvatarUsuario.layer setMasksToBounds:YES];
    [_imgViewAvatarUsuario.layer setCornerRadius:10];
//    [_imgViewAvatarUsuario.layer setBorderWidth:1];
//    [_imgViewAvatarUsuario.layer setBorderColor:[UIColor whiteColor].CGColor];

    [_projectBackgroundView.layer setBorderColor:[UIColor colorWithRed:199.0/255.0 green:199/255.0 blue:199/255.0 alpha:1].CGColor];
    [_projectBackgroundView.layer setBorderWidth:0.5];

    UINib *cellScene = [UINib nibWithNibName:@"ProjectSceneCollectionViewCell" bundle:nil];
    [_scenesCollectionView registerNib:cellScene forCellWithReuseIdentifier:@"cellScene"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (IBAction)commentsPressed:(id)sender {

    ViewControllerComment *vc = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"commentVC"];
    vc.project = _project;

    [[Together instance].obTabBarController presentViewController:vc animated:YES completion:nil];
//    [_viewController.navigationController pushViewController:vc animated:YES];
}

- (IBAction)likePressed:(id)sender {
    User *user = [Together instance].user;

    if (!user) {
        [UIAlertView showAlertWithString:@"Precisa estar logado."];
        return;
    }

    if ([[Together instance] isUserDemo]) {
        [[Together instance] showAlertNeedRegister];
        return;
    }

    [user likeProject:_project success:^{
        [self.delegate updateProject:_project];
    } failure:^{

    }];
}

- (IBAction)projectPressed:(id)sender {
    [self showProject];

}

- (void)showProject {
    Project *project = _project;

    ViewControllerProject *vc = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
    vc.project = project;

    TBNavigationControllerPortrait *navigationController = [[TBNavigationControllerPortrait alloc] initWithRootViewController:vc];

    [[Together instance].obTabBarController presentViewController:navigationController animated:YES completion:nil];
}

- (IBAction)actionPressed:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         destructiveButtonTitle:NSLocalizedString(@"inappropriate", nil)
                                              otherButtonTitles:NSLocalizedString(@"share", nil),nil];
    [sheet showInView:[Together instance].obTabBarController.view];
}

- (IBAction)userPressed:(id)sender {
    User *user = _project.user;


    TableViewControllerProfile *vc = [self.viewController.storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
    vc.user = user;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

- (IBAction)totalLikesPressed:(id)sender {

    [_project likesWithSucces:^(NSArray *likes) {
        UsersTableViewController *vc = [self.viewController.storyboard instantiateViewControllerWithIdentifier:@"usersVC"];
        vc.users = likes;
        [vc.navigationItem setTitle:[NSString stringWithFormat:@"%u %@",likes.count, NSLocalizedString(@"likes", nil)]];

        [self.viewController.navigationController pushViewController:vc animated:YES];
    } failure:^{

    }];

}

- (IBAction)scenePressed:(id)sender {
    [self showProject];
}

#pragma mark - CollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _project.scenes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Scene *scene = _project.scenes[indexPath.row];

    ProjectSceneCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellScene" forIndexPath:indexPath];
    if (scene.upload.user.avatarUrl) {
        [cell.sceneImageView sd_setImageWithURL:[NSURL URLWithString:scene.upload.user.avatarUrl]];
    } else {
        [cell.sceneImageView setImage:[UIImage imageNamed:@"icon_plus_40x40"]];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self showProject];
}

#pragma mark - ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    switch (buttonIndex) {
        case 0:{
            break;
        }
        case 1: {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[Together instance].obTabBarController.view animated:YES];
            hud.labelText = NSLocalizedString(@"downloading", nil);
            if ([_project deleteLocalVideoFile]) {
                [_project downloadWithSuccess:^{
                    [hud hide:YES];
                    [self showShare];
                } andFailure:^{
                    [hud hide:YES];
                    [self showShare];
                }];
            }
//            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[Together instance].obTabBarController.view animated:YES];
//            hud.labelText = NSLocalizedString(@"downloading", nil);
//            [_project downloadAndSaveLibraryWithSuccess:^{
//                [UIAlertView showAlertWithString:NSLocalizedString(@"saved", nil)];
//                [hud hide:YES];
//            }                                andFailure:^{
//                [hud hide:YES];
//            }];
//            break;
        }
        default: break;
    }
}

- (void)showShare {
    [[Together instance] showShareWithProject:_project];
}




@end
