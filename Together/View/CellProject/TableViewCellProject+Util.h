//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableViewCellProject.h"

@interface TableViewCellProject (Util)

+ (CGFloat)defaultHeightCell;

- (void)configureWithProject:(Project *)project;

@end