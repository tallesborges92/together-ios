//
// Created by Talles  Borges  on 9/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"


@interface UserDelegate : NSObject <UITableViewDelegate>

@property UITableView *tableView;
@property (nonatomic, copy) void (^ didSelectUser)(User *);

- (instancetype)initWithTableView:(UITableView *)tableView didSelectUser:(void (^)(User *))didSelectUser;

+ (instancetype)delegateWithTableView:(UITableView *)tableView didSelectUser:(void (^)(User *))didSelectUser;


@end