//
// Created by Talles  Borges  on 9/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UserDelegate.h"
#import "UserDataSource.h"


@implementation UserDelegate {

}


- (instancetype)initWithTableView:(UITableView *)tableView didSelectUser:(void (^)(User *))didSelectUser {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        self.didSelectUser = didSelectUser;
        self.tableView.delegate = self;
    }

    return self;
}

+ (instancetype)delegateWithTableView:(UITableView *)tableView didSelectUser:(void (^)(User *))didSelectUser {
    return [[self alloc] initWithTableView:tableView didSelectUser:didSelectUser];
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    UserDataSource *dataSource = tableView.dataSource;

    User *user = dataSource.array[indexPath.row];

    _didSelectUser(user);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 61;
}
@end