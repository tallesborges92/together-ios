//
//  TableViewCellUser.m
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TableViewCellUser.h"
#import "Together.h"
#import "User+WebService.h"
#import "UIAlertView+Utils.h"

@implementation TableViewCellUser

- (void)awakeFromNib
{
    _imageViewProfile.layer.cornerRadius = 22;
    _imageViewProfile.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)followPressed:(id)sender {

    User *userLogged = [Together instance].user;

    if ([userLogged.objectId isEqualToNumber:_user.objectId]) {
        [UIAlertView showAlertWithString:@"Você não pode seguir você mesmo."];
        return;
    }

    if (!userLogged) {
        [UIAlertView showAlertWithString:@"Precisa estar logado."];
        return;
    }

    [self.delegate followUser:_user];
    [userLogged followUser:_user success:^{
        NSLog(@"seguido!");
    } failure:^{
        NSLog(@"erro!");
    }];

}
@end
