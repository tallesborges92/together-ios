//
//  TableViewCellUser.h
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"


@protocol TableViewCellUserDelegate <NSObject>

-(void) followUser:(User *)user;
-(void) configPressed;

@end

@interface TableViewCellUser : UITableViewCell

#pragma mark - Model
@property (weak, nonatomic) User *user;
@property (weak, nonatomic) id <TableViewCellUserDelegate> delegate;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UILabel *labelNome;
@property (weak, nonatomic) IBOutlet UIButton *buttonSeguir;
@property (weak, nonatomic) IBOutlet UILabel *followingLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersLabel;

#pragma mark - Actions
- (IBAction)followPressed:(id)sender;

@end
