//
// Created by Talles  Borges  on 9/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UserDataSource.h"
#import "User+CellUtils.h"


@implementation UserDataSource {

}

- (instancetype)initWithTableView:(UITableView *)tableView array:(NSArray *)array {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        self.array = [array mutableCopy];
        self.tableView.dataSource = self;

        UINib *cellUser = [UINib nibWithNibName:@"TableViewCellUser" bundle:nil];
        [self.tableView registerNib:cellUser forCellReuseIdentifier:@"cellUser"];
    }

    return self;
}

+ (instancetype)sourceWithTableView:(UITableView *)tableView array:(NSArray *)array {
    return [[self alloc] initWithTableView:tableView array:array];
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TableViewCellUser *cell = [tableView dequeueReusableCellWithIdentifier:@"cellUser"];

    User *user = _array[indexPath.row];
    [user configureUserCell:cell];

    cell.delegate = self;
    return cell;
}

- (void)followUser:(User *)user {

    for (User *u in _array) {
        if ([u.objectId isEqualToNumber:user.objectId]) {
            if (u.following) {
                u.totalFollowers = @([u.totalFollowers intValue] - 1);
            } else {
                u.totalFollowers = @([u.totalFollowers intValue] + 1);
            }
            u.following = !u.following;
            [self.tableView reloadData];
            return;
        }
    }
}

- (void)configPressed {

}


@end