//
// Created by Talles  Borges  on 9/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableViewCellUser.h"


@interface UserDataSource : NSObject <UITableViewDataSource, TableViewCellUserDelegate>

@property UITableView *tableView;
@property NSMutableArray *array;

- (instancetype)initWithTableView:(UITableView *)tableView array:(NSArray *)array;

+ (instancetype)sourceWithTableView:(UITableView *)tableView array:(NSArray *)array;


@end