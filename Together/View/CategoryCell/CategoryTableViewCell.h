//
//  CategoryTableViewCell.h
//  Together
//
//  Created by Talles  Borges  on 11/2/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryBackgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@end
