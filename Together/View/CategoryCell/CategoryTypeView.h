//
//  CategoryTypeView.h
//  Together
//
//  Created by Talles  Borges  on 11/13/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CategoryTypeViewDelegate <NSObject>

- (void)segmentChanged:(UISegmentedControl *)segmentedControl;

@end

@interface CategoryTypeView : UIView

@property (weak, nonatomic) IBOutlet UISegmentedControl *typesSegment;
@property (weak, nonatomic) id<CategoryTypeViewDelegate> delegate;
- (IBAction)typeChanged:(id)sender;

@end
