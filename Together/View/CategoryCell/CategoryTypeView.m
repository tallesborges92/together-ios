//
//  CategoryTypeView.m
//  Together
//
//  Created by Talles  Borges  on 11/13/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "CategoryTypeView.h"

@implementation CategoryTypeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)typeChanged:(id)sender {
    [self.delegate segmentChanged:_typesSegment];
}
@end
