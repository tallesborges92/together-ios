//
//  TableViewCellProfile.h
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "TableViewCellUser.h"

@interface TableViewCellProfile : UITableViewCell

#pragma mark - Model
@property User *user;
@property UIViewController *vc;
@property (weak, nonatomic) id <TableViewCellUserDelegate> delegate;

#pragma mark - Outlet
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UIButton *buttonSeguir;
@property (strong, nonatomic) IBOutlet UIImageView *imgBlurProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblNomeProfile;
@property (weak, nonatomic) IBOutlet UIButton *configButton;



@property (weak, nonatomic) IBOutlet UIButton *followedUsersButton;
@property (weak, nonatomic) IBOutlet UIButton *followersButton;


#pragma mark - Actions
- (IBAction)followedPressed:(id)sender;
- (IBAction)followersPressed:(id)sender;
- (IBAction)followPressed:(id)sender;
- (IBAction)configPressed:(id)sender;

@end
