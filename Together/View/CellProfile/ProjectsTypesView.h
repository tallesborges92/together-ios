//
//  ProjectsTypesView.h
//  Together
//
//  Created by Talles  Borges  on 11/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProjectsTypesViewDelegate  <NSObject>

@required
-(void) selectSegmentAtIndex:(NSInteger)index;

@end


@interface ProjectsTypesView : UIView


@property (nonatomic, weak) id <ProjectsTypesViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *projectsTypesSegment;
- (IBAction)projectTypeChanged:(id)sender;

@end
