//
//  TableViewCellProfile.m
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TableViewCellProfile.h"
#import "User+WebService.h"
#import "UsersTableViewController.h"
#import "Together.h"

@implementation TableViewCellProfile

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)followedPressed:(id)sender {
    [_user followedUsersWithSender:[Together instance].user success:^(NSArray *users) {
        UsersTableViewController *vc = [_vc.storyboard instantiateViewControllerWithIdentifier:@"usersVC"];
        vc.users = users;
        [vc.navigationItem setTitle:NSLocalizedString(@"following", nil)];

        [_vc.navigationController pushViewController:vc animated:YES];
    } failure:^{

    }];
}

- (IBAction)followersPressed:(id)sender {
    [_user followersWithSender:[Together instance].user success:^(NSArray *users) {
        UsersTableViewController *vc = [_vc.storyboard instantiateViewControllerWithIdentifier:@"usersVC"];
        vc.users = users;
        [vc.navigationItem setTitle:NSLocalizedString(@"followers", nil)];

        [_vc.navigationController pushViewController:vc animated:YES];
    } failure:^{

    }];

}

- (IBAction)followPressed:(id)sender {

    [[Together instance].user followUser:_user success:^{
        [self.delegate followUser:_user];
    } failure:^{

    }];
}

- (IBAction)configPressed:(id)sender {
    [self.delegate configPressed];
}
@end
