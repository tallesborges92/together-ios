//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scene.h"

@interface Scene (ImageExport)
- (void)saveImageLocal:(UIImage *)image WithSucess:(void (^)())success andFailure:(void (^)())failure;
@end