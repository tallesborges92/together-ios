//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Scene+FileUtils.h"
#import "Project+FileUtils.h"


@implementation Scene (FileUtils)


-(NSURL *)pathImageURL {
    NSURL *projectPathURL = [self.project projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"scene-%@-image.jpg",self.objectId];
    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

-(BOOL)localImageExist {
    NSString *path = [[self pathImageURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(BOOL)deleteLocalImageFile {
    if ([self localImageExist]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self pathImageURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}

@end