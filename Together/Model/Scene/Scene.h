//
// Created by Talles  Borges  on 7/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"
#import "Upload.h"

@interface Scene : NSObject

@property NSString *name;
@property NSString *descriptionText;
@property NSString *objectId;
@property NSString *imageUrl;
@property NSNumber *start;
@property NSNumber *time;
@property Project *project;
@property Upload *upload;

@end