//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scene.h"

@interface Scene (AmazonS3)
- (void)saveImageInAmazonS3WithSuccess:(void (^)(NSString *imageUrl))success andFailure:(void (^)())failure;
@end