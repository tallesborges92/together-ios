//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Scene+Dictionary.h"
#import "Upload+Dictionary.h"


@implementation Scene (Dictionary)


- (NSDictionary *)toDictionaryPOST {

    NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];

    NSDictionary *name = @{@"scene[name]" : self.name};
    NSDictionary *description = @{@"scene[description]" : self.descriptionText};
    NSDictionary *projectId = @{@"scene[project_id]" : self.project.objectId};
    NSDictionary *imageUrl = @{@"scene[image_url]" : self.imageUrl};
    NSDictionary *uploadId = @{@"scene[upload_id]" : (self.upload.objectId) ? self.upload.objectId : @""};
    NSDictionary *start = @{@"scene[start]" : self.start };
    NSDictionary *time = @{@"scene[time]" : self.time };

    [mutableDictionary addEntriesFromDictionary:name];
    [mutableDictionary addEntriesFromDictionary:description];
    [mutableDictionary addEntriesFromDictionary:projectId];
    [mutableDictionary addEntriesFromDictionary:imageUrl];
    [mutableDictionary addEntriesFromDictionary:uploadId];
    [mutableDictionary addEntriesFromDictionary:start];
    [mutableDictionary addEntriesFromDictionary:time];

    return [mutableDictionary copy];
}

+(instancetype) sceneWithDictionary:(NSDictionary *)dictionary andProject:(Project *)project{
    Scene *scene = [Scene new];

    scene.name = dictionary[@"name"];
    scene.descriptionText = dictionary[@"description"];
    scene.objectId = dictionary[@"id"];
    scene.project = project;
    scene.imageUrl = dictionary[@"image_url"];
    scene.start = dictionary[@"start"];
    scene.time = dictionary[@"time"];
    if ([dictionary[@"upload"] class] != [NSNull class]) scene.upload = [Upload uploadWithDictionary:dictionary[@"upload"] andScene:scene];

    return scene;
}


+(NSArray *) scenesWithArray:(NSArray *)array andProject:(Project *)project{
    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Scene *scene = [Scene sceneWithDictionary:dictionary andProject:project];
        [mutableArray addObject:scene];
    }
    return [mutableArray copy];
}

@end