//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Scene+AmazonS3.h"
#import "AWSS3TransferManager.h"
#import "Constants.h"
#import "BFTask.h"


@implementation Scene (AmazonS3)

-(void) saveImageInAmazonS3WithSuccess:(void (^)(NSString *imageUrl))success andFailure:(void (^)())failure {
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = UPLOADS_BUCKET;
    uploadRequest.key = [self stringKeyUpload];
    uploadRequest.contentType = @"image/png";
    uploadRequest.body = [NSURL URLWithString:self.imageUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    ShowNetworkActivityIndicator();
    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.imageUrl = [self finalImageUrlWithKey:uploadRequest.key];
            success([self finalImageUrlWithKey:uploadRequest.key]);
            HideNetworkActivityIndicator();
        } else {
            failure();
            HideNetworkActivityIndicator();
        }
        return nil;
    }];

}

-(NSString *) stringKeyUpload {
    NSURL *url = [NSURL URLWithString:self.imageUrl];
    return [NSString stringWithFormat:@"scene-%@.%@",self.objectId,[[url pathExtension] lowercaseString]];
}

-(NSString *) finalImageUrlWithKey:(NSString *)key {
    return [NSString stringWithFormat:@"%@/%@/%@", AMAZON_URL, UPLOADS_BUCKET, key];
}

@end