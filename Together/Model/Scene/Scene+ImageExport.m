//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Scene+ImageExport.h"
#import "Scene+FileUtils.h"


@implementation Scene (ImageExport)

-(void) saveImageLocal:(UIImage *)image WithSucess:(void (^)())success andFailure:(void (^)())failure {
    [self deleteLocalImageFile];
    bool save = [UIImageJPEGRepresentation(image,0) writeToURL:[self pathImageURL] atomically:YES];
    if (save) {
        self.imageUrl = [[self pathImageURL] absoluteString];
        success();
    }else{
        failure();
    }
}

@end