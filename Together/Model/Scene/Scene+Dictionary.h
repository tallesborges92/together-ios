//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scene.h"

@interface Scene (Dictionary)

+ (instancetype)sceneWithDictionary:(NSDictionary *)dictionary andProject:(Project *)project;

+ (NSArray *)scenesWithArray:(NSArray *)array andProject:(Project *)project;

- (NSDictionary *)toDictionaryPOST;
@end