//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Scene+Webfaction.h"
#import "Constants.h"
#import "Scene+Dictionary.h"
#import "Upload+Dictionary.h"


@implementation Scene (Webfaction)

- (void)saveInWebfactionSuccess:(void (^)(Scene *sceneSaved))success failure:(void (^)())failure {
    assert(self.project);
    assert(self.descriptionText);
    assert(self.name);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/scenes.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Scene *scene = [Scene sceneWithDictionary:responseObject andProject:self.project];
        success(scene);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}
- (void)deleteInWebfactionWithSuccess:(void (^)())success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager DELETE:[NSString stringWithFormat:@"%@/scenes/%@.json", URL_BASE,self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}


-(void)updateInWebfactionSuccess:(void (^)(Scene *sceneUpdated))success failure:(void (^)())failure {
    assert(self.project);
    assert(self.descriptionText);
    assert(self.name);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager PUT:[NSString stringWithFormat:@"%@/scenes/%@.json", URL_BASE, self.objectId] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Scene *scene = [Scene sceneWithDictionary:responseObject andProject:self.project];
        success(scene);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error = %@", error);
        failure();
    }];
}

-(void) uploadsWithSuccess:(void (^)(NSArray *uploads))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/scenes/%@/uploads.json", URL_BASE, self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *uploads = [Upload uploadsWithArray:responseObject andScene:self];
        success(uploads);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

@end