//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scene.h"

@interface Scene (Webfaction)


- (void)saveInWebfactionSuccess:(void (^)(Scene *sceneSaved))success failure:(void (^)())failure;

- (void)deleteInWebfactionWithSuccess:(void (^)())success failure:(void (^)())failure;

- (void)updateInWebfactionSuccess:(void (^)(Scene *sceneUpdated))success failure:(void (^)())failure;

- (void)uploadsWithSuccess:(void (^)(NSArray *uploads))success failure:(void (^)())failure;
@end