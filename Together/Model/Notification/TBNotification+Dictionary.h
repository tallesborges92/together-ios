//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBNotification.h"

@interface TBNotification (Dictionary)


+ (instancetype)notificationWithDictionary:(NSDictionary *)dictionary;


+ (NSArray *)notificationsWithArray:(NSArray *)array;
@end