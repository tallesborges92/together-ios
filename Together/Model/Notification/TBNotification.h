//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Project.h"


@interface TBNotification : NSObject

@property User *user;
@property User *sender;
@property Project *project;
@property NSString *message;
@property NSDate *creationDate;
@property NSDate *serverTime;

@end