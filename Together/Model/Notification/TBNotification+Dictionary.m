//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TBNotification+Dictionary.h"
#import "User+Dictionary.h"
#import "NSDate+TBUtils.h"
#import "Project+Dictionary.h"


@implementation TBNotification (Dictionary)

+ (instancetype)notificationWithDictionary:(NSDictionary *)dictionary {

    TBNotification *notification = [TBNotification new];

    notification.user = [User userWithDictionary:dictionary[@"user"]];
    notification.sender = [User userWithDictionary:dictionary[@"sender"]];
    notification.message = dictionary[@"message"];
    if (![dictionary[@"project"] isKindOfClass:[NSNull class]]) {
        notification.project = [Project projectWithDictionary:dictionary[@"project"]];
    } else {
        notification.project = [Project new];
    }
    notification.creationDate = [NSDate dateFromRailsString:dictionary[@"created_at"]];
    notification.serverTime =
    [NSDate dateFromRailsString:dictionary[@"server_time"]];


    return notification;
}


+(NSArray *)notificationsWithArray:(NSArray *)array {
    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        TBNotification *notification = [self notificationWithDictionary:dictionary];
        [mutableArray addObject:notification];
    }

    return [mutableArray copy];
}


@end