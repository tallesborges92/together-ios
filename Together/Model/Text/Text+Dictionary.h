//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Text.h"

@interface Text (Dictionary)

+ (instancetype)textWithDictionary:(NSDictionary *)dictionary andProject:(Project *)project;

+ (NSArray *)textsWithArray:(NSArray *)array andProject:(Project *)project;

- (NSDictionary *)toDictionaryPOST;


@end