//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"


@interface Text : NSObject

@property NSNumber *objectId;

@property NSString *text;
@property NSNumber *start;
@property NSNumber *time;
@property Project *project;

@end