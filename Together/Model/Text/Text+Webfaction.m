//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Text+Webfaction.h"
#import "Constants.h"
#import "Text+Dictionary.h"


@implementation Text (Webfaction)

- (void)saveInWebfactionWithSuccess:(void (^)(Text *textSaved))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/texts.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Text *text = [Text textWithDictionary:responseObject andProject:self.project];
        success(text);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)deleteInWebfactionWithSuccess:(void (^)())success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager DELETE:[NSString stringWithFormat:@"%@/texts/%@.json", URL_BASE,self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}
@end