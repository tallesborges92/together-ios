//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Text+Dictionary.h"


@implementation Text (Dictionary)

+ (instancetype)textWithDictionary:(NSDictionary *)dictionary andProject:(Project *)project{
    Text *text = [Text new];

    text.objectId = dictionary[@"id"];
    text.text = dictionary[@"text"];
    text.start = dictionary[@"start"];
    text.time = dictionary[@"time"];
    text.project = project;

    return text;
}

+ (NSArray *)textsWithArray:(NSArray *)array andProject:(Project *)project{
    NSMutableArray *texts = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Text *text = [Text textWithDictionary:dictionary andProject:project];
        [texts addObject:text];
    }

    return [texts copy];
}

- (NSDictionary *)toDictionaryPOST {

    NSDictionary *text = @{@"text[text]" : self.text};
    NSDictionary *project = @{@"text[project_id]" : self.project.objectId};
    NSDictionary *start = @{@"text[start]" : self.start};
    NSDictionary *time = @{@"text[time]" : self.time};

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary addEntriesFromDictionary:text];
    [dictionary addEntriesFromDictionary:project];
    [dictionary addEntriesFromDictionary:start];
    [dictionary addEntriesFromDictionary:time];

    return [dictionary copy];
}

@end