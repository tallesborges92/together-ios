//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OBTabBarController/OBTabBarController.h>
#import "User.h"
#import "Project.h"


@interface Together : NSObject <OBTabBarControllerDelegate, UIDocumentInteractionControllerDelegate, UIAlertViewDelegate>

+ (Together *)instance;

- (void)showShareWithProject:(Project *)project;

- (void)initTabBarController;

- (void)showAlertNeedRegister;

- (BOOL)isUserDemo;

+ (void)goToMainScreen;

@property User *user;
@property OBTabBarController *obTabBarController;

@end