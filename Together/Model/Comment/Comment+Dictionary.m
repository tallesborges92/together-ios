//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Comment+Dictionary.h"
#import "Project+Dictionary.h"
#import "User+Dictionary.h"
#import "NSDate+TBUtils.h"


@implementation Comment (Dictionary)


+ (instancetype)commentWithDictionary:(NSDictionary *)dictionary {

    Comment *comment = [Comment new];

    comment.text = dictionary[@"text"];
    comment.project = [Project projectWithDictionary:dictionary[@"project"]];
    comment.user = [User userWithDictionary:dictionary[@"user"]];

    comment.creationDate = [NSDate dateFromRailsString:dictionary[@"created_at"]];
    comment.serverTime = [NSDate dateFromRailsString:dictionary[@"server_time"]];

    return comment;
}

+ (NSArray *)commentsWithArray:(NSArray *)array {

    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Comment *comment = [Comment commentWithDictionary:dictionary];
        [mutableArray addObject:comment];
    }

    return [mutableArray copy] ;
}

- (NSDictionary *)toDictionaryPOST {

    NSDictionary *text = @{@"comment[text]" : self.text};
    NSDictionary *project = @{@"comment[project_id]" : self.project.objectId};
    NSDictionary *user = @{@"comment[user_id]" : self.user.objectId};

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary addEntriesFromDictionary:text];
    [dictionary addEntriesFromDictionary:project];
    [dictionary addEntriesFromDictionary:user];

    return [dictionary copy];
}

@end