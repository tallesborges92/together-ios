//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Comment+Webfaction.h"
#import "Constants.h"
#import "Comment+Dictionary.h"


@implementation Comment (Webfaction)

- (void)saveInWebfactionWithSuccess:(void (^)(Comment *commentSaved))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/comments.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Comment *comment = [Comment commentWithDictionary:responseObject];
        success(comment);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

@end