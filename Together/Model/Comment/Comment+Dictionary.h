//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Comment.h"

@interface Comment (Dictionary)


+ (instancetype)commentWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)commentsWithArray:(NSArray *)array;

- (NSDictionary *)toDictionaryPOST;

@end