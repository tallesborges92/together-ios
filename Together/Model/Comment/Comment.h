//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"


@interface Comment : NSObject

@property Project *project;
@property User *user;
@property NSString *text;


@property NSDate *creationDate;
@property NSDate *serverTime;

@end