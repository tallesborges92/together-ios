//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBCategory.h"
#import "User.h"

@interface TBCategory (WebService)
+ (void)categoriesWithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure;

-(void) doneProjectsWithSender:(User *)user type:(NSNumber *)type success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)doneProjectsPage:(NSNumber *)page WithSender:(User *)user type:(NSNumber *)type success:(void (^)(NSArray *projects))success failure:(void (^)())failure;
@end