//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TBCategory : NSObject


@property NSNumber *objectId;
@property NSString *name;
@property NSString *image_url;

@end