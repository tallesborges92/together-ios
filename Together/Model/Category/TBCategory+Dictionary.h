//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBCategory.h"

@interface TBCategory (Dictionary)

+(instancetype) categoryWithDictionary:(NSDictionary *)dictionary;


+ (NSArray *)categoriesWithArray:(NSArray *)array;
@end