//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TBCategory+Dictionary.h"


@implementation TBCategory (Dictionary)

+ (instancetype)categoryWithDictionary:(NSDictionary *)dictionary {

    TBCategory *category = [TBCategory new];

    category.name = dictionary[@"name"];
    category.image_url = dictionary[@"image_url"];
    category.objectId = dictionary[@"id"];

    return category;
}

+ (NSArray *) categoriesWithArray:(NSArray *)array {

    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        TBCategory *category = [self categoryWithDictionary:dictionary];
        [mutableArray addObject:category];
    }
    return [mutableArray copy];

}


@end