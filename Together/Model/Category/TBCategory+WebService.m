//
// Created by Talles  Borges  on 11/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TBCategory+WebService.h"
#import "AFHTTPRequestOperation.h"
#import "Constants.h"
#import "AFHTTPRequestOperationManager.h"
#import "TBCategory+Dictionary.h"
#import "Project.h"
#import "Project+Dictionary.h"


@implementation TBCategory (WebService)

+(void) categoriesWithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/categories.json", URL_BASE] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *array = [TBCategory categoriesWithArray:responseObject];
        success(array);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) doneProjectsWithSender:(User *)user type:(NSNumber *)type success:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    [self doneProjectsPage:@1 WithSender:user type:type success:success failure:failure];
}

-(void) doneProjectsPage:(NSNumber *)page WithSender:(User *)user type:(NSNumber *)type success:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};
    [manager GET:[NSString stringWithFormat:@"%@/categories/%@/projects/%@/%@.json", URL_BASE,self.objectId,type,user.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

@end