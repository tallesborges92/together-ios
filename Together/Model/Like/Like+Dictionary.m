//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Like+Dictionary.h"
#import "Project+Dictionary.h"
#import "User+Dictionary.h"


@implementation Like (Dictionary)
+ (instancetype)likeWithDictionary:(NSDictionary *)dictionary {

    Like *like = [Like new];
    like.project = [Project projectWithDictionary:dictionary[@"project"]];
    like.user = [User userWithDictionary:dictionary[@"user"]];

    return like;
}

+ (NSArray *)likesWithArray:(NSArray *)array {
    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Like *like = [Like likeWithDictionary:dictionary];
        [mutableArray addObject:like];
    }

    return mutableArray;
}

- (NSDictionary *)toDictionaryPOST {
    NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];

    NSDictionary *user = @{@"like[user_id]" : self.user.objectId};
    NSDictionary *project = @{@"like[project_id]" : self.project.objectId};

    [mutableDictionary addEntriesFromDictionary:user];
    [mutableDictionary addEntriesFromDictionary:project];

    return [mutableDictionary copy] ;
}

@end