//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Like.h"

@interface Like (Dictionary)
+ (instancetype)likeWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)likesWithArray:(NSArray *)array;

- (NSDictionary *)toDictionaryPOST;

@end