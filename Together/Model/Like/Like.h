//
// Created by Talles  Borges  on 9/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"


@interface Like : NSObject

@property Project *project;
@property User *user;

@end