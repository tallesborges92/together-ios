//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Project (Dictionary)
+ (instancetype)projectWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)projectsWithArray:(NSArray *)array;

- (NSDictionary *)toDictionaryPOST;
@end