//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Project+Webfaction.h"
#import "AFHTTPRequestOperationManager.h"
#import "Constants.h"
#import "Project+Dictionary.h"
#import "Scene.h"
#import "Scene+Dictionary.h"
#import "Text.h"
#import "Text+Dictionary.h"
#import "Audio.h"
#import "Audio+Dictionary.h"
#import "Comment.h"
#import "Comment+Dictionary.h"
#import "Like.h"
#import "Like+Dictionary.h"
#import "Together.h"
#import "User+Dictionary.h"


@implementation Project (Webfaction)

-(void) saveInWebfactionWithSuccess:(void (^)(Project *projectSaved))success failure:(void (^)())failure {
    assert(self.videoUrl);
    assert(self.name);
    assert(self.descriptionText);
    assert(self.user);
    assert(self.imageUrl);
    assert(self.category);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/projects.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Project *project = [Project projectWithDictionary:responseObject];
        success(project);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) deleteInWebServiceWithSuccess:(void (^)())success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager DELETE:[NSString stringWithFormat:@"%@/projects/%@.json", URL_BASE,self.objectId] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) updateInWebfactionWithSuccess:(void (^)(Project *projectUpdated))success failure:(void (^)())failure {
    assert(self.imageUrl);
    assert(self.videoUrl);
    assert(self.name);
    assert(self.descriptionText);
    assert(self.user);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager PUT:[NSString stringWithFormat:@"%@/projects/%@.json", URL_BASE,self.objectId] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Project *project = [Project projectWithDictionary:responseObject];
        success(project);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) playlistWithSuccess:(void (^)(NSArray *playlist))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@/playlist.json", URL_BASE, self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();

    }];
}

+(void) projectWithId:(NSNumber *)objectId success:(void (^)(Project *project))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@.json", URL_BASE, objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Project *project = [Project projectWithDictionary:responseObject];
        success(project);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) scenesWithSuccess:(void (^)(NSArray *scenes))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@/scenes.json", URL_BASE, self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *scenes = [Scene scenesWithArray:responseObject andProject:self];
        success(scenes);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();

    }];
}

- (void)textsWithSuccess:(void (^)(NSArray *texts))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@/texts.json", URL_BASE, self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *texts = [Text textsWithArray:responseObject andProject:self];
        success(texts);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();

    }];
}

- (void)audiosWithSuccess:(void (^)(NSArray *audios))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@/audios.json", URL_BASE, self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *audios = [Audio audiosWithArray:responseObject andProject:self];
        success(audios);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)commentsWithSuccess:(void (^)(NSArray *comments))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@/comments.json", URL_BASE, self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *comments = [Comment commentsWithArray:responseObject];
        success(comments);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void)likesWithSucces:(void (^)(NSArray *likes))success failure:(void (^)())failure{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects/%@/likes.json", URL_BASE, self.objectId] parameters:@{@"user_id" : [Together instance].user.objectId} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *likes = [User usersWithArray:responseObject];
        success(likes);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)likeByUser:(User *)user success:(void (^)())success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    Like *like = [Like new];
    like.project = self;
    like.user = user;

    [manager POST:[NSString stringWithFormat:@"%@/likes.json", URL_BASE] parameters:[like toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

+(void) projectsWithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/projects.json", URL_BASE] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();

    }];
}
@end