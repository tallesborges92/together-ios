//
// Created by Talles  Borges  on 12/11/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Project (Downloader)


- (void)downloadWithSuccess:(void (^)())success andFailure:(void (^)())failure;

- (void)downloadAndSaveLibraryWithSuccess:(void (^)())sucess andFailure:(void (^)())failure;
@end