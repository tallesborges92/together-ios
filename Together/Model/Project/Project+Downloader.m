//
// Created by Talles  Borges  on 12/11/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFURLSessionManager.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Project+Downloader.h"
#import "Constants.h"
#import "Project+FileUtils.h"


@implementation Project (Downloader)

- (void)downloadWithSuccess:(void (^)())success andFailure:(void (^)())failure {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:self.videoUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    ShowNetworkActivityIndicator();
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        return [self pathVideoURL];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", [filePath absoluteURL]);
        if (!error) {
            HideNetworkActivityIndicator();
            NSLog(@"Projeto baixado");
            success();
        } else {
            HideNetworkActivityIndicator();
            NSLog(@"Erro baixar projeto");
            failure();
        }
    }];
    [downloadTask resume];
}

-(void) downloadAndSaveLibraryWithSuccess:(void (^)())sucess andFailure:(void (^)())failure {
    if ([self localVideoExist]) {
        [self deleteLocalVideoFile];
    }
    [self downloadWithSuccess:^{
        ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
        [assetLibrary writeVideoAtPathToSavedPhotosAlbum:[self pathVideoURL] completionBlock:^(NSURL *assetURL, NSError *error) {
            NSLog(@"error = %@", error);
        }];
        sucess();
    }              andFailure:^{
        failure();
    }];
}

@end