//
// Created by Talles  Borges  on 8/1/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Project (ImageGenerate)
- (void)generateThumbImageAtSecond:(int)seconds withSuccess:(void (^)())success andFailure:(void (^)())failure;
@end