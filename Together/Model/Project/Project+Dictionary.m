//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Project+Dictionary.h"
#import "User+Dictionary.h"
#import "Scene.h"
#import "Scene+Dictionary.h"
#import "NSDate+TBUtils.h"
#import "TBCategory+Dictionary.h"


@implementation Project (Dictionary)

-(NSDictionary *) toDictionaryPOST {
    return @{@"project[name]" : self.name, @"project[description]" : self.descriptionText,
            @"project[user_id]" : self.user.objectId, @"project[video_url]" : self.videoUrl,
            @"project[audio_url]" : self.audioUrl, @"project[image_url]" : self.imageUrl,
            @"project[category_id]" : self.category.objectId, @"project[description_video_url]" : self.videoDescriptionUrl};
}

+(instancetype) projectWithDictionary:(NSDictionary *)dictionary {

    Project *project = [Project new];
    project.name = dictionary[@"name"];
    project.descriptionText = dictionary[@"description"];
    project.videoDescriptionUrl = dictionary[@"description_video_url"];
    project.videoUrl = dictionary[@"video_url"];
    project.audioUrl = dictionary[@"audio_url"];
    project.imageUrl = dictionary[@"image_url"];
    project.objectId = dictionary[@"id"];
    project.user = [User userWithDictionary:dictionary[@"user"]];
    project.texts = [NSArray array];
    project.category = [TBCategory categoryWithDictionary:dictionary[@"category"]];
    project.audios = [NSArray array];
    project.scenes = [Scene scenesWithArray:dictionary[@"scenes"] andProject:project];
    project.liked = [dictionary[@"liked"] boolValue];
    project.serverTime =
            [NSDate dateFromRailsString:dictionary[@"server_time"]];
    project.creationDate = [NSDate dateFromRailsString:dictionary[@"created_at"]];

    project.totalComments = dictionary[@"total_comments"];
    project.totalLikes = dictionary[@"total_likes"];
    return project;
}

+(NSArray *) projectsWithArray:(NSArray *)array {
    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Project *project = [self projectWithDictionary:dictionary];
        [mutableArray addObject:project];
    }

    return [mutableArray copy];
}
@end