//
// Created by Talles  Borges  on 9/24/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Project (MusicDownload)

- (void)downloadMusicWithSuccess:(void (^)())success andFailure:(void (^)())failure;

@end