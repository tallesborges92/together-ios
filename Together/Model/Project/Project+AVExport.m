//
// Created by Talles  Borges  on 7/23/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Project+AVExport.h"
#import "Upload.h"
#import "Upload+FileUtils.h"
#import "Project+FileUtils.h"
#import "Scene.h"
#import "Audio.h"
#import "Text.h"
#import "SCAssetExportSession.h"


@implementation Project (AVExport)


-(void)exportDescriptionVideoWithFilter:(SCFilterGroup *)filter success:(void (^)())success andFailure:(void (^)())failure {
    if([self localDescriptionVideoExist])
        [self deleteLocalDescriptionVideoFile];

    NSURL *url = [NSURL URLWithString:self.videoDescriptionUrl];

    SCAssetExportSession *exportSession = [[SCAssetExportSession alloc] initWithAsset:[AVAsset assetWithURL:url]];
    exportSession.filterGroup = filter;
    exportSession.sessionPreset = SCAssetExportSessionPreset568x320;
    exportSession.outputUrl = [self pathDescriptionVideoURL];
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.keepVideoSize = YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@"exportSession = %@", exportSession.error);
        self.videoDescriptionUrl = [[self pathDescriptionVideoURL] absoluteString];
        success();
    }];

}

-(BOOL) canExport {
    for (Scene *scene in self.scenes) {
        if (![scene.upload uploadFileExists] && scene.upload) {
            return false;
        }
    }

    return true;
}

- (void)exportMediaItem:(MPMediaItem *)mediaItem withSuccess:(void (^)(NSURL *audioUrl))success andFailure:(void (^)())failure {
    if([self localAudioExist])
        [self deleteLocalAudioFile];

    NSURL *url = [mediaItem valueForProperty:MPMediaItemPropertyAssetURL];

    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:[AVAsset assetWithURL:url] presetName:AVAssetExportPresetLowQuality];
    exportSession.shouldOptimizeForNetworkUse = YES;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.outputURL = [self pathAudioURL];

    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if ([exportSession status] == AVAssetExportSessionStatusCompleted) {
            self.audioUrl = [[self pathAudioURL] absoluteString];
            success(exportSession.outputURL);
        } else {
            failure();
        }
    }];

}


-(void) exportWithUploads:(NSArray *)uploads success:(void (^)())success failure:(void (^)())failure {
    if([self localVideoExist])
        [self deleteLocalVideoFile];

    AVMutableComposition *mutableComposition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [mutableComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
//    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(M_PI_2);
//    compositionVideoTrack.preferredTransform = rotationTransform;

    AVMutableCompositionTrack *compositionAudioVideoTrack = [mutableComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    compositionAudioVideoTrack.preferredVolume = 0.70;

    AVMutableCompositionTrack *compositionAudioTrack = [mutableComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];

    CMTime nextClipStartTime = kCMTimeZero;
    CMTime duration = kCMTimeZero;

    for (Upload *upload in uploads) {
        AVURLAsset *asset = [AVURLAsset assetWithURL:[upload uploadPathURL]];

        AVAssetTrack *assetVideoTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
        AVAssetTrack *assetAudioTrack = [asset tracksWithMediaType:AVMediaTypeAudio][0];

        CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, [asset duration]);
        [compositionVideoTrack insertTimeRange:timeRange ofTrack:assetVideoTrack atTime:nextClipStartTime error:nil];
        [compositionAudioVideoTrack insertTimeRange:timeRange ofTrack:assetAudioTrack atTime:nextClipStartTime error:nil];

        duration = CMTimeAdd(duration, timeRange.duration);
        nextClipStartTime = CMTimeAdd(nextClipStartTime, timeRange.duration);
    }

    AVURLAsset *asset = [AVURLAsset assetWithURL:[self pathAudioURL]];
    AVAssetTrack *assetAudioTrack = [asset tracksWithMediaType:AVMediaTypeAudio][0];
    CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, duration);
    [compositionAudioTrack insertTimeRange:timeRange ofTrack:assetAudioTrack atTime:kCMTimeZero error:nil];

// Create the export session with the composition and set the preset to the highest quality.
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mutableComposition presetName:AVAssetExportPresetMediumQuality];
// Set the desired output URL for the file created by the export process.

    exporter.outputURL = [self pathVideoURL];
// Set the output file type to be a QuickTime movie.
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
//    exporter.videoComposition = mutableVideoComposition;

    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exporter.status == AVAssetExportSessionStatusCompleted) {
                NSLog(@"exporter = %@", exporter);
                self.videoUrl = [[self pathVideoURL] absoluteString];
                success();
            } else {
                NSError *error = exporter.error;
                NSLog(@"error = %@", error);
                failure();
            }
        });
    }];
}

-(void) exportWithSuccess:(void (^)())success failure:(void (^)())failure {
    NSMutableArray *_assetsScenes = [NSMutableArray array];
    NSMutableArray *_assetsAudios = [NSMutableArray array];
    NSDictionary *_assetMusica;

    NSString *assetVideoURL = [[NSBundle mainBundle] pathForResource:@"EMPTY_VIDEO_16S" ofType:@"mp4"];

    AVAsset *assetVideoDefault = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:assetVideoURL] options:nil];


    // Uploads
    for (Scene *scene in self.scenes) {
        AVAsset *asset;
        if (scene.upload) {
            asset = [AVAsset assetWithURL:[scene.upload uploadPathURL] ];
        } else {
            asset = assetVideoDefault;
        }


        NSDictionary *dictionary = @{@"asset" : asset, @"object" : scene};
        [_assetsScenes addObject:dictionary];
    }

    // Audios
    for (Audio *audio in self.audios) {
        AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:audio.aurdioUrl]];
        NSDictionary *dictionary = @{@"asset" : asset, @"object" : audio};
        [_assetsAudios addObject:dictionary];
    }

    // Musica
    if (![self.audioUrl isEqualToString:@""]) {
        AVAsset *musica = [AVAsset assetWithURL:[self pathAudioURL] ];
        _assetMusica = @{@"asset" : musica, @"object" : self.audioUrl};
    }

    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *videoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *audioVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *musicaTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];

    // Uploads
    for (NSDictionary *dictionary in _assetsScenes) {
        AVAsset *asset = dictionary[@"asset"];
        Scene *scene = dictionary[@"object"];

        CMTimeRange timeRangeInAsset = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds([scene.time floatValue] , NSEC_PER_SEC));

        CMTime start = CMTimeMakeWithSeconds([scene.start floatValue], NSEC_PER_SEC);

        NSArray *mediaTypeVideos = [asset tracksWithMediaType:AVMediaTypeVideo];
        AVAssetTrack *clipVideoTrack = mediaTypeVideos[0];
        [videoTrack insertTimeRange:timeRangeInAsset ofTrack:clipVideoTrack atTime:start error:nil];

        NSArray *mediaTypeAudios = [asset tracksWithMediaType:AVMediaTypeAudio];
        if (mediaTypeAudios.count > 0) {
            AVAssetTrack *clipAudioTrack = mediaTypeAudios[0];
            [audioVideoTrack insertTimeRange:timeRangeInAsset ofTrack:clipAudioTrack atTime:start error:nil];
        }

    }
    // END
    NSString *videoURL = [[NSBundle mainBundle] pathForResource:@"568x320_END" ofType:@"mp4"];
    AVAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:videoURL] options:nil];
    NSArray *mediaTypeVideos = [asset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *clipVideoTrack = mediaTypeVideos[0];
    CMTimeRange timeRangeInAsset = CMTimeRangeMake(kCMTimeZero, clipVideoTrack.timeRange.duration);

    [videoTrack insertTimeRange:timeRangeInAsset ofTrack:clipVideoTrack atTime:composition.duration error:nil];

    // Audios
    for (NSDictionary *dictionary in _assetsAudios) {
        AVAsset *asset = dictionary[@"asset"];
        Audio *audio = dictionary[@"object"];

        NSArray *mediaTypeAudios = [asset tracksWithMediaType:AVMediaTypeAudio];
        if (mediaTypeAudios.count > 0) {
            AVAssetTrack *clipAudioTrack = mediaTypeAudios[0];

            CMTimeRange timeRangeInAsset = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds([audio.time floatValue] , NSEC_PER_SEC));

            CMTime start = CMTimeMakeWithSeconds([audio.start floatValue], NSEC_PER_SEC);

            [audioTrack insertTimeRange:timeRangeInAsset ofTrack:clipAudioTrack atTime:start error:nil];
        }
    }

    // Music
    if (_assetMusica) {
        AVAsset *asset = _assetMusica[@"asset"];

        NSArray *mediaTypeAudios = [asset tracksWithMediaType:AVMediaTypeAudio];
        if (mediaTypeAudios.count > 0) {
            AVAssetTrack *clipAudioTrack = mediaTypeAudios[0];

            CMTimeRange timeRangeInAsset = CMTimeRangeMake(kCMTimeZero, composition.duration);

            [musicaTrack insertTimeRange:timeRangeInAsset ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
        }
    }


    // Text
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoCompositionWithPropertiesOfAsset:composition];
    CGSize videoSize = composition.naturalSize;
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, videoSize.width, videoSize.height);
    videoLayer.frame = CGRectMake(0, 0, videoSize.width, videoSize.height);
    [parentLayer addSublayer:videoLayer];

    for (Text *text in self.texts) {
        CALayer *textLayer = [self watermarkLayerForSize:composition.naturalSize withString:text.text start:text.start finish:text.time];
        [parentLayer addSublayer:textLayer];
    }

    AVVideoCompositionCoreAnimationTool *animationVideoTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];

    videoComposition.animationTool = animationVideoTool;

    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, composition.duration);

    AVAssetTrack *videoTrackForLayer = [composition tracksWithMediaType:AVMediaTypeVideo][0];

    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrackForLayer];

    instruction.layerInstructions = @[layerInstruction];
    videoComposition.instructions = @[instruction];

    // Export the Project
    if([self localVideoExist])
        [self deleteLocalVideoFile];
    // Create the export session with the composition and set the preset to the highest quality.
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetMediumQuality];
    exporter.outputURL = [self pathVideoURL];
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.shouldOptimizeForNetworkUse = YES;
    exporter.videoComposition = videoComposition;

    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exporter.status == AVAssetExportSessionStatusCompleted) {
                NSLog(@"exporter = %@", exporter);
                self.videoUrl = [[self pathVideoURL] absoluteString];
                success();
            } else {
                NSError *error = exporter.error;
                NSLog(@"error = %@", error);
                failure();
            }
        });
    }];

}

- (CALayer*)watermarkLayerForSize:(CGSize)videoSize withString:(NSString *)string start:(NSNumber *)start finish:(NSNumber *)duration
{
    CALayer *_watermarkLayer = [CALayer layer];
    _watermarkLayer.frame = CGRectMake(0, 0, videoSize.width, videoSize.height);

    // Create a layer for the text of the title.
    CATextLayer *titleLayer = [CATextLayer layer];
    titleLayer.string = string;
    UIFont *font = [UIFont fontWithName:@"Avenir-Light" size:52];
    titleLayer.font = CGFontCreateWithFontName((__bridge CFStringRef)font.fontName);;
    titleLayer.fontSize = 52;
    titleLayer.opacity = ([start floatValue] == 0) ? 1.0f : 0.0f;
    titleLayer.foregroundColor = [[UIColor whiteColor] CGColor];
    titleLayer.shadowOpacity = 0.5;
    titleLayer.alignmentMode = kCAAlignmentCenter;
    titleLayer.frame = CGRectMake(0, 0, videoSize.width, videoSize.height);

    CABasicAnimation *animation1 = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation1 setDuration:0];
    [animation1 setFromValue:@0.0F];
    [animation1 setToValue:@1.0F];
    [animation1 setBeginTime:[start doubleValue]];
    [animation1 setRemovedOnCompletion:NO];
    [animation1 setFillMode:kCAFillModeForwards];
    [titleLayer addAnimation:animation1 forKey:@"animateOpacityShow"];

    CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation2 setDuration:0];
    [animation2 setFromValue:@1.0F];
    [animation2 setToValue:@0.0F];
    [animation2 setBeginTime:[start doubleValue] + [duration doubleValue]];
    [animation2 setRemovedOnCompletion:NO];
    [animation2 setFillMode:kCAFillModeForwards];
    [titleLayer addAnimation:animation2 forKey:@"animateOpacityHide"];

    // Add it to the overall layer.
    [_watermarkLayer addSublayer:titleLayer];

    return _watermarkLayer;
}


@end