//
// Created by Talles  Borges  on 7/23/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AWSiOSSDKv2/AWSS3TransferManager.h>
#import "Project+AmazonS3.h"
#import "BFTask.h"
#import "Constants.h"


@implementation Project (AmazonS3)

-(void)saveVideoDescriptionInAmazonS3WithSuccess:(void (^)())success failure:(void (^)())failure {
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = PROJECTS_BUCKET;
    uploadRequest.key = [self stringKeyVideoDescriptionUpload];
    uploadRequest.contentType = @"video/quicktime";
    uploadRequest.body = [NSURL URLWithString:self.videoDescriptionUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.videoDescriptionUrl = [self finalUploadUrlWithKey:uploadRequest.key bucket:PROJECTS_BUCKET];
            success();
        } else {
            failure();
        }
        return nil;
    }];
}

-(void)saveVideoInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure {

    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = PROJECTS_BUCKET;
    uploadRequest.key = [self stringKeyVideoUpload];
    uploadRequest.contentType = @"video/quicktime";
    uploadRequest.body = [NSURL URLWithString:self.videoUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.videoUrl = [self finalUploadUrlWithKey:uploadRequest.key bucket:PROJECTS_BUCKET];
            success();
        } else {
            failure();
        }
        return nil;
    }];
}

-(void)saveImageInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure {
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = UPLOADS_BUCKET;
    uploadRequest.key = [self stringKeyImageUpload];
    uploadRequest.contentType = @"image/png";
    uploadRequest.body = [NSURL URLWithString:self.imageUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.imageUrl = [self finalUploadUrlWithKey:uploadRequest.key bucket:UPLOADS_BUCKET];
            success();
        } else {
            failure();
        }
        return nil;
    }];
}

-(void)saveAudioInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure {
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = UPLOADS_BUCKET;
    uploadRequest.key = [self stringKeyAudioUpload];
    uploadRequest.contentType = @"audio/mp4";
    uploadRequest.body = [NSURL URLWithString:self.audioUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.audioUrl = [self finalUploadUrlWithKey:uploadRequest.key bucket:UPLOADS_BUCKET];
            success();
        } else {
            failure();
        }
        return nil;
    }];
}

-(NSString *)stringKeyVideoUpload {
    NSURL *url = [NSURL URLWithString:self.videoUrl];
    return [NSString stringWithFormat:@"project-%@_user-%@.%@",self.objectId,self.user.objectId,[[url pathExtension] lowercaseString]];
}

-(NSString *)stringKeyVideoDescriptionUpload {
    NSURL *url = [NSURL URLWithString:self.videoDescriptionUrl];
    return [NSString stringWithFormat:@"project-description-%@_user-%@.%@",self.objectId,self.user.objectId,[[url pathExtension] lowercaseString]];
}

-(NSString *)stringKeyAudioUpload {
    NSURL *url = [NSURL URLWithString:self.audioUrl];
    return [NSString stringWithFormat:@"project-%@_user-%@.%@",self.objectId,self.user.objectId,[[url pathExtension] lowercaseString]];
}

-(NSString *)stringKeyImageUpload {
    NSURL *url = [NSURL URLWithString:self.imageUrl];
    return [NSString stringWithFormat:@"project-%@_user-%@.%@",self.objectId,self.user.objectId,[[url pathExtension] lowercaseString]];
}

-(NSString *) finalUploadUrlWithKey:(NSString *)key bucket:(NSString *)bucket {
    return [NSString stringWithFormat:@"%@/%@/%@", AMAZON_URL, bucket, key];
}


@end