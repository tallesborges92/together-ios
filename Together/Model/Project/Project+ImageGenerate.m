//
// Created by Talles  Borges  on 8/1/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "Project+ImageGenerate.h"
#import "Project+FileUtils.h"


@implementation Project (ImageGenerate)

-(void)generateThumbImageAtSecond :(int) seconds withSuccess:(void (^)())success andFailure:(void (^)())failure
{
    if([self localImageExist])
        [self deleteLocalImageFile];

    AVAsset *asset = [AVAsset assetWithURL:[self pathVideoURL]];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = seconds * 1000;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    bool save = [UIImagePNGRepresentation(thumbnail) writeToURL:[self pathImageURL] atomically:YES];
    if (save) {
        self.imageUrl = [[self pathImageURL] absoluteString];
        success();
    }else{
        failure();
    }
}

@end