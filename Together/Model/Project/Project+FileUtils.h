//
// Created by Talles  Borges  on 7/22/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFURLSessionManager.h>
#import "Project.h"

@interface Project (FileUtils)


- (NSURL *)pathAudioURL;

- (NSURL *)pathVideoURL;

- (NSURL *)pathImageURL;

- (NSURL *)pathDescriptionVideoURL;

- (NSURL *)projectPathURL;

- (BOOL)localDescriptionVideoExist;

- (BOOL)localVideoExist;

- (BOOL)localAudioExist;

- (BOOL)localImageExist;

- (BOOL)deleteLocalDescriptionVideoFile;

- (BOOL)deleteLocalVideoFile;

- (BOOL)deleteLocalImageFile;

- (BOOL)deleteLocalAudioFile;

- (NSString *)projectPathComponent;
@end