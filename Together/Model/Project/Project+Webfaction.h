//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Project (Webfaction)
- (void)playlistWithSuccess:(void (^)(NSArray *playlist))success failure:(void (^)())failure;


+ (void)projectWithId:(NSNumber *)objectId success:(void (^)(Project *project))success failure:(void (^)())failure;

- (void)scenesWithSuccess:(void (^)(NSArray *scenes))success failure:(void (^)())failure;
- (void)textsWithSuccess:(void (^)(NSArray *texts))success failure:(void (^)())failure;
- (void)audiosWithSuccess:(void (^)(NSArray *audios))success failure:(void (^)())failure;
- (void)commentsWithSuccess:(void (^)(NSArray *comments))success failure:(void (^)())failure;

- (void)likesWithSucces:(void (^)(NSArray *likes))success failure:(void (^)())failure;

-(void)likeByUser:(User *)user  success:(void (^)())success failure:(void (^)())failure;

+ (void)projectsWithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)saveInWebfactionWithSuccess:(void (^)(Project *projectSaved))success failure:(void (^)())failure;

- (void)deleteInWebServiceWithSuccess:(void (^)())success failure:(void (^)())failure;

- (void)updateInWebfactionWithSuccess:(void (^)(Project *projectUpdated))success failure:(void (^)())failure;
@end