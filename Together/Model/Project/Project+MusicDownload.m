//
// Created by Talles  Borges  on 9/24/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFURLSessionManager.h>
#import "Project+MusicDownload.h"
#import "Constants.h"
#import "Project+FileUtils.h"


@implementation Project (MusicDownload)
- (void)downloadMusicWithSuccess:(void (^)())success andFailure:(void (^)())failure {

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:self.audioUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    ShowNetworkActivityIndicator();
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        return [self pathAudioURL];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"error = %@", error);
        if (!error) {
            HideNetworkActivityIndicator();
            NSLog(@"File downloaded to: %@", [filePath absoluteURL]);
            NSLog(@"Musica baixada");
            success();
        } else {
            HideNetworkActivityIndicator();
            [self deleteLocalAudioFile];
            NSLog(@"Erro baixar musica");
            failure();
        }
    }];
    [downloadTask resume];

}

@end