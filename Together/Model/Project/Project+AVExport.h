//
// Created by Talles  Borges  on 7/23/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"
#import "SCFilterGroup.h"

@interface Project (AVExport)
- (void)exportDescriptionVideoWithFilter:(SCFilterGroup *)filter success:(void (^)())success andFailure:(void (^)())failure;

- (BOOL)canExport;

- (void)exportMediaItem:(MPMediaItem *)mediaItem withSuccess:(void (^)(NSURL *audioUrl))success andFailure:(void (^)())failure;

- (void)exportWithUploads:(NSArray *)uploads success:(void (^)())success failure:(void (^)())failure;

- (void)exportWithSuccess:(void (^)())success failure:(void (^)())failure;
@end