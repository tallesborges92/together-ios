//
// Created by Talles  Borges  on 7/23/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface Project (AmazonS3)

- (void)saveVideoDescriptionInAmazonS3WithSuccess:(void (^)())success failure:(void (^)())failure;

- (void)saveVideoInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure;

- (void)saveImageInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure;

- (void)saveAudioInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure;

@end