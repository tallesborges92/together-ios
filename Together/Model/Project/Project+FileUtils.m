//
// Created by Talles  Borges  on 7/22/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Project+FileUtils.h"
#import "Constants.h"


@implementation Project (FileUtils)

-(NSURL *)pathAudioURL {
    NSURL *projectPathURL = [self projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"audio.m4a"];
    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

-(NSURL *)pathVideoURL {
    NSURL *projectPathURL = [self projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"video.mov"];
    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

-(NSURL *)pathImageURL {
    NSURL *projectPathURL = [self projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"image.png"];
    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

-(NSURL *)pathDescriptionVideoURL {
    NSURL *projectPathURL = [self projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"description.mp4"];
    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

- (NSURL *)projectPathURL {
    NSURL *projectPathURL = [[NSFileManager defaultManager] URLForDirectory:LIBRAY_PATH inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    projectPathURL = [projectPathURL URLByAppendingPathComponent:[self projectPathComponent]];

    if (![[NSFileManager defaultManager] fileExistsAtPath:[projectPathURL resourceSpecifier]])
        [[NSFileManager defaultManager] createDirectoryAtPath:[projectPathURL resourceSpecifier] withIntermediateDirectories:YES attributes:nil error:nil];

    return projectPathURL;
}

-(BOOL)localDescriptionVideoExist {
    NSString *path = [[self pathDescriptionVideoURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(BOOL)localVideoExist {
    NSString *path = [[self pathVideoURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(BOOL)localAudioExist {
    NSString *path = [[self pathAudioURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(BOOL)localImageExist {
    NSString *path = [[self pathImageURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(BOOL)deleteLocalDescriptionVideoFile {
    if ([self localDescriptionVideoExist]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self pathDescriptionVideoURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}

-(BOOL)deleteLocalVideoFile {
    if ([self localVideoExist]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self pathVideoURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}

-(BOOL)deleteLocalImageFile {
    if ([self localImageExist]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self pathImageURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}

-(BOOL)deleteLocalAudioFile {
    if ([self localAudioExist]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self pathAudioURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}

-(NSString *)projectPathComponent {
    return [NSString stringWithFormat:@"Project/%@", self.objectId];
}

@end