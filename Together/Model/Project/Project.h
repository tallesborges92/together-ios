//
// Created by Talles  Borges  on 7/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "TBCategory.h"


@interface Project : NSObject

@property NSString *name;
@property NSString *descriptionText;
@property NSString *videoUrl;
@property NSString *videoDescriptionUrl;
@property NSString *audioUrl;
@property NSNumber *objectId;
@property NSString *imageUrl;
@property TBCategory *category;
@property User *user;

#pragma mark - Counts
@property NSNumber *totalComments;
@property NSNumber *totalLikes;

#pragma mark - Liked
@property BOOL liked;

#pragma mark - Time
@property NSDate *serverTime;
@property NSDate *creationDate;

#pragma mark - To Export
@property NSArray *scenes;
@property NSArray *audios;
@property NSArray *texts;


#pragma mark - FAKE
@property NSString *privacyString;

@end