//
// Created by Talles  Borges  on 12/4/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Storyboard.h"

@interface Storyboard (Dictionary)


+ (instancetype)storyboardWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)storyboardsWithArray:(NSArray *)array;
@end