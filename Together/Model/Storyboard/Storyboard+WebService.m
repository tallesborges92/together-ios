//
// Created by Talles  Borges  on 12/4/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Storyboard+WebService.h"
#import "AFHTTPRequestOperationManager.h"
#import "Constants.h"
#import "Storyboard+Dictionary.h"


@implementation Storyboard (WebService)


+(void) storyboardsWithSuccess:(void (^)(NSArray *storyboards))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/storyboards.json", URL_BASE] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *storyboards = [Storyboard storyboardsWithArray:responseObject];
        success(storyboards);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();

    }];
}

@end