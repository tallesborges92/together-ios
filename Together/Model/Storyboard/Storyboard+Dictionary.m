//
// Created by Talles  Borges  on 12/4/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Storyboard+Dictionary.h"


@implementation Storyboard (Dictionary)

+(instancetype) storyboardWithDictionary:(NSDictionary *)dictionary {

    Storyboard *storyboard = [Storyboard new];

    storyboard.image_url = dictionary[@"image_url"];

    return storyboard;
}

+(NSArray *) storyboardsWithArray:(NSArray *)array {
    NSMutableArray *mutableArray = [NSMutableArray array];
//
    for (NSDictionary *dictionary in array) {
        Storyboard *storyboard = [self storyboardWithDictionary:dictionary];
        [mutableArray addObject:storyboard];
    }

    return [mutableArray copy];
}

@end