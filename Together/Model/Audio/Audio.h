//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"


@interface Audio : NSObject

@property NSNumber *objectId;

@property Project *project;
@property NSString *aurdioUrl;
@property NSNumber *start;
@property NSNumber *time;

@end