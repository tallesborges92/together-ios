//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Audio+Dictionary.h"


@implementation Audio (Dictionary)

+ (instancetype)audioWithDictionary:(NSDictionary *)dictionary andProject:(Project *)project {

    Audio *audio = [Audio new];
    audio.objectId = dictionary[@"id"];
    audio.aurdioUrl = dictionary[@"text"];
    audio.project = project;
    audio.start = dictionary[@"start"];
    audio.time = dictionary[@"time"];

    return audio;
}

+ (NSArray *)audiosWithArray:(NSArray *)array andProject:(Project *)project {

    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Audio *audio = [Audio audioWithDictionary:dictionary andProject:project];
        [mutableArray addObject:audio];
    }

    return [mutableArray copy];
}

- (NSDictionary *)toDictionaryPOST {

    NSDictionary *project = @{@"audio[project_id]" : self.project.objectId};
    NSDictionary *start = @{@"audio[start]" : self.start};
    NSDictionary *time = @{@"audio[time]" : self.time};
    NSDictionary *audioUrl = @{@"audio[text]" : self.aurdioUrl};

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    [dictionary addEntriesFromDictionary:project];
    [dictionary addEntriesFromDictionary:start];
    [dictionary addEntriesFromDictionary:time];
    [dictionary addEntriesFromDictionary:audioUrl];

    return dictionary;
}


@end