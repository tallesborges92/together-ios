//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Audio.h"

@interface Audio (Webfaction)

- (void)saveInWebfactionWithSuccess:(void (^)(Audio *audioSaved))success failure:(void (^)())failure;

- (void)deleteInWebfactionWithSuccess:(void (^)())success failure:(void (^)())failure;
@end