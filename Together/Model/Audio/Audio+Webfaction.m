//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Audio+Webfaction.h"
#import "Constants.h"
#import "Audio+Dictionary.h"


@implementation Audio (Webfaction)

- (void)saveInWebfactionWithSuccess:(void (^)(Audio *audioSaved))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/audios.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Audio *audio = [Audio audioWithDictionary:responseObject andProject:self.project];
        success(audio);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)deleteInWebfactionWithSuccess:(void (^)())success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager DELETE:[NSString stringWithFormat:@"%@/audios/%@.json", URL_BASE,self.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

@end