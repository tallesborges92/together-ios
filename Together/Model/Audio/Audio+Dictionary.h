//
// Created by Talles  Borges  on 9/17/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Audio.h"

@interface Audio (Dictionary)
+ (instancetype)audioWithDictionary:(NSDictionary *)dictionary andProject:(Project *)project;

+ (NSArray *)audiosWithArray:(NSArray *)array andProject:(Project *)project;

- (NSDictionary *)toDictionaryPOST;

@end