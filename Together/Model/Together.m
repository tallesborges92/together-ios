//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Together.h"
#import "AppDelegate.h"
#import "UIAlertView+Utils.h"
#import "Project.h"
#import "Project+FileUtils.h"


@implementation Together {

    UIDocumentInteractionController *_interactionController;
}
+ (Together *)instance {
    static Together *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];

        }
    }
    return _instance;
}

#pragma mark - Share
-(void)showShareWithProject:(Project *)project {
    _interactionController = [UIDocumentInteractionController interactionControllerWithURL:[project pathVideoURL]];
    _interactionController.delegate = self;
    _interactionController.UTI = @"public.movie";

    CGPoint centerPoint = [UIApplication sharedApplication].delegate.window.center;
    CGRect centerRec = CGRectMake(centerPoint.x, centerPoint.y, 0, 0);

//    [interactionController presentPreviewAnimated:YES];
//    [interactionController presentOpenInMenuFromRect:centerRec inView:[Together instance].obTabBarController.view animated:YES];
    [_interactionController presentOptionsMenuFromRect:centerRec inView:[Together instance].obTabBarController.view animated:YES];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return [Together instance].obTabBarController;
}



#pragma mark - OBTabBar

-(void)initTabBarController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UINavigationController *navigationFeed = [storyboard instantiateViewControllerWithIdentifier:@"navigationFeed"];
    UINavigationController *navigationCategory = [storyboard instantiateViewControllerWithIdentifier:@"navigationCategory"];
    UIViewController *navigationNew = [UIViewController new];
    UINavigationController *navigationPerfil = [storyboard instantiateViewControllerWithIdentifier:@"navigationPerfil"];
    UINavigationController *navigationNotification = [storyboard instantiateViewControllerWithIdentifier:@"navigationNotification"];

    NSArray *viewsControllers = @[navigationFeed, navigationCategory, navigationNew, navigationPerfil, navigationNotification];

    _obTabBarController = [[OBTabBarController alloc] initWithViewControllers:viewsControllers delegate:self];

    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    app.window.rootViewController = _obTabBarController;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnavailableInDeploymentTarget"
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                |UIRemoteNotificationTypeSound
                |UIRemoteNotificationTypeAlert) categories:nil];

        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
#pragma clang diagnostic pop


}

- (UIImage *)imageTabAtIndex:(NSUInteger)index {

    UIImage *iconFeed = [UIImage imageNamed:@"ic_tab_feed_OFF"];
    UIImage *iconCategory = [UIImage imageNamed:@"ic_tab_explorar_OFF"];
    UIImage *iconNew = [UIImage imageNamed:@"ic_tab_plus"];
    UIImage *iconPerfil = [UIImage imageNamed:@"ic_tab_perfil_OFF"];
    UIImage *iconNotification = [UIImage imageNamed:@"ic_tab_notificacao_OFF"];

    NSArray *icons = @[iconFeed,iconCategory,iconNew,iconPerfil,iconNotification];
    return icons[index];
}

- (UIImage *)highlightedImageTabAtIndex:(NSUInteger)index {

    UIImage *iconFeed = [UIImage imageNamed:@"ic_tab_feed_ON"];
    UIImage *iconCategory = [UIImage imageNamed:@"ic_tab_explorar_ON"];
    UIImage *iconNew = [UIImage imageNamed:@"ic_tab_plus"];
    UIImage *iconPerfil = [UIImage imageNamed:@"ic_tab_perfil_ON"];
    UIImage *iconNotification = [UIImage imageNamed:@"ic_tab_notificacao_ON"];

    NSArray *icons = @[iconFeed,iconCategory,iconNew,iconPerfil,iconNotification];
    return icons[index];
}


- (UIImage *)tabBarBackground {
    return [UIImage imageNamed:@"tabBar-background"];
}

- (BOOL)shouldSelectTabAtIndex:(NSUInteger)index {
    if ([self isUserDemo]) {
        if (index == 3 || index == 4) {
            return false;
        }
    }
    return index != 2;
}

- (void)didSelectedTabAtIndex:(NSUInteger)index {
    if (![Together instance].user) {
         [UIAlertView showAlertWithString:@"Pecisa estar logado"];
        return;
    }
    if (index == 2) {
        if ([self isUserDemo]) {
            [self showAlertNeedRegister];
            return;
        }
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *vc = [storyboard instantiateViewControllerWithIdentifier:@"navigationNew"];
        [[Together instance].obTabBarController presentViewController:vc animated:YES completion:nil];
    } else if (index == 3 || index == 4) {
        if ([self isUserDemo]) {
            [self showAlertNeedRegister];
        }
    }
}

- (void)showAlertNeedRegister {
    UIAlertView *alertView = [UIAlertView new];
    alertView.title = NSLocalizedString(@"register", nil);
    alertView.message = NSLocalizedString(@"need_user", nil);

    [alertView addButtonWithTitle:NSLocalizedString(@"cancel", nil)];
    [alertView addButtonWithTitle:NSLocalizedString(@"register", nil)];
    [alertView setCancelButtonIndex:0];
    [alertView setDelegate:self];

    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_logged"];
        [Together goToMainScreen];
    }
}

+ (void)goToMainScreen {
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    app.window.rootViewController = [storyboard instantiateInitialViewController];
}

- (BOOL)isUserDemo {
    return [[Together instance].user.objectId isEqualToNumber:@4];
}


@end