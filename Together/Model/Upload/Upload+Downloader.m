//
// Created by Talles  Borges  on 7/26/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFURLSessionManager.h>
#import "Upload+Downloader.h"
#import "Upload+FileUtils.h"
#import "Constants.h"


@implementation Upload (Downloader)

- (void)downloadWithSuccess:(void (^)())success andFailure:(void (^)())failure {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURL *URL = [NSURL URLWithString:self.videoUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    ShowNetworkActivityIndicator();
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        return [self uploadPathURL];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", [filePath absoluteURL]);
        if (!error) {
            HideNetworkActivityIndicator();
            NSLog(@"Upload baixado");
            success();
        } else {
            HideNetworkActivityIndicator();
            NSLog(@"Erro baixar upload");
            failure();
        }
    }];
    [downloadTask resume];
}

@end