//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Upload+AmazonS3.h"
#import "BFTask.h"
#import "AWSS3TransferManager.h"
#import "Constants.h"
#import "Scene.h"


@implementation Upload (AmazonS3)

-(void)saveImageInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure {
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = UPLOADS_BUCKET;
    uploadRequest.key = [self stringKeyImageUpload];
    uploadRequest.contentType = @"image/png";
    uploadRequest.body = [NSURL URLWithString:self.imageUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.imageUrl = [self finalUploadUrlWithKey:uploadRequest.key];
            success();
        } else {
            failure();
        }
        return nil;
    }];
}

- (void)saveInAmazonS3WithSuccess:(void (^)(NSString *uploadUrl))success failure:(void (^)())failure {

    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = UPLOADS_BUCKET;
    uploadRequest.key = [self stringKeyUpload];
    uploadRequest.contentType = @"video/quicktime";
    uploadRequest.body = [NSURL URLWithString:self.videoUrl];
    uploadRequest.storageClass = AWSS3StorageClassReducedRedundancy;

    ShowNetworkActivityIndicator();
    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            self.videoUrl = [self finalUploadUrlWithKey:uploadRequest.key];
            success([self finalUploadUrlWithKey:uploadRequest.key]);
            HideNetworkActivityIndicator();
        } else {
            failure();
            HideNetworkActivityIndicator();
        }
        return nil;
    }];
}

-(NSString *) stringKeyUpload {
    NSURL *url = [NSURL URLWithString:self.videoUrl];
    NSString *time = [[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]] stringByReplacingOccurrencesOfString:@"." withString:@"-"] ;
    return [NSString stringWithFormat:@"scene-%@_user-%@_time-%@.%@",self.scene.objectId,self.user.objectId,time,[[url pathExtension] lowercaseString]];
}

-(NSString *)stringKeyImageUpload {
    NSURL *url = [NSURL URLWithString:self.imageUrl];
    return [NSString stringWithFormat:@"scene-%@_user-%@.%@",self.scene.objectId,self.user.objectId,[[url pathExtension] lowercaseString]];
}

-(NSString *) finalUploadUrlWithKey:(NSString *)key {
    return [NSString stringWithFormat:@"%@/%@/%@", AMAZON_URL, UPLOADS_BUCKET, key];
}

@end