//
// Created by Talles  Borges  on 7/22/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Upload+FileUtils.h"
#import "Scene.h"
#import "Project+FileUtils.h"


@implementation Upload (FileUtils)


-(NSURL *)pathImageURL {
    NSURL *projectPathURL = [self.scene.project projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"upload-%@-image.png",self.objectId];
    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

-(BOOL)localImageExist {
    NSString *path = [[self pathImageURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(BOOL)deleteLocalImageFile {
    if ([self localImageExist]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self pathImageURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}


-(NSURL *)uploadPathURL {
    Project *project = self.scene.project;

    NSURL *videoUrl = [NSURL URLWithString:self.videoUrl];

    NSURL *projectPathURL = [project projectPathURL];
    NSString *pathFile = [NSString stringWithFormat:@"%@.%@",self.scene.objectId, [videoUrl pathExtension]];

    return [projectPathURL URLByAppendingPathComponent:pathFile];
}

-(BOOL)deleteLocalUpload {
    if ([self uploadFileExists]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self uploadPathURL] resourceSpecifier] error:&error];
        if (error) {
            return false;
        }
    }
    return true;
}

-(BOOL)uploadFileExists {
    NSString *path = [[self uploadPathURL] resourceSpecifier];
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(NSString *)uploadFileName {
    NSURL *url = [NSURL URLWithString:self.videoUrl];
    return [url lastPathComponent];
}




@end