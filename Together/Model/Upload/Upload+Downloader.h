//
// Created by Talles  Borges  on 7/26/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"

@interface Upload (Downloader)
- (void)downloadWithSuccess:(void (^)())success andFailure:(void (^)())failure;
@end