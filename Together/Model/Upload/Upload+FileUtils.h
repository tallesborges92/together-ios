//
// Created by Talles  Borges  on 7/22/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"

@interface Upload (FileUtils)

- (NSURL *)pathImageURL;

- (BOOL)localImageExist;

- (BOOL)deleteLocalImageFile;

- (NSURL *)uploadPathURL;

- (BOOL)deleteLocalUpload;

- (BOOL)uploadFileExists;
@end