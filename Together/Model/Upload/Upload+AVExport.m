//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "Upload+AVExport.h"
#import "Upload+FileUtils.h"
#import "SCAssetExportSession.h"


@implementation Upload (AVExport)

- (void)exportWithSuccess:(void (^)())success andFailure:(void (^)())failure {

    if([self uploadFileExists])
        [self deleteLocalUpload];

    NSURL *url = [NSURL URLWithString:self.videoUrl];

    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:[AVAsset assetWithURL:url] presetName:AVAssetExportPresetMediumQuality];
    exportSession.shouldOptimizeForNetworkUse = YES;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.outputURL = [self uploadPathURL];

    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if ([exportSession status] == AVAssetExportSessionStatusCompleted) {
            self.videoUrl = [[self uploadPathURL] absoluteString];
            success();
        } else {
            failure();
        }
    }];
}

-(void) exportWithFilter:(SCFilterGroup *)filter success:(void (^)())success andFailure:(void (^)())failure {
    if([self uploadFileExists])
        [self deleteLocalUpload];

    NSURL *url = [NSURL URLWithString:self.videoUrl];

    SCAssetExportSession *exportSession = [[SCAssetExportSession alloc] initWithAsset:[AVAsset assetWithURL:url]];
    exportSession.filterGroup = filter;
    exportSession.sessionPreset = SCAssetExportSessionPreset568x320;
    exportSession.outputUrl = [self uploadPathURL];
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.keepVideoSize = YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@"exportSession = %@", exportSession.error);
        self.videoUrl = [[self uploadPathURL] absoluteString];
        success();
    }];

}

@end