//
// Created by Talles  Borges  on 7/28/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"
#import "SCFilterGroup.h"

@interface Upload (AVExport)
- (void)exportWithSuccess:(void (^)())success andFailure:(void (^)())failure;
- (void)exportWithFilter:(SCFilterGroup *)filter success:(void (^)())success andFailure:(void (^)())failure;

@end