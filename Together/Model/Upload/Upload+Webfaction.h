//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"

@interface Upload (Webfaction)
- (void)saveInWebfactionWithSuccess:(void (^)(Upload *uploadSaved))success failure:(void (^)())failure;

- (void)updateInWebfactionWithSuccess:(void (^)(Upload *uploadSaved))success failure:(void (^)())failure;
@end