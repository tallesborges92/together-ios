//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Upload+Webfaction.h"
#import "Constants.h"
#import "Upload+Dictionary.h"
#import "Upload+AmazonS3.h"


@implementation Upload (Webfaction)


-(void) saveInWebfactionWithSuccess:(void (^)(Upload *uploadSaved))success failure:(void (^)())failure {

    assert(self.user);
    assert(self.scene);
    assert(self.videoUrl);
    assert(self.imageUrl);

    ShowNetworkActivityIndicator();
    [self saveInAmazonS3WithSuccess:^(NSString *uploadUrl) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:[NSString stringWithFormat:@"%@/uploads.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            Upload *upload = [Upload uploadWithDictionary:responseObject andScene:self.scene];
            self.objectId = upload.objectId;
            success(upload);
            HideNetworkActivityIndicator();
        }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure();
            HideNetworkActivityIndicator();
        }];
    } failure:^{
        HideNetworkActivityIndicator();
    }];
}

-(void) updateInWebfactionWithSuccess:(void (^)(Upload *uploadSaved))success failure:(void (^)())failure {

    assert(self.user);
    assert(self.scene);
    assert(self.videoUrl);
    assert(self.imageUrl);

    ShowNetworkActivityIndicator();
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager PUT:[NSString stringWithFormat:@"%@/uploads/%@.json", URL_BASE, self.objectId] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        Upload *upload = [Upload uploadWithDictionary:responseObject andScene:self.scene];
        self.objectId = upload.objectId;
        self.videoUrl = upload.videoUrl;
        self.imageUrl = upload.imageUrl;
        self.scene = upload.scene;
        self.user = upload.user;
        success(self);
        HideNetworkActivityIndicator();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
        HideNetworkActivityIndicator();
    }];
}

@end