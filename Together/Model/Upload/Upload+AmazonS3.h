//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"

@interface Upload (AmazonS3)
- (void)saveImageInAmazonS3WithSuccess:(void (^)())success andFailure:(void (^)())failure;

- (void)saveInAmazonS3WithSuccess:(void (^)(NSString *uploadUrl))success failure:(void (^)())failure;
@end