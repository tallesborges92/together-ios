//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"

@interface Upload (ImageGenerator)
- (void)generateThumbImageAtSecond:(int)seconds withSuccess:(void (^)())success andFailure:(void (^)())failure;
@end