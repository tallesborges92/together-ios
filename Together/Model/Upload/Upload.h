//
// Created by Talles  Borges  on 7/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"


@class Scene;

@interface Upload : NSObject

@property NSNumber *objectId;
@property User *user;
@property (weak) Scene *scene;
@property NSString *videoUrl;
@property NSString *imageUrl;

@end