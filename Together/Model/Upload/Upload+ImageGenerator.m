//
// Created by Talles  Borges  on 8/2/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "Upload+ImageGenerator.h"
#import "Upload+FileUtils.h"


@implementation Upload (ImageGenerator)

-(void)generateThumbImageAtSecond :(int) seconds withSuccess:(void (^)())success andFailure:(void (^)())failure
{
    if([self localImageExist])
        [self deleteLocalImageFile];

    AVAsset *asset = [AVAsset assetWithURL:[self uploadPathURL]];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform=TRUE;
    CMTime time = CMTimeMakeWithSeconds(seconds, NSEC_PER_SEC);
    NSError *error;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:&error];

    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    bool save = [UIImagePNGRepresentation(thumbnail) writeToURL:[self pathImageURL] atomically:YES];
    if (save) {
        self.imageUrl = [[self pathImageURL] absoluteString];
        success();
    }else{
        failure();
    }
}

@end