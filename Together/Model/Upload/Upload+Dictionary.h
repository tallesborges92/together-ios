//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Upload.h"

@interface Upload (Dictionary)
//+ (instancetype)uploadWithDictionary:(NSDictionary *)dictionary;
//
//+ (NSArray *)uploadsWithArray:(NSArray *)array;

+ (instancetype)uploadWithDictionary:(NSDictionary *)dictionary andScene:(Scene *)scene;

+ (NSArray *)uploadsWithArray:(NSArray *)array andScene:(Scene *)scene;

- (NSDictionary *)toDictionaryPOST;
@end