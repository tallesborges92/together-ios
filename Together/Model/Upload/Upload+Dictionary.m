//
// Created by Talles  Borges  on 7/20/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "Upload+Dictionary.h"
#import "User+Dictionary.h"
#import "Scene+Dictionary.h"


@implementation Upload (Dictionary)

-(NSDictionary *) toDictionaryPOST {
    return @{@"upload[user_id]" : self.user.objectId, @"upload[scene_id]" : self.scene.objectId, @"upload[video_url]" : self.videoUrl, @"upload[image_url]" : self.imageUrl};
}

+(instancetype) uploadWithDictionary:(NSDictionary *)dictionary andScene:(Scene *)scene {
    Upload *upload = [Upload new];

    upload.objectId = dictionary[@"id"];
    upload.user = [User userWithDictionary:dictionary[@"user"]];
    upload.scene = scene;
    upload.videoUrl = dictionary[@"video_url"];
    upload.imageUrl = dictionary[@"image_url"];

    return upload;
}

+(NSArray *) uploadsWithArray:(NSArray *)array andScene:(Scene *)scene{
    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        Upload *upload = [Upload uploadWithDictionary:dictionary andScene:scene];
        [mutableArray addObject:upload];
    }
    return [mutableArray copy];
}

@end