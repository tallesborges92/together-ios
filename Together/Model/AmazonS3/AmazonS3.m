//
// Created by Talles  Borges  on 9/29/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "AmazonS3.h"
#import "BFTask.h"
#import "AWSS3TransferManager.h"
#import "Constants.h"


@implementation AmazonS3 {

}
+ (void)uploadImage:(UIImage *)image withSuccess:(void (^)(NSString *urlInAmazon))success andFailure:(void (^)())failure {

    NSURL *imageUrl = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"userAvatar.jpg"]];
    [[NSFileManager defaultManager] removeItemAtPath:[imageUrl absoluteString] error:nil];

    [UIImageJPEGRepresentation(image, 0) writeToURL:imageUrl atomically:YES];

    NSString *fileName = [NSString stringWithFormat:@"image_%f", [[NSDate date] timeIntervalSince1970]];

    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = UPLOADS_BUCKET;
    uploadRequest.key = fileName;
    uploadRequest.contentType = @"image/png";
    uploadRequest.body = imageUrl;

    [[transferManager upload:uploadRequest] continueWithBlock:^id(BFTask *task) {
        if (task.isCompleted) {
            success([self finalUrlWithKey:fileName]);
        } else {
            failure();
        }
        return nil;
    }];
}

+(NSString *) finalUrlWithKey:(NSString *)key {
    return [NSString stringWithFormat:@"%@/%@/%@", AMAZON_URL, UPLOADS_BUCKET, key];
}

@end