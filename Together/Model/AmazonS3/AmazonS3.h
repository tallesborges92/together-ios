//
// Created by Talles  Borges  on 9/29/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface AmazonS3 : NSObject
+ (void)uploadImage:(UIImage *)image withSuccess:(void (^)(NSString *urlInAmazon))success andFailure:(void (^)())failure;
@end