//
// Created by Talles  Borges  on 7/25/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>
#import "User+CellUtils.h"


@implementation User (CellUtils)

-(void) configureUserCell:(TableViewCellUser *)cell {
    cell.user = self;
    [cell.imageViewProfile setImageWithURL:[NSURL URLWithString:self.avatarUrl]];
    [cell.labelNome setText:self.name];
    [cell.buttonSeguir setTitle:self.following? @"Seguindo" : @"Seguir" forState:UIControlStateNormal];
    [cell.buttonSeguir setBackgroundImage:self.following? [UIImage imageNamed:@"bt_following"] : [UIImage imageNamed:@"bt_follow"] forState:UIControlStateNormal];
    UIEdgeInsets insetsFollowing = UIEdgeInsetsMake(0, 0, 0, 0);
    UIEdgeInsets insetsFollow = UIEdgeInsetsMake(0, 15, 0, 0);
    [cell.buttonSeguir setContentEdgeInsets:self.following? insetsFollowing : insetsFollow];
    UIColor *redColor = [UIColor colorWithRed:0.894f green:0.000f blue:0.000f alpha:1.00f];
    [cell.buttonSeguir setTitleColor:self.following? [UIColor whiteColor] : redColor forState:UIControlStateNormal];
    [cell.followingLabel setText:[NSString stringWithFormat:@"Following %@",self.totalFollowedUsers]];
    [cell.followersLabel setText:[NSString stringWithFormat:@"Followers %@",self.totalFollowers]];
}

+(NSNumber *) cellSize {
    return @(61);
}

@end