//
// Created by Talles  Borges  on 7/18/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>


@interface User : NSObject

@property NSString *email;
@property NSString *password;
@property NSString *avatarUrl;
@property NSString *coverUrl;
@property NSString *facebookId;
@property NSString *name;
@property NSNumber *objectId;

@property NSNumber *totalFollowedUsers;
@property NSNumber *totalFollowers;


@property bool following;

@end