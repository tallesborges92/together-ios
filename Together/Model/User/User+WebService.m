//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "User+WebService.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "User+Dictionary.h"
#import "Constants.h"
#import "Project+Dictionary.h"
#import "TBNotification.h"
#import "TBNotification+Dictionary.h"


@implementation User (WebService)

-(void)saveInWebfactionWithSuccess:(void (^)(User *user))success failure:(void (^)())failure {
    assert(self.email);
    assert(self.name);
    assert(self.facebookId);
    assert(self.password);
    assert(self.avatarUrl);


    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/users.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        User *user = [User userWithDictionary:responseObject];
        success(user);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

+(void)usersWithSuccess:(void (^)(NSArray *users))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/users.json", URL_BASE] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *users = [User usersWithArray:responseObject];
        success(users);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

+(void)userWithObjectId:(NSNumber *)objectId withSender:(User *)sender success:(void (^)(User *user))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/%@.json", URL_BASE,objectId,sender.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        User *user = [User userWithDictionary:responseObject];
        success(user);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)projectsWithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure{
    [self projectsPage:@1 WithSender:sender success:success failure:failure];
}

- (void)projectsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/projects/%@.json",URL_BASE,self.objectId,sender.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}


-(void)likedProjectsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/likedProjects/%@.json",URL_BASE,self.objectId,sender.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void)participedProjectsWithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    [self participedProjectsPage:@1 WithSender:sender success:success failure:failure];
}

-(void)participedProjectsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/participedProjects/%@.json",URL_BASE,self.objectId,sender.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}


- (void)projectsUnfinishedsWithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure{
    [self projectsUnfinishedsPage:@1 WithSender:sender success:success failure:failure];
}

- (void)projectsUnfinishedsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/projectsUnfinisheds/%@.json",URL_BASE,self.objectId,sender.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void)search:(NSString *)param success:(void (^)(NSArray *users,NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/searches/%@.json",URL_BASE,self.objectId] parameters:@{@"param" : param} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject[@"projects"]];
        NSArray *users = [User usersWithArray:responseObject[@"users"]];
        success(users,projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)feedPage:(NSNumber *)page WithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};

    [manager GET:[NSString stringWithFormat:@"%@/users/%@/feed.json",URL_BASE,self.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *projects = [Project projectsWithArray:responseObject];
        success(projects);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)feedWithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure {
    [self feedPage:@1 WithSuccess:success failure:failure];
}

-(void) likeProject:(Project *)project success:(void (^)())success failure:(void (^)()) failure {
    NSDictionary *dictionary = @{@"project_id" : project.objectId};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/users/%@/like_project", URL_BASE,self.objectId] parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)followUser:(User *)user success:(void (^)())success failure:(void (^)())failure {
    NSDictionary *dictionary = @{@"user[followable_id]" : user.objectId};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/users/%@/follow_user", URL_BASE,self.objectId] parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) followedUsersWithSender:(User *)sender success:(void (^)(NSArray *users))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/followed_users/%@.json", URL_BASE, self.objectId,sender.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *users = [User usersWithArray:responseObject];
        success(users);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) followersWithSender:(User *)sender success:(void (^)(NSArray *users))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/followers/%@.json", URL_BASE, self.objectId,sender.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *users = [User usersWithArray:responseObject];
        success(users);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

- (void)notificationsWithSuccess:(void (^)(NSArray *notifications))success failure:(void (^)())failure {
    [self notificationsPage:@1 WithSuccess:success failure:failure];
}

- (void)notificationsPage:(NSNumber *)page WithSuccess:(void (^)(NSArray *notifications))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *paramns = @{@"page" : page};
    [manager GET:[NSString stringWithFormat:@"%@/users/%@/notifications.json", URL_BASE,self.objectId] parameters:paramns success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *notifications = [TBNotification notificationsWithArray:responseObject];
        success(notifications);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

-(void) loginWithSuccess:(void (^)(User *user))success failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@/users/login.json", URL_BASE] parameters:[self toDictionaryPOST] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        User *user = [User userWithDictionary:responseObject];
        success(user);
    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure();
    }];
}

@end