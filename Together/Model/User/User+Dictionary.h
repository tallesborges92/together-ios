//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface User (Dictionary)
+ (instancetype)userWithDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)toUserDefaults;

+ (instancetype)userFromUserDefaults:(NSDictionary *)dictionary;

+ (NSArray *)usersWithArray:(NSArray *)array;

- (NSDictionary *)toDictionaryPOST;
@end