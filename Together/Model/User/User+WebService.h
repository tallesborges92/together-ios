//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Project.h"

@interface User (WebService)

- (void)saveInWebfactionWithSuccess:(void (^)(User *userSaved))success failure:(void (^)())failure;

+ (void)usersWithSuccess:(void (^)(NSArray *users))success failure:(void (^)())failure;

+(void)userWithObjectId:(NSNumber *)objectId withSender:(User *)sender success:(void (^)(User *user))success failure:(void (^)())failure;

- (void)projectsWithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)projectsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)likedProjectsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)participedProjectsWithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)participedProjectsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)projectsUnfinishedsWithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)projectsUnfinishedsPage:(NSNumber *)page WithSender:(User *)sender success:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)search:(NSString *)param success:(void (^)(NSArray *users, NSArray *projects))success failure:(void (^)())failure;

- (void)feedPage:(NSNumber *)page WithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)feedWithSuccess:(void (^)(NSArray *projects))success failure:(void (^)())failure;

- (void)likeProject:(Project *)project success:(void (^)())success failure:(void (^)())failure;

-(void)followUser:(User *)user success:(void (^)())success failure:(void (^)())failure;

-(void) followedUsersWithSender:(User *)sender success:(void (^)(NSArray *users))success failure:(void (^)())failure;

-(void) followersWithSender:(User *)sender success:(void (^)(NSArray *users))success failure:(void (^)())failure;

- (void)notificationsWithSuccess:(void (^)(NSArray *notifications))success failure:(void (^)())failure;

- (void)notificationsPage:(NSNumber *)page WithSuccess:(void (^)(NSArray *notifications))success failure:(void (^)())failure;

- (void)loginWithSuccess:(void (^)(User *user))success failure:(void (^)())failure;
@end