//
// Created by Talles  Borges  on 7/19/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "User+Dictionary.h"


@implementation User (Dictionary)


-(NSDictionary *) toDictionaryPOST {
    return @{@"user[name]" : self.name, @"user[email]": self.email, @"user[password]" : self.password, @"user[facebook_id]" : self.facebookId, @"user[avatar_url]" : self.avatarUrl, @"user[cover_url]" : self.coverUrl};
}


+(instancetype) userWithDictionary:(NSDictionary *)dictionary {
    User *user = [User new];

    user.name = dictionary[@"name"];
    user.password = dictionary[@"password"];
    user.email = dictionary[@"email"];
    user.facebookId = dictionary[@"facebook_id"];
    user.avatarUrl = dictionary[@"avatar_url"];
    user.coverUrl = dictionary[@"cover_url"];
    user.objectId = dictionary[@"id"];
    user.following = [dictionary[@"following"] boolValue];

    user.totalFollowedUsers = dictionary[@"total_followed_users"];
    user.totalFollowers = dictionary[@"total_followers"];

    return  user;
}

-(NSDictionary *) toUserDefaults {
    return @{@"user[name]" : self.name, @"user[email]": self.email, @"user[password]" : self.password, @"user[facebook_id]" : self.facebookId, @"user[avatar_url]" : self.avatarUrl, @"user[cover_url]" : self.coverUrl, @"user[id]" : self.objectId, @"user[following]" : @(self.following)};
}

+(instancetype) userFromUserDefaults:(NSDictionary *)dictionary {
    User *user = [User new];

    user.name = dictionary[@"user[name]"];
    user.password = dictionary[@"user[password]"];
    user.email = dictionary[@"user[email]"];
    user.facebookId = dictionary[@"user[facebook_id]"];
    user.avatarUrl = dictionary[@"user[avatar_url]"];
    user.coverUrl = dictionary[@"user[cover_url]"];
    user.objectId = dictionary[@"user[id]"];
    user.following = [dictionary[@"user[following]"] boolValue];

    user.totalFollowedUsers = dictionary[@"total_followed_users"];
    user.totalFollowers = dictionary[@"total_followers"];

    return  user;
}


+(NSArray *) usersWithArray:(NSArray *)array {

    NSMutableArray *mutableArray = [NSMutableArray array];

    for (NSDictionary *dictionary in array) {
        User *user = [User userWithDictionary:dictionary];
        [mutableArray addObject:user];
    }

    return [mutableArray copy];
}

@end