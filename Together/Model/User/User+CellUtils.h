//
// Created by Talles  Borges  on 7/25/14.
// Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "TableViewCellUser.h"

@interface User (CellUtils)


+ (NSNumber *)cellSize;

- (void)configureUserCell:(TableViewCellUser *)cell;
@end