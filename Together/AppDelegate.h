//
//  AppDelegate.h
//  Together
//
//  Created by Talles  Borges  on 7/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (void)defaultStyleAppearance;

@end
