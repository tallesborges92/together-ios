//
//  ViewControllerScene.h
//  Together
//
//  Created by Talles  Borges  on 7/20/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"

@interface ViewControllerScene : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

#pragma mark - UIView
@property IBOutlet UIView *viewPlayer;

#pragma mark - Model
@property Scene *scene;

#pragma mark - UIImageView
@property (weak, nonatomic) IBOutlet UIImageView *imageViewScene;

#pragma mark - UICollectionView
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewUploads;

@property(nonatomic, strong) UIActionSheet *sheetUpload;
#pragma mark - Actions
- (IBAction)novoUpload:(id)sender;

@end
