//
//  SceneViewController.h
//  Together
//
//  Created by Talles  Borges  on 10/1/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"

@interface SceneViewController : UIViewController
#pragma mark - Model
@property Scene *scene;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UILabel *sceneTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sceneStoryboardImageView;
@property (weak, nonatomic) IBOutlet UILabel *sceneTitle1Label;
@property (weak, nonatomic) IBOutlet UILabel *sceneTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sceneDescriptionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *sceneUploadsCollectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *formScrollView;
@property (weak, nonatomic) IBOutlet UIButton *recButton;
@property (weak, nonatomic) IBOutlet UILabel *noneVideoSentLabel;


#pragma mark - Actions
- (IBAction)backPressed:(id)sender;
- (IBAction)recScenePressed:(id)sender;
- (IBAction)sceneMusicPressed:(id)sender;

@end
