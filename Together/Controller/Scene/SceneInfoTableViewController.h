//
//  SceneInfoTableViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"

@interface SceneInfoTableViewController : UITableViewController

#pragma mark - Outlet
@property Scene *scene;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UILabel *nameSceneLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionSceneLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationSceneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageSceneImageView;

@end
