//
//  ViewControllerScene.m
//  Together
//
//  Created by Talles  Borges  on 7/20/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ViewControllerScene.h"
#import "Together.h"
#import "Upload+Webfaction.h"
#import "Scene+Webfaction.h"
#import "Upload+FileUtils.h"
#import "MBProgressHUD.h"
#import "Upload+Downloader.h"
#import "Upload+AVExport.h"
#import "UploadDataSource.h"
#import "UploadDelegate.h"
#import "Upload+ImageGenerator.h"
#import "Upload+AmazonS3.h"
#import "ViewControllerRecord.h"
#import "SceneInfoTableViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIImageEffects/UIImage+ImageEffects.h>

@interface
ViewControllerScene ()

@end

@implementation ViewControllerScene {
    NSArray *_uploads;
    MPMoviePlayerController *_playerController;
    UIActionSheet *_actionSheedOpcaoUpload;
    UploadDataSource *_uploadDataSource;
    UploadDelegate *_uploadDelegate;
}


#pragma mark - UIViewMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _uploads = [NSMutableArray array];
    [self.navigationItem setTitle:_scene.name];

    _playerController = [[MPMoviePlayerController alloc] init];
    [_playerController.view setFrame:_viewPlayer.bounds];
    [_viewPlayer addSubview:_playerController.view];

    [_imageViewScene sd_setImageWithURL:[NSURL URLWithString:_scene.imageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_imageViewScene setImage:[image applyLightEffect]];
    }];

    _uploadDataSource = [UploadDataSource sourceWithUploads:_uploads collectionView:_collectionViewUploads];
    _uploadDelegate = [UploadDelegate delegateWithViewController:self dataSource:_uploadDataSource];
    _collectionViewUploads.delegate = _uploadDelegate;

    UIBarButtonItem *buttonItemNewUpload = [[UIBarButtonItem alloc] initWithTitle:@"Add Video" style:UIBarButtonItemStylePlain target:self action:@selector(addVideo:)];
    UIBarButtonItem *buttonItemSceneInfo = [[UIBarButtonItem alloc] initWithTitle:@"?" style:UIBarButtonItemStylePlain target:self action:@selector(sceneInfo:)];

    NSArray *rightItem = @[buttonItemNewUpload,buttonItemSceneInfo];

    self.navigationItem.rightBarButtonItems = rightItem;

}

- (void)sceneInfo:(id)sceneInfo {
    SceneInfoTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"sceneInfoVC"];
    vc.scene = _scene;

    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addVideo:(id)addVideo {
//    _uploadActionSheet = [[UIActionSheet alloc] initWithTitle:nil
//                                                          delegate:self
//                                                 cancelButtonTitle:@"Cancelar"
//                                            destructiveButtonTitle:nil
//                                                 otherButtonTitles:@"Gravar video", nil];
//    [_uploadActionSheet showFromTabBar:self.tabBarController.tabBar];
    ViewControllerRecord *vcRecord = [self.storyboard instantiateViewControllerWithIdentifier:@"recordVC"];
    vcRecord.scene = _scene;

    [self presentViewController:vcRecord animated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (_playerController.playbackState == MPMoviePlaybackStatePlaying) {
        [_playerController pause];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_scene uploadsWithSuccess:^(NSArray *uploads) {
        _uploads = uploads;
        _uploadDataSource.uploads = _uploads;
        [_collectionViewUploads reloadData];
    } failure:^{

    }];
}


#pragma mark - Actions
- (IBAction)novoUpload:(id)sender {


}

//#pragma mark - UITableViewDataSource
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return _uploads.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//
//    Upload *upload = _uploads[indexPath.row];
//
//    [cell.textLabel setText:upload.user.name];
//    [cell.detailTextLabel setText:[upload.objectId stringValue]];
//    if ([_scene.upload.objectId isEqualToNumber:upload.objectId]) {
//        cell.textLabel.text = [cell.textLabel.text stringByAppendingString:@" *"];
//    }
//    if ([upload uploadFileExists]) {
//        cell.textLabel.text = [cell.textLabel.text stringByAppendingString:@" Downloaded"];
//    }
//
//    return cell;
//}

//#pragma mark - UITableViewDelegate
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    self.sheetUpload = [[UIActionSheet alloc] initWithTitle:nil
//                                                   delegate:self
//                                          cancelButtonTitle:@"Cancelar"
//                                     destructiveButtonTitle:nil
//                                          otherButtonTitles:@"Reproduzir",@"Selecionar como default",@"Download", nil];
//    self.sheetUpload.tag = indexPath.row;
//    [self.sheetUpload showFromTabBar:self.tabBarController.tabBar];
//}

#pragma mark -UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (actionSheet == _sheetUpload) {
        Upload *upload = _uploads[actionSheet.tag];

        if (buttonIndex == 0) { // Reproduzir
            [_playerController setContentURL:[NSURL URLWithString:upload.videoUrl]];
            [_playerController play];
        } else if (buttonIndex == 1) { // Selecionar Default
            _scene.upload = upload;
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Salvando";
            [_scene updateInWebfactionSuccess:^(Scene *sceneUpdated) {
                _scene = sceneUpdated;
                [self viewWillAppear:NO];
                [hud hide:YES];
            }                         failure:^{
                [hud hide:YES];
            }];
        } else if (buttonIndex == 2) { // Download
            if ([upload uploadFileExists]) {
                NSLog(@"Arquivo ja baixado");
            } else {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.labelText = @"Baixando video";
                [upload downloadWithSuccess:^{
//                    [_tableViewUploads reloadData];
                    [hud hide:YES];
                }                andFailure:^{
                    [hud hide:YES];
                }];
            }
        }
    }else {
        if(buttonIndex == 2) return;

//        if (buttonIndex == 0) {
////            [vc setSourceType:UIImagePickerControllerSourceTypeCamera];
////            [vc setCameraCaptureMode:UIImagePickerControllerCameraCaptureModeVideo];
//            ViewControllerRecord *vcRecord = [self.storyboard instantiateViewControllerWithIdentifier:@"recordVC"];
//            vcRecord.scene = _scene;
//
//            [self presentViewController:vcRecord animated:YES completion:nil];
//        } else if(buttonIndex == 1) {
//            UIImagePickerController *vc = [UIImagePickerController new];
//            vc.delegate = self;
//            [vc setVideoMaximumDuration:30.0];
//            [vc setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
//            [vc setVideoQuality:UIImagePickerControllerQualityType640x480];
//            [vc setMediaTypes:[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]];
//
//            [self presentViewController:vc animated:YES completion:nil];
//        }

    }

}


#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = NSLocalizedString(@"sending_video", nil);

        Upload *upload = [Upload new];
        upload.scene =_scene;
        upload.user = [Together instance].user;
        upload.videoUrl = [info[@"UIImagePickerControllerMediaURL"] absoluteString];
        upload.imageUrl = @"";

        [upload exportWithSuccess:^{
            [upload saveInWebfactionWithSuccess:^(Upload *uploadSaved) {
                [upload generateThumbImageAtSecond:1 withSuccess:^{
                    [upload saveImageInAmazonS3WithSuccess:^{
                        [upload updateInWebfactionWithSuccess:^(Upload *uploadSaved) {
                            [self viewWillAppear:NO];
                            [hud hide:YES];
                        } failure:^{
                            NSLog(@"pau quando foi atualizar");
                        }];
                    } andFailure:^{
                        NSLog(@"pau quando foi salvar imagem");
                    }];
                } andFailure:^{
                    NSLog(@"pau quando foi gerar thumb");
                }];
            } failure:^{
                NSLog(@"pau quando foi salvar na webfaction");
                [hud hide:YES];
            }];
        } andFailure:^{
            NSLog(@"pau quando foi exportar");
            [hud hide:YES];
        }];
    }];
}

@end
