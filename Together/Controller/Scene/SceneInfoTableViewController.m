//
//  SceneInfoTableViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "SceneInfoTableViewController.h"

@interface SceneInfoTableViewController ()

@end

@implementation SceneInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _nameSceneLabel.text = _scene.name;
    _descriptionSceneLabel.text = _scene.descriptionText;
    _durationSceneLabel.text = [NSString stringWithFormat:@"%.2f s", [_scene.time floatValue]];
    [_imageSceneImageView sd_setImageWithURL:[NSURL URLWithString:_scene.imageUrl]];
}

@end
