//
//  SceneViewController.m
//  Together
//
//  Created by Talles  Borges  on 10/1/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "SceneViewController.h"
#import "ViewControllerRecord.h"
#import "UploadDataSource.h"
#import "Scene+Webfaction.h"
#import "UploadDelegate.h"
#import "Project+FileUtils.h"
#import "Project+MusicDownload.h"
#import "UIAlertView+Utils.h"
#import "Together.h"
#import "Upload+FileUtils.h"
#import "UIButton+TBButtonFont.h"

@interface SceneViewController ()

@end

@implementation SceneViewController {
    UploadDataSource *_uploadDataSource;
    UploadDelegate *_uploadDelegate;
    NSArray *_uploads;
    AVPlayer *_avPlayer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _sceneTitleLabel.text = _scene.name;
    _sceneTitle1Label.text = _scene.name;
    _sceneDescriptionLabel.text = _scene.descriptionText;
//    [_sceneStoryboardImageView sd_setImageWithURL:[NSURL URLWithString:_scene.imageUrl]];
    [_sceneStoryboardImageView sd_setImageWithURL:[NSURL URLWithString:_scene.imageUrl] placeholderImage:[UIImage imageNamed:@"bt_createScene"] options:SDWebImageRefreshCached];
    _sceneStoryboardImageView.layer.borderWidth = 2;
    _sceneStoryboardImageView.layer.borderColor = [UIColor redColor].CGColor;
    _sceneTimeLabel.text = [NSString stringWithFormat:@"%.1f sec", [_scene.time doubleValue]];

    [_recButton setTitlePacifico:NSLocalizedString(@"rec_video", nil)];

    _uploadDataSource = [UploadDataSource sourceWithUploads:[NSArray array] collectionView:_sceneUploadsCollectionView];
    _uploadDelegate = [UploadDelegate delegateWithViewController:self dataSource:_uploadDataSource];
    _uploadDelegate.selectUpload = ^(Upload *upload) {
        [upload deleteLocalUpload];
        _scene.upload = upload;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = NSLocalizedString(@"saving", nil);
        [_scene updateInWebfactionSuccess:^(Scene *sceneUpdated) {
            _scene = sceneUpdated;
            [self viewWillAppear:NO];
            [hud hide:YES];
        }                         failure:^{
            [hud hide:YES];
        }];
    };


    if (![_scene.project.audioUrl isEqualToString:@""]) {
        if (![_scene.project localAudioExist]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = NSLocalizedString(@"synchronizing_music", nil);
            [_scene.project downloadMusicWithSuccess:^{
                [hud hide:YES];
            } andFailure:^{
                [hud hide:YES];
            }];
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
//    _formScrollView.contentSize = CGSizeMake(320, 630);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_scene uploadsWithSuccess:^(NSArray *uploads) {
        _noneVideoSentLabel.hidden = uploads.count > 0;
        _uploads = uploads;
        _uploadDataSource.uploads = _uploads;
        [_uploadDataSource.collectionView reloadData];
    } failure:^{

    }];
}

#pragma mark - Actions

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)recScenePressed:(id)sender {
    if (![Together instance].user) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"need_logged", nil)];
        return;
    }
    ViewControllerRecord *vcRecord = [self.storyboard instantiateViewControllerWithIdentifier:@"recordVC"];
    vcRecord.scene = _scene;

    [self presentViewController:vcRecord animated:YES completion:nil];
}

- (IBAction)sceneMusicPressed:(id)sender {
    if ([_scene.project.audioUrl isEqualToString:@""]) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"music_not_found", nil)];
        return;
    }
    if (![_scene.project localAudioExist]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = NSLocalizedString(@"synchronizing_music", nil);
        [_scene.project downloadMusicWithSuccess:^{
            [hud hide:YES];
        } andFailure:^{
            [hud hide:YES];
        }];
        return;
    }
    AVMutableComposition *composition = [AVMutableComposition composition];
    AVMutableCompositionTrack *musicTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];

    AVAsset *assetMusica = [AVAsset assetWithURL:[_scene.project pathAudioURL]];

    NSArray *musicTypeAudios = [assetMusica tracksWithMediaType:AVMediaTypeAudio];
    if (musicTypeAudios.count > 0) {
        AVAssetTrack *musicAudioTrack = musicTypeAudios[0];

        CMTimeRange musicRange = CMTimeRangeMake(CMTimeMakeWithSeconds([_scene.start floatValue], NSEC_PER_SEC), CMTimeMakeWithSeconds([_scene.time floatValue], NSEC_PER_SEC));

        [musicTrack insertTimeRange:musicRange ofTrack:musicAudioTrack atTime:kCMTimeZero error:nil];

        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:composition];
        _avPlayer = [AVPlayer playerWithPlayerItem:item];
        [_avPlayer play];
    }else {
        [_scene.project deleteLocalAudioFile];
    }
}




@end
