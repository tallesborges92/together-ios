//
//  SignInViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/29/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController

#pragma mark - Actions
- (IBAction)backPressed:(id)sender;
- (IBAction)signInPressed:(id)sender;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITextField *userEmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *userPasswordTextField;

@end
