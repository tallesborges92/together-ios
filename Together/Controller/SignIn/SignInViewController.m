//
//  SignInViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/29/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "SignInViewController.h"
#import "User.h"
#import "User+WebService.h"
#import "UIAlertView+Utils.h"
#import "Together.h"
#import "User+Dictionary.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)signInPressed:(id)sender {

    User *user = [User new];
    user.name = @"";
    user.avatarUrl = @"";
    user.facebookId = @"";
    user.email = _userEmailTextField.text;
    user.password = _userPasswordTextField.text;
    user.coverUrl = @"";

    [user loginWithSuccess:^(User *userLogged) {
        [self dismissViewControllerAnimated:YES completion:^{
            [Together instance].user = userLogged;
            [[NSUserDefaults standardUserDefaults] setObject:[userLogged toUserDefaults] forKey:@"user_logged"];
            [[Together instance] initTabBarController];
        }];
    } failure:^{
        [UIAlertView showAlertWithString:NSLocalizedString(@"user_invalid", nil)];
    }];
}
@end
