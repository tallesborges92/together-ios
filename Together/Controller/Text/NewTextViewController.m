//
//  NewTextViewController.m
//  Together
//
//  Created by Talles  Borges  on 12/9/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "NewTextViewController.h"
#import "Text.h"
#import "MBProgressHUD.h"
#import "Text+Webfaction.h"

@interface NewTextViewController ()

@end

@implementation NewTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)savePressed:(id)sender {
    Text *text = [Text new];
    text.project = _project;
    text.text = _textTextField.text;
    text.start = _start;
    text.time = @(_timeSlider.value);

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"creating_text", nil);

    [text saveInWebfactionWithSuccess:^(Text *textSaved) {
            [hud hide:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
    } failure:^{
        [hud hide:YES];
    }];

}

- (IBAction)timeSliderChanged:(id)sender {
    _timeLabel.text = [NSString stringWithFormat:@"%.1f", _timeSlider.value];
}
@end
