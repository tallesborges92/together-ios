//
//  TableViewControllerText.m
//  Together
//
//  Created by Talles  Borges  on 9/17/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import "TableViewControllerText.h"
#import "Text.h"
#import "Text+Webfaction.h"

@interface TableViewControllerText ()

@end

@implementation TableViewControllerText

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction)timeChanged:(id)sender {
    _timeLabel.text = [NSString stringWithFormat:@"%.2f s", _timeSlider.value];
}

- (IBAction)savePressed:(id)sender {

    Text *text = [Text new];
    text.project = _project;
    text.text = _textTextField.text;
    text.start = _start;
    text.time = @(_timeSlider.value);

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Criando texto";

    [text saveInWebfactionWithSuccess:^(Text *textSaved) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hide:YES];
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failure:^{
        [hud hide:YES];
    }];

}
@end
