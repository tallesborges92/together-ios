//
//  TableViewControllerText.h
//  Together
//
//  Created by Talles  Borges  on 9/17/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface TableViewControllerText : UITableViewController

#pragma mark - model
@property Project *project;
@property NSNumber *start;

#pragma mark - outlet
@property (weak, nonatomic) IBOutlet UITextField *textTextField;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UISlider *timeSlider;

#pragma mark - Action
- (IBAction)timeChanged:(id)sender;
- (IBAction)savePressed:(id)sender;


@end
