//
//  NewTextViewController.h
//  Together
//
//  Created by Talles  Borges  on 12/9/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface NewTextViewController : UIViewController

#pragma mark - Model
@property Project *project;
@property NSNumber *start;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITextField *textTextField;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UISlider *timeSlider;
@property (weak, nonatomic) IBOutlet UILabel *secondsLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

#pragma mark - Actions
- (IBAction)backPressed:(id)sender;
- (IBAction)savePressed:(id)sender;
- (IBAction)timeSliderChanged:(id)sender;



@end
