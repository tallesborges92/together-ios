//
//  UsersTableViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "UsersTableViewController.h"
#import "UserDataSource.h"
#import "TableViewControllerProfile.h"

@interface UsersTableViewController ()

@end

@implementation UsersTableViewController {
    UserDataSource *_userDataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _userDataSource = [UserDataSource sourceWithTableView:self.tableView array:_users];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    User *user = _users[indexPath.row];


    TableViewControllerProfile *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
    vc.user = user;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
