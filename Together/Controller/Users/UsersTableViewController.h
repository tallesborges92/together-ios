//
//  UsersTableViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UsersTableViewController : UITableViewController

#pragma mark - Model
@property NSArray *users;

@end
