//
//  StoryboardViewController.m
//  Together
//
//  Created by Talles  Borges  on 12/4/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "StoryboardViewController.h"
#import "Storyboard+WebService.h"
#import "StoryboardCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@interface StoryboardViewController ()

@end

@implementation StoryboardViewController {
    NSArray *_array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _array = [NSArray new];

    [Storyboard storyboardsWithSuccess:^(NSArray *storyboards) {
        _array = storyboards;
        [_collectionView reloadData];
    } failure:^{
        NSLog(@"FALHA");
    }];


}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - DataSource - Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    StoryboardCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    Storyboard *storyboard = _array[indexPath.row];

    [cell.storyboardImage sd_setImageWithURL:[NSURL URLWithString:storyboard.image_url]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    StoryboardCollectionViewCell *cell = (StoryboardCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];

    Storyboard *storyboard = _array[indexPath.row];
    UIImage *selectedImage = cell.storyboardImage.image;

    [_delegate selectedStoryboard:storyboard withImage:selectedImage];

    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
