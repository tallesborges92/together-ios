//
//  StoryboardViewController.h
//  Together
//
//  Created by Talles  Borges  on 12/4/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Storyboard.h"

@protocol StoryboardViewControllerDelegate <NSObject>

-(void) selectedStoryboard:(Storyboard *)storyboard withImage:(UIImage *)image;

@end

@interface StoryboardViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>

#pragma mark - Model
@property (weak, nonatomic) id <StoryboardViewControllerDelegate> delegate;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


#pragma mark - Action
- (IBAction)backPressed:(id)sender;

@end
