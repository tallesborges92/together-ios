//
//  TableViewControllerAudio.m
//  Together
//
//  Created by Talles  Borges  on 9/17/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TableViewControllerAudio.h"
#import "Audio.h"
#import "Audio+Webfaction.h"

@interface TableViewControllerAudio ()

@end

@implementation TableViewControllerAudio {
    SCRecorder *_recorder;
    SCPlayer *_player;
    NSURL *_fileUrl;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    _recorder = [SCRecorder recorder];
    _recorder.videoEnabled = NO;
    _recorder.photoEnabled = NO;
    _recorder.audioEnabled = YES;
    _recorder.delegate = self;

    [_recorder openSession:^(NSError *sessionError, NSError *audioError, NSError *videoError, NSError *photoError) {
        if (!sessionError || audioError) {
            NSLog(@"sessionError = %@", sessionError);
            NSLog(@"audioError = %@", audioError);
        }

        [_recorder startRunningSession];
    }];

    _player = [SCPlayer player];
    _player.delegate = self;
    [_player beginSendingPlayMessages];

}


#pragma mark - Actions
- (IBAction)salvarPressed:(id)sender {

    Audio *audio = [Audio new];
    audio.aurdioUrl = [_fileUrl absoluteString];
    audio.project = _project;
    audio.start = _start;
    audio.time =  @(CMTimeGetSeconds(_player.itemDuration));

    [audio saveInWebfactionWithSuccess:^(Audio *audioSaved) {
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^{

    }];

}

- (IBAction)playPressed:(id)sender {
    [_player play];
}

- (IBAction)recordPressed:(id)sender {
    SCRecordSession *recordSession = [SCRecordSession recordSession];
    recordSession.fileType = AVFileTypeAppleM4A;

    _recorder.recordSession = recordSession;

    [_recorder record];
}

- (IBAction)stopRecordPressed:(id)sender {
    SCRecordSession *recordSession = _recorder.recordSession;
    _recorder.recordSession = nil;

    [recordSession endSession:^(NSError *error) {
        if (error == nil) {
            NSURL *fileUrl = recordSession.outputUrl;
            NSLog(@"fileUrl = %@", fileUrl);
            _fileUrl = fileUrl;
            [_player setItemByUrl:_fileUrl];
        } else {
            // Handle the error
            NSLog(@"error = %@", error);
        }
    }];

}

#pragma mark - SCRecorderDelegate
- (void)recorder:(SCRecorder *)recorder didAppendAudioSampleBuffer:(SCRecordSession *)recordSession {
    _timeLabel.text = [NSString stringWithFormat:@"%.2fs", CMTimeGetSeconds(recordSession.currentRecordDuration)];
}

#pragma mark - SCPlayerDelegate
- (void)player:(SCPlayer *)player didReachEndForItem:(AVPlayerItem *)item {
    [_player seekToTime:kCMTimeZero];
    [_player pause];
}

@end
