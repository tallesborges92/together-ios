//
//  NewAudioViewController.h
//  Together
//
//  Created by Talles  Borges  on 10/1/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface NewAudioViewController : UIViewController <SCPlayerDelegate, SCRecorderDelegate>

#pragma mark - Model
@property Project *project;
@property NSNumber *start;

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *recButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UILabel *secondsLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

#pragma mark - Actions
- (IBAction)recPressed:(id)sender;
- (IBAction)stopPressed:(id)sender;
- (IBAction)playPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)savePressed:(id)sender;

@end
