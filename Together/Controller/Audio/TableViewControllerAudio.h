//
//  TableViewControllerAudio.h
//  Together
//
//  Created by Talles  Borges  on 9/17/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SCRecorder/SCRecorder.h>
#import "Project.h"

@interface TableViewControllerAudio : UITableViewController <SCRecorderDelegate, SCPlayerDelegate>

#pragma mark - Model
@property Project *project;
@property NSNumber *start;


#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

#pragma mark - Actions
- (IBAction)salvarPressed:(id)sender;
- (IBAction)playPressed:(id)sender;
- (IBAction)recordPressed:(id)sender;
- (IBAction)stopRecordPressed:(id)sender;

@end
