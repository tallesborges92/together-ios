//
//  NewAudioViewController.m
//  Together
//
//  Created by Talles  Borges  on 10/1/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SCRecorder/SCPlayer.h>
#import <SCRecorder/SCRecorder.h>
#import "NewAudioViewController.h"
#import "UIAlertView+Utils.h"
#import "Audio.h"
#import "Audio+Webfaction.h"
#import "UIButton+TBButtonFont.h"

@interface NewAudioViewController ()   {
    SCRecorder *_recorder;
    SCPlayer *_player;
    NSURL *_fileUrl;
}

@end

@implementation NewAudioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_playButton setHidden:YES];
    [_stopButton setHidden:YES];

    _recorder = [SCRecorder recorder];
    _recorder.videoEnabled = NO;
    _recorder.photoEnabled = NO;
    _recorder.audioEnabled = YES;
    _recorder.delegate = self;

    [_recorder openSession:^(NSError *sessionError, NSError *audioError, NSError *videoError, NSError *photoError) {
        if (!sessionError || audioError) {
            NSLog(@"sessionError = %@", sessionError);
            NSLog(@"audioError = %@", audioError);
        }

        [_recorder startRunningSession];
    }];

    _player = [SCPlayer player];
    _player.delegate = self;
    [_player beginSendingPlayMessages];

    [_saveButton setTitlePacifico:NSLocalizedString(@"save_audio", nil)];
}

#pragma mark - Actions
- (IBAction)recPressed:(id)sender {
    _fileUrl = nil;
    _secondsLabel.text = @"0.0";
    [_recButton setHidden:YES];
    [_playButton setHidden:YES];
    [_stopButton setHidden:NO];

    SCRecordSession *recordSession = [SCRecordSession recordSession];
    recordSession.fileType = AVFileTypeAppleM4A;

    _recorder.recordSession = recordSession;

    [_recorder record];
}

- (IBAction)stopPressed:(id)sender {
    [_stopButton setHidden:YES];
    [_playButton setHidden:NO];
    [_recButton setHidden:NO];

    SCRecordSession *recordSession = _recorder.recordSession;
    _recorder.recordSession = nil;

    [recordSession endSession:^(NSError *error) {
        if (error == nil) {
            NSURL *fileUrl = recordSession.outputUrl;
            NSLog(@"fileUrl = %@", fileUrl);
            _fileUrl = fileUrl;
            [_player setItemByUrl:_fileUrl];
        } else {
            // Handle the error
            NSLog(@"error = %@", error);
        }
    }];

}
- (IBAction)playPressed:(id)sender {
    [_player play];
}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)savePressed:(id)sender {
    if (!_fileUrl) {
         [UIAlertView showAlertWithString:NSLocalizedString(@"record_something", nil)];
        return;
    }
    Audio *audio = [Audio new];
    audio.aurdioUrl = [_fileUrl absoluteString];
    audio.project = _project;
    audio.start = _start;
    audio.time =  @(CMTimeGetSeconds(_player.itemDuration));

    [audio saveInWebfactionWithSuccess:^(Audio *audioSaved) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } failure:^{

    }];
}

#pragma mark - SCRecorderDelegate
- (void)recorder:(SCRecorder *)recorder didAppendAudioSampleBuffer:(SCRecordSession *)recordSession {
    _secondsLabel.text = [NSString stringWithFormat:@"%.1f", CMTimeGetSeconds(recordSession.currentRecordDuration)];
}

#pragma mark - SCPlayerDelegate
- (void)player:(SCPlayer *)player didReachEndForItem:(AVPlayerItem *)item {
    [_player seekToTime:kCMTimeZero];
    [_player pause];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
