//
//  ProjectDescriptionRecordViewController.m
//  Together
//
//  Created by Talles  Borges  on 12/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ProjectDescriptionRecordViewController.h"
#import "UploadPreviewViewController.h"

@interface ProjectDescriptionRecordViewController ()

@end

@implementation ProjectDescriptionRecordViewController{
    SCRecorder *_recorder;
}

#pragma mark - View Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _recorder = [SCRecorder recorder];
    _recorder.sessionPreset = AVCaptureSessionPreset640x480;
    _recorder.audioEnabled = YES;
    _recorder.videoEnabled = YES;
    _recorder.photoEnabled = NO;
    _recorder.delegate = self;
    _recorder.autoSetVideoOrientation = YES;

    _recorder.previewView = _previewView;

    [self prepareCamera];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];

    [self validInterfaceOrientation];
}

- (void)orientationChanged:(NSNotification *)notification{
    [self validInterfaceOrientation];;
}

-(void) validInterfaceOrientation {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(UIDeviceOrientationIsPortrait(orientation) || orientation ==UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp) {
        _turnPhoneImageView.hidden = NO;
        _recordButton.enabled = NO;
    } else {
        _turnPhoneImageView.hidden = YES;
        _recordButton.enabled = YES;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)prepareCamera {
    [_recorder openSession:^(NSError *sessionError, NSError *audioError, NSError *videoError, NSError *photoError) {
        NSError *error = nil;
        [_recorder setActiveFormatThatSupportsFrameRate:24 width:960 andHeight:540 error:&error];
        _recorder.frameRate = 24;
        NSLog(@"%@", error);

        NSLog(@"==== Opened session ====");
        NSLog(@"Session error: %@", sessionError.description);
        NSLog(@"Audio error : %@", audioError.description);
        NSLog(@"Video error: %@", videoError.description);
        NSLog(@"Photo error: %@", photoError.description);
        NSLog(@"=======================");
        [_recorder startRunningSession];
    }];
}

- (BOOL)shouldAutorotate {
    return YES;
}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_recorder previewViewFrameChanged];
}


#pragma mark - Actions
- (IBAction)recordPressed:(id)sender {
    if ([_recorder isRecording]) {
        [_recordButton setTitle:NSLocalizedString(@"rec", nil) forState:UIControlStateNormal];
        [_recorder pause];
    } else {
        [_recordButton setTitle:NSLocalizedString(@"stop", nil) forState:UIControlStateNormal];
        SCRecordSession *recordSession = [SCRecordSession recordSession];

        recordSession.suggestedMaxRecordDuration = CMTimeMakeWithSeconds(30, NSEC_PER_SEC);
        _recorder.recordSession = recordSession;

        [_recorder record];
    }
}

- (IBAction)cancelPressed:(id)sender {
    [_recorder closeSession];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)rotateCameraPressed:(id)sender {
    [_recorder switchCaptureDevices];
}

//- (IBAction)stopPressed:(id)sender {
//    [_recorder pause];
//}

#pragma mark - SCRecorderDelegate

- (void)recorder:(SCRecorder *)recorder didCompleteRecordSession:(SCRecordSession *)recordSession {
    NSLog(@"didCompleteRecordSession");
    [recorder closeSession];
    recorder.recordSession = nil;
    [recordSession endSession:^(NSError *error) {
        if (error == nil) {
            NSURL *fileUrl = recordSession.outputUrl;

            UploadPreviewViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"previewVC"];
            vc.fileUrl = fileUrl;

            vc.savePreview = ^(SCFilterGroup *filterGroup) {
                _project.videoDescriptionUrl = [fileUrl absoluteString];
                [self dismissViewControllerAnimated:YES completion:nil];
            };

            vc.cancelPreview = ^{
                [self prepareCamera];
            };

            [self presentViewController:vc animated:YES completion:nil];

        } else {
            // Handle the error
        }
    }];

}

- (void)recorder:(SCRecorder *)recorder didEndRecordSegment:(SCRecordSession *)recordSession segmentIndex:(NSInteger)segmentIndex error:(NSError *)error {
    [self recorder:recorder didCompleteRecordSession:recordSession];
}


- (void)recorder:(SCRecorder *)recorder didInitializeAudioInRecordSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"error = %@", error);
}

- (void)recorder:(SCRecorder *)recorder didInitializeVideoInRecordSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"error = %@", error);
}

@end
