//
//  TableViewControllerCreateProject.h
//  Together
//
//  Created by Talles  Borges  on 7/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewControllerCreateProject : UITableViewController

#pragma mark - UITextField
@property IBOutlet UITextField *tfNome;
@property IBOutlet UITextView *tfDescricao;

#pragma mark - Action
- (IBAction)salvarProjeto:(id)sender;


@end
