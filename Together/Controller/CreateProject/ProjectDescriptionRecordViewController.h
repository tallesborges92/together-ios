//
//  ProjectDescriptionRecordViewController.h
//  Together
//
//  Created by Talles  Borges  on 12/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecorder.h"
#import "Project.h"

@interface ProjectDescriptionRecordViewController : UIViewController <SCRecorderDelegate>

#pragma mark - Model
@property Project *project;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIView *turnPhoneImageView;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

#pragma mark - Action
- (IBAction)recordPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;
- (IBAction)rotateCameraPressed:(id)sender;

@end
