//
//  NewProjectViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/28/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <OBTabBarController/OBTabBarController.h>
#import "NewProjectViewController.h"
#import "Together.h"
#import "ViewControllerProject.h"
#import "Project+Webfaction.h"
#import "CategoriesViewController.h"
#import "PrivacyTableViewController.h"
#import "ProjectDescriptionRecordViewController.h"
#import "Project+AmazonS3.h"
#import "Project+AVExport.h"
#import "MBProgressHUD.h"
#import "UIAlertView+Utils.h"

@interface NewProjectViewController ()

@end

@implementation NewProjectViewController {
    LMMediaPlayerView *_lmMediaplayerView;
}


#pragma mark - Init
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *textButton;
    if (!_project) {
        _project = [Project new];
        textButton = @"create_video";
    }else {
        textButton = @"update_video";
        if ([_project.user.objectId isEqualToNumber:[Together instance].user.objectId]) {
            [self.navigationItem setTitle:NSLocalizedString(@"edit_project", nil)];
            UIBarButtonItem *buttonDeletar = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_delete_project"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteProject)];
            [self.navigationItem setRightBarButtonItem:buttonDeletar];
        } else {
            [self.navigationItem setTitle:NSLocalizedString(@"description_project", nil)];
        }
    }
    [self.navigationItem.leftBarButtonItem setImage:[[UIImage imageNamed:@"ic_voltar"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [_createButton setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(textButton, nil ) attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Pacifico" size:22]}]forState:UIControlStateNormal];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPressed)];
    tapGestureRecognizer.numberOfTapsRequired = 1;

    [self.view addGestureRecognizer:tapGestureRecognizer];

}

- (void)tapPressed {
    [_summaryTextView resignFirstResponder];
    [_titleTextField resignFirstResponder];
}

- (void)deleteProject {
    UIAlertView *alertView = [UIAlertView new];
    alertView.title = NSLocalizedString(@"confirm", nil);
    alertView.message = NSLocalizedString(@"confirm_delete", nil);
    [alertView addButtonWithTitle:NSLocalizedString(@"no", nil)];
    [alertView addButtonWithTitle:NSLocalizedString(@"yes", nil)];
    [alertView setDelegate:self];
    [alertView show];
}


#pragma mark - ViewMethods
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController.navigationBar setTranslucent:NO];

    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:84.0/255 green:105.0/255 blue:121.0/255 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Avenir-Medium" size:16]}];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:84.0/255 green:105.0/255 blue:121.0/255 alpha:1.0]];

    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;

    if (_project.category) {
        [_categoryTextField setText:_project.category.name];
    }
    if (_project.name) {
        _titleTextField.text = _project.name;
    }
    if (_project.descriptionText) {
        _summaryTextView.text = _project.descriptionText;
    }

    if (_project.privacyString) {
        _privacyLabel.text = _project.privacyString;
    } else {
        _privacyLabel.text = @"Public";
    }

    if (![_project.videoDescriptionUrl isEqualToString:@""] && _project.videoDescriptionUrl) {
        _noneVideoDescriptionLabel.hidden = YES;
        _viewPlayer.hidden = NO;
        _addVideoButton.hidden = YES;
        _image1.hidden = YES;
        _label1.hidden = YES;
        _label2.hidden = YES;
        if (!_lmMediaplayerView) {
            _lmMediaplayerView = [LMMediaPlayerView create];
        }
        _lmMediaplayerView.delegate = self;

        _lmMediaplayerView.frame = _viewPlayer.bounds;
        [_viewPlayer addSubview:_lmMediaplayerView];

        LMMediaItem *item = [[LMMediaItem alloc] initWithInfo:@{
                LMMediaItemInfoURLKey:[NSURL URLWithString:_project.videoDescriptionUrl],
                LMMediaItemInfoContentTypeKey:@(LMMediaItemContentTypeVideo)
        }];
        [_lmMediaplayerView.mediaPlayer playMedia:item];
    } else {
        _viewPlayer.hidden = YES;
    }
    if (![_project.user.objectId isEqualToNumber:[Together instance].user.objectId] && _project.objectId) {
        _image1.hidden = YES;
        _label1.hidden = YES;
        _label2.hidden = YES;
        _noneVideoDescriptionLabel.text = NSLocalizedString(@"description_project_bellow", nil);
        _titleTextField.enabled = false;
        _summaryTextView.editable = false;
        _createButton.enabled = false;
        _createButton.hidden = YES;
        _categoryButton.enabled = false;
        _addVideoButton.enabled = false;
    }else {
        _noneVideoDescriptionLabel.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self.navigationController.navigationBar setTranslucent:YES];

    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Pacifico" size:20]}];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.894f green:0.000f blue:0.000f alpha:1.00f]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];

//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;

}

#pragma mark - Actions

- (IBAction)closePressed:(id)sender {
    if (_popViewBackAction) {
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)saveProjectPressed:(id)sender {

    if (!_project.category) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"category_needed", nil)];
        return;
    }
    if ([_titleTextField.text isEqualToString:@""]) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"title_empty", nil)];
        return;
    }


    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

    if (_project.objectId) {
        _project.name = _titleTextField.text;
        _project.descriptionText = _summaryTextView.text;

        if (![_project.videoDescriptionUrl isEqualToString:@""]) {
            [_project exportDescriptionVideoWithFilter:nil success:^{
                [_project saveVideoDescriptionInAmazonS3WithSuccess:^{
                    [_project updateInWebfactionWithSuccess:^(Project *projectUpdated) {
                        NSLog(@"projectSaved = %@", projectUpdated);
                        [hud hide:YES];
                        ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
                        vc.project = projectUpdated;
                        [self.navigationController pushViewController:vc animated:YES];
                    } failure:^{
                        [hud hide:YES];
                        NSLog(@"falha update");
                    }];
                } failure:^{
                    [hud hide:YES];
                    NSLog(@"falha save video");
                }];
            } andFailure:^{
                [hud hide:YES];
                NSLog(@"falha export");
            }];
        } else {
            [_project updateInWebfactionWithSuccess:^(Project *projectUpdated) {
                NSLog(@"projectSaved = %@", projectUpdated);
                ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
                vc.project = projectUpdated;
                [hud hide:YES];
                [self.navigationController pushViewController:vc animated:YES];
            } failure:^{

            }];
        }

    } else {
        NSString *videoDescriptionUrl = _project.videoDescriptionUrl;

        _project.name = _titleTextField.text;
        _project.descriptionText = _summaryTextView.text;
        _project.videoUrl = @"";
        _project.audioUrl = @"";
        _project.videoDescriptionUrl = @"";
        _project.imageUrl = @"";
        _project.objectId = @0;
        _project.user = [Together instance].user;

        [_project saveInWebfactionWithSuccess:^(Project *projectSaved) {
            NSLog(@"projectSaved = %@", projectSaved);

            if (![videoDescriptionUrl isEqualToString:@""] && videoDescriptionUrl) {
                projectSaved.videoDescriptionUrl = videoDescriptionUrl;

                [projectSaved exportDescriptionVideoWithFilter:nil success:^{
                    [projectSaved saveVideoDescriptionInAmazonS3WithSuccess:^{
                        NSLog(@"projectSaved = %@", projectSaved.videoDescriptionUrl);
                        [projectSaved updateInWebfactionWithSuccess:^(Project *projectUpdated) {
                            [hud hide:YES];
                            [self presentViewControllerProjectWIthProject:projectSaved];
                        } failure:^{
                            NSLog(@"falha1");
                            [hud hide:YES];
                        }];
                    } failure:^{
                        NSLog(@"falha2");
                        [hud hide:YES];
                    }];
                } andFailure:^{
                    NSLog(@"falha3");
                    [hud hide:YES];
                }];

            } else {
                [hud hide:YES];
                [self presentViewControllerProjectWIthProject:projectSaved];
            }


        }                             failure:^{
            [hud hide:YES];
        }];
    }

}

- (void)presentViewControllerProjectWIthProject:(Project *)projectSaved {
    ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
    vc.project = projectSaved;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)categoryPressed:(id)sender {

    CategoriesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"categoriesVC"];
    vc.project = _project;

    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)privacyPressed:(id)sender {
    PrivacyTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"privacyVC"];
    vc.project = _project;

    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)addVideoPressed:(id)sender {
    ProjectDescriptionRecordViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"descriptionProjectRecordVC"];
    vc.project = _project;

    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [_project deleteInWebServiceWithSuccess:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^{

        }];

    }
}

#pragma mark - LMPlayer

- (void)mediaPlayerViewDidFinishPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media {
    NSLog(@"media = %@", media);
    [[playerView mediaPlayer] pause];
    [[playerView mediaPlayer] seekTo:0];
}

- (void)mediaPlayerViewDidStartPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media {
    NSLog(@"media = %@", media);
}

- (BOOL)mediaPlayerViewWillStartPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media {
    return YES;
}

- (void)mediaPlayerViewWillChangeState:(LMMediaPlayerView *)playerView state:(LMMediaPlaybackState)state {
    NSLog(@"state = %d", state);
}

- (void)dealloc {
    [_lmMediaplayerView.mediaPlayer stop];
    NSLog(@"dealloc New Project");
}


@end
