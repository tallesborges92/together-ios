//
//  TableViewControllerCreateProject.m
//  Together
//
//  Created by Talles  Borges  on 7/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TableViewControllerCreateProject.h"
#import "Project.h"
#import "Together.h"
#import "Project+Webfaction.h"
#import "ViewControllerProject.h"

@interface TableViewControllerCreateProject ()

@end

@implementation TableViewControllerCreateProject {
    Project *_project;
}


#pragma mark - ViewMethods
- (void)viewDidLoad
{
    [super viewDidLoad];


    
}


#pragma mark - Actions
- (IBAction)salvarProjeto:(id)sender {
    _project = [Project new];

    _project.name = _tfNome.text;
    _project.descriptionText = _tfDescricao.text;
    _project.videoUrl = @"";
    _project.audioUrl = @"";
    _project.imageUrl = @"";
    _project.objectId = @0;
    _project.user = [Together instance].user;

    [_project saveInWebfactionWithSuccess:^(Project *projectSaved) {
        NSLog(@"projectSaved = %@", projectSaved);
        ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
        vc.project = projectSaved;
        [self.navigationController pushViewController:vc animated:YES];
    }                             failure:^{

    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_tfDescricao resignFirstResponder];
    [_tfNome resignFirstResponder];
    return YES;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    OBTabBarController *tabBarController = [Together instance].obTabBarController;
//    [tabBarController setTabBarHidden:YES animated:YES];
}


@end
