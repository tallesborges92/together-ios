//
//  NewProjectViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/28/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LMMediaPlayer/LMMediaPlayerView.h>
#import "Project.h"

@interface NewProjectViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate, UIAlertViewDelegate, LMMediaPlayerViewDelegate>

#pragma mark - Model
@property Project *project;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarItem;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *projectScrollView;
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;
@property (weak, nonatomic) IBOutlet UILabel *noneVideoDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *createButton;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *addVideoButton;
@property (weak, nonatomic) IBOutlet UIView *viewPlayer;
@property (weak, nonatomic) IBOutlet UILabel *privacyLabel;

#pragma mark - Utils
@property bool popViewBackAction;

#pragma mark - Actions
- (IBAction)closePressed:(id)sender;
- (IBAction)saveProjectPressed:(id)sender;
- (IBAction)categoryPressed:(id)sender;
- (IBAction)privacyPressed:(id)sender;
- (IBAction)addVideoPressed:(id)sender;

#pragma mark - HideThings
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end
