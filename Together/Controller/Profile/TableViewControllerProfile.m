//
//  TableViewControllerProfile.m
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TableViewControllerProfile.h"
#import "Together.h"
#import "TableViewCellProfile.h"
#import "User+WebService.h"
#import "TableViewCellProject+Util.h"
#import "UIImageView+WebCache.h"
#import "MHFacebookImageViewer.h"
#import "SettingsTableViewController.h"

@interface TableViewControllerProfile ()

@end

@implementation TableViewControllerProfile {
    TableViewCellProfile *_cellProfile;
    NSMutableArray *_dataSource;
    ProjectsTypesView *_projectsTypesToolbar;
    NSNumber *_page;
    bool _noMoreProjects;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!_user) {
        _user = [Together instance].user;
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_config_profile"] style:UIBarButtonItemStylePlain target:self action:@selector(configPressed)];
        self.navigationItem.rightBarButtonItem = item;

    }
    self.tableView.tableHeaderView = [UIView new];

    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refreshControllPressed) forControlEvents:UIControlEventValueChanged];


    UINib *cellProfile = [UINib nibWithNibName:@"TableViewCellProfile" bundle:nil];
    [self.tableView registerNib:cellProfile forCellReuseIdentifier:@"imageViewProfile"];

    UINib *cellProject = [UINib nibWithNibName:@"TableViewCellProject" bundle:nil];
    [self.tableView registerNib:cellProject forCellReuseIdentifier:@"cellProject"];

    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"ProjectsTypesView" owner:nil options:nil];
    _projectsTypesToolbar = [nibContents lastObject];
    _projectsTypesToolbar.delegate = self;

}

- (void)resetConfigTable {
    _page = @1;
    _dataSource = [NSMutableArray new];
    _noMoreProjects = false;
    [self updateProjects];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self updateCellUser];

    [self resetConfigTable];
    [self updateUser];

//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:84.0/255 green:105.0/255 blue:121.0/255 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Avenir-Medium" size:16]}];
//    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setHidden:YES];
//
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

//    [self.navigationController.navigationBar setHidden:NO];
//
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Pacifico" size:20]}];
//    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.894f green:0.000f blue:0.000f alpha:1.00f]];
//    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}


#pragma mark - Util

- (void)updateProjects {
    [self.refreshControl beginRefreshing];


    switch (_projectsTypesToolbar.projectsTypesSegment.selectedSegmentIndex){

        case 0:{
            [self updateUserProjects];
            break;
        }
        case 1:{
            [self updateParticipedProjects];
            break;
        }

        case 2:{
            [self updateLikedProjects];
            break;
        }

        default:break;
    };

}

-(void)updateUser{
    [User userWithObjectId:_user.objectId withSender:[Together instance].user success:^(User *user) {
        _user = user;
        [self updateCellUser];
    } failure:^{

    }];
}

-(void) updateCellUser {
    _cellProfile = [self.tableView dequeueReusableCellWithIdentifier:@"imageViewProfile"];
    _cellProfile.vc = self;
    _cellProfile.user = _user;
    [_cellProfile.followedUsersButton setTitle:[NSString stringWithFormat:@"%@ %@",_user.totalFollowedUsers, NSLocalizedString(@"following", nil)] forState:UIControlStateNormal];
    [_cellProfile.followersButton setTitle:[NSString stringWithFormat:@"%@ %@",_user.totalFollowers, NSLocalizedString(@"followers", nil)] forState:UIControlStateNormal];
    [_cellProfile.imgProfile setupImageViewer];
    [_cellProfile.imgProfile sd_setImageWithURL:[NSURL URLWithString:_user.avatarUrl]];
    [_cellProfile.imgProfile.layer setCornerRadius:38];
    [_cellProfile.imgProfile.layer setMasksToBounds:YES];
    [_cellProfile.imgProfile.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_cellProfile.imgProfile.layer setBorderWidth:0.7];
    _cellProfile.lblNomeProfile.text = _user.name;
    if (![_user.coverUrl isEqualToString:@""]) {
        [_cellProfile.imgBlurProfile sd_setImageWithURL:[NSURL URLWithString:_user.coverUrl]];
    }else {
        _cellProfile.imgBlurProfile.image = [UIImage imageNamed:@"art_together"];
    }

    [_cellProfile.buttonSeguir setHidden:[_user.objectId isEqualToNumber:[Together instance].user.objectId]];
    [_cellProfile.configButton setHidden:![_user.objectId isEqualToNumber:[Together instance].user.objectId]];

//    [_cellProfile.imgBlurProfile sd_setImageWithURL:[NSURL URLWithString:_user.avatarUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        _cellProfile.imgBlurProfile.image = [image applyDarkEffect];
//    }];
    _cellProfile.delegate = self;
    [_cellProfile.buttonSeguir setTitle:_user.following? NSLocalizedString(@"following", nil) : NSLocalizedString(@"bt_follow", nil) forState:UIControlStateNormal];
}

#pragma mark - RefreshControl

- (void)refreshControllPressed {
    [self resetConfigTable];
    [self updateUser];
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) {
        return 1;
    } else {
        return _dataSource.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return _cellProfile;
    }

    TableViewCellProject *cell = [tableView dequeueReusableCellWithIdentifier:@"cellProject" forIndexPath:indexPath];
    cell.viewController = self;
    cell.delegate = self;
    Project *project = _dataSource[indexPath.row];
    [cell configureWithProject:project];

    return cell;
}


#pragma mark - TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 222;
    } else {
        return [TableViewCellProject defaultHeightCell];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return _projectsTypesToolbar;
    }
    return [super tableView:tableView viewForHeaderInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 44;
    }
    return [super tableView:tableView heightForHeaderInSection:section];
}

#pragma mark - ProjectsTypesDelegate
- (void)selectSegmentAtIndex:(NSInteger)index {
    NSLog(@"index = %i", index);
    [self resetConfigTable];
}


#pragma mark - TableCellProjectDeleagate

- (void)updateProject:(Project *)project {

    for (Project *pj in _dataSource) {
        if ([pj.objectId isEqualToNumber:project.objectId]) {
            pj.totalLikes = (pj.liked) ? @([pj.totalLikes intValue] - 1) : @([pj.totalLikes intValue] + 1);
            pj.liked = !pj.liked;
            [self.tableView reloadData];
            return;
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _dataSource.count - 1) {
        if (!_noMoreProjects) {
            _page = @( [_page intValue] + 1 );
            [self updateProjects];
        }
    }
}


#pragma - UserDelegate

- (void)followUser:(User *)user {
   [self updateUser];
}

- (void)configPressed {
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
//                                delegate:self
//                       cancelButtonTitle:@"Cancelar"
//                  destructiveButtonTitle:@"Logout"
//                       otherButtonTitles: nil];
//
//    [actionSheet showInView:[Together instance].obTabBarController.view];

    SettingsTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - ActionSheet Delegate

//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (buttonIndex == 0) {
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_logged"];
//
//        AppDelegate *app = [[UIApplication sharedApplication] delegate];
//        app.window.rootViewController = [self.storyboard instantiateInitialViewController];
//    }
//
//}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row == 0) return;
//    Project *project = _dataSource[indexPath.row -1];
//
//    ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
//    vc.project = project;
//    [self presentViewController:vc animated:YES completion:nil];
//}

#pragma mark - Utils
- (void)updateLikedProjects {
    [_user likedProjectsPage:_page WithSender:[Together instance].user success:^(NSArray *projects) {
        [self.refreshControl endRefreshing];
        _dataSource = [[_dataSource arrayByAddingObjectsFromArray:projects] mutableCopy];
        if (projects.count == 0) {
            _noMoreProjects = true;
        }
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    }                failure:^{
    }];
}

- (void)updateParticipedProjects {
    [_user participedProjectsPage:_page WithSender:[Together instance].user success:^(NSArray *projects) {
        _dataSource = [[_dataSource arrayByAddingObjectsFromArray:projects] mutableCopy];
        if (projects.count == 0) {
            _noMoreProjects = true;
        }
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    }                     failure:^{
    }];
}

- (void)updateUserProjects {
    [_user projectsPage:_page WithSender:[Together instance].user success:^(NSArray *projects) {
        [self.refreshControl endRefreshing];
        _dataSource = [[_dataSource arrayByAddingObjectsFromArray:projects] mutableCopy];
        if (projects.count == 0) {
            _noMoreProjects = true;
        }
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    }           failure:^{
    }];
}

@end
