//
//  TableViewControllerProfile.h
//  Together
//
//  Created by Talles  Borges  on 7/25/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "ProjectsTypesView.h"
#import "TableViewCellProject.h"
#import "TableViewCellUser.h"

@interface TableViewControllerProfile : UITableViewController <ProjectsTypesViewDelegate, TableViewCellProjectDelegate, TableViewCellUserDelegate, UIActionSheetDelegate>

@property User *user;

@end
