//
//  ViewControllerComment.m
//  Together
//
//  Created by Talles  Borges  on 9/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ViewControllerComment.h"
#import "CommentDataSource.h"
#import "Project+Webfaction.h"
#import "Comment.h"
#import "Together.h"
#import "Comment+Webfaction.h"
#import "UIAlertView+Utils.h"
#import "MBProgressHUD.h"

@interface ViewControllerComment ()

@end

@implementation ViewControllerComment {
    CommentDataSource *_commentDataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.commentsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _commentDataSource = [CommentDataSource sourceWithTableView:_commentsTableView array:[NSArray array]];

    [self updateComments];

    UITapGestureRecognizer *tap = [UITapGestureRecognizer new];
    tap.numberOfTapsRequired = 1;
    [tap addTarget:self action:@selector(tapView)];

    [self.commentsTableView addGestureRecognizer:tap];
}

- (void)tapView {
    [_commentTextField resignFirstResponder];
}

#pragma mark - Utils
- (void)updateComments {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [_project commentsWithSuccess:^(NSArray *comments) {
        _commentDataSource.array = comments;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [_commentsTableView reloadData];
    }                     failure:^{
        NSLog(@"pau");
    }];
}

#pragma mark - Actions
- (IBAction)sendPressed:(id)sender {

    if ([[Together instance] isUserDemo]) {
        [[Together instance] showAlertNeedRegister];
        return;
    }

    [_commentTextField resignFirstResponder];
    User *user = [Together instance].user;

    if (!user) {
        [UIAlertView showAlertWithString:@"Precisa estar logado."];
        return;
    }

    Comment *comment = [Comment new];
    comment.project = _project;
    comment.user = user;
    comment.text = _commentTextField.text;

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [comment saveInWebfactionWithSuccess:^(Comment *commentSaved) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self updateComments];
        _commentTextField.text = @"";
    } failure:^{
        NSLog(@"Erro ao salvar");
    }];
}

- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
