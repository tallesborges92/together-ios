//
//  ViewControllerComment.h
//  Together
//
//  Created by Talles  Borges  on 9/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface ViewControllerComment : UIViewController

#pragma mark - Model
@property Project *project;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;

#pragma mark - Actions
- (IBAction)sendPressed:(id)sender;
- (IBAction)closePressed:(id)sender;


@end
