//
//  NewUserViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/29/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewUserViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate>

#pragma mark - Actions
- (IBAction)createAccountPressed:(id)sender;
- (IBAction)addPhotoPressed:(id)sender;
- (IBAction)closePressed:(id)sender;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *userLastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *userEmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *userPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmTextField;
@property (weak, nonatomic) IBOutlet UIButton *userAvatarButton;


@end
