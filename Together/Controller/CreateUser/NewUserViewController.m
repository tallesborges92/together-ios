//
//  NewUserViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/29/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "NewUserViewController.h"
#import "User.h"
#import "User+WebService.h"
#import "AmazonS3.h"
#import "UIAlertView+Utils.h"
#import "Together.h"
#import "MBProgressHUD.h"

@interface NewUserViewController ()

@end

@implementation NewUserViewController {
    UIImage *_userAvatar;
    NSString *_avatarUrl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _avatarUrl = @"";
}

#pragma mark - Actions
- (IBAction)createAccountPressed:(id)sender {
    if ([_userEmailTextField.text isEqualToString:@""] || [_userPasswordTextField.text isEqualToString:@""] || [_userEmailTextField.text isEqualToString:@""] || [_userLastNameTextField.text isEqualToString:@""]) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"all_field_filled", nil)];
        return;
    }
    if (![_userPasswordTextField.text isEqualToString:_passwordConfirmTextField.text]) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"password_dont_match", nil)];
        return;
    }
    if (!_userAvatar) {
        [UIAlertView showAlertWithString:NSLocalizedString(@"add_a_foto", nil)];
        return;
    }


    User *user = [User new];

    user.name = [NSString stringWithFormat:@"%@ %@",_userNameTextField.text,_userLastNameTextField.text];
    user.password = _userPasswordTextField.text;
    user.email = _userEmailTextField.text;
    user.avatarUrl = @"";
    user.coverUrl = @"";
    user.facebookId = @"";

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [AmazonS3 uploadImage:_userAvatar withSuccess:^(NSString *urlInAmazon) {
        user.avatarUrl = urlInAmazon;
        [user saveInWebfactionWithSuccess:^(User *userSaved) {
            [self dismissViewControllerAnimated:YES completion:^{
                [hud hide:YES];
                [Together instance].user = userSaved;
                [[Together instance] initTabBarController];
            }];
        }                         failure:^{
            [UIAlertView showAlertWithString:@"Erro ao Salvar na WebService"];
        }];

    }          andFailure:^{
        [UIAlertView showAlertWithString:@"Erro ao fazer Upload"];
    }];
}

- (IBAction)addPhotoPressed:(id)sender {
    UIImagePickerController *vc = [UIImagePickerController new];
    vc.delegate = self;
    [vc setAllowsEditing:YES];
    [vc setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        _userAvatar = info[@"UIImagePickerControllerEditedImage"];
        [_userAvatarButton.layer setMasksToBounds:YES];
        [_userAvatarButton.layer setCornerRadius:35];
        [_userAvatarButton setImage:[_userAvatar imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    }];
}

#pragma mark - ScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_userEmailTextField resignFirstResponder];
    [_userPasswordTextField resignFirstResponder];
    [_userLastNameTextField resignFirstResponder];
    [_userNameTextField resignFirstResponder];
    [_passwordConfirmTextField resignFirstResponder];
}

@end
