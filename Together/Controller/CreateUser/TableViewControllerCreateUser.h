//
//  TableViewControllerCreateUser.h
//  Together
//
//  Created by Talles  Borges  on 7/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewControllerCreateUser : UITableViewController

#pragma mark - UITextField
@property IBOutlet UITextField *tfNome;
@property IBOutlet UITextField *tfEmail;
@property IBOutlet UITextField *tfSenha;

#pragma mark - UIImageView
@property IBOutlet UIImageView *imgViewAvatarUsuario;

#pragma mark - Actions
- (IBAction) salvarUsuario:(id)sender;

- (IBAction)fecharTela:(id)sender;
@end
