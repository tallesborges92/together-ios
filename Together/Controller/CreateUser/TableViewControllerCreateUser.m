//
//  TableViewControllerCreateUser.m
//  Together
//
//  Created by Talles  Borges  on 7/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TableViewControllerCreateUser.h"
#import "User.h"
#import "User+WebService.h"

@interface TableViewControllerCreateUser ()

@end

@implementation TableViewControllerCreateUser

#pragma mark - UIView Methods
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Actions
- (IBAction)salvarUsuario:(id)sender {

    User *user = [User new];

    user.name = _tfNome.text;
    user.password = _tfSenha.text;
    user.email = _tfEmail.text;
    user.avatarUrl = @"";
    user.facebookId = @"";

    [user saveInWebfactionWithSuccess:^(User *userSaved) {

    }                         failure:^{

    }];
}

-(IBAction) fecharTela:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
