//
//  ViewControllerStart.m
//  Together
//
//  Created by Talles  Borges  on 7/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SCRecorder/SCPlayer.h>
#import <Parse/Parse.h>
#import "ViewControllerStart.h"
#import "User.h"
#import "User+WebService.h"
#import "Together.h"
#import "FBRequestConnection.h"
#import "NewUserViewController.h"
#import "SignInViewController.h"
#import "MBProgressHUD.h"
#import "User+Dictionary.h"
#import "UIAlertView+Utils.h"

@interface ViewControllerStart ()

@end

@implementation ViewControllerStart {
    AVAsset *_asset;
}

#pragma mark - UIView Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    NSString *videoURL = [[NSBundle mainBundle] pathForResource:@"bg_login_video" ofType:@"mp4"];

    _asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:videoURL] options:nil];

    NSArray *assetKeysToLoadAndTest = @[@"playable"];

    [_asset loadValuesAsynchronouslyForKeys:assetKeysToLoadAndTest completionHandler:
            ^{
                dispatch_async(dispatch_get_main_queue(),
                        ^{
                            // IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem.
                            [self initPlayer];
                        });
            }];

}

- (void)playerItemDidReachEnd:(id)playerItemDidReachEnd {
    [_playerView.player seekToTime:kCMTimeZero];
    [_playerView.player play];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
    if (_playerView.player) {
        [_playerView.player seekToTime:kCMTimeZero];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[_playerView.player currentItem]];
    }

}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Utils


- (void)initPlayer {
    dispatch_async(dispatch_get_main_queue(), ^{
        _playerView.player = [AVPlayer playerWithPlayerItem:[[AVPlayerItem alloc] initWithAsset:_asset]];

        [_playerView.player actionAtItemEnd];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[_playerView.player currentItem]];

        [_playerView setPlayer:_playerView.player];

        [[_playerView player] play];
    });
}


- (void)loginWithUserDictionary:(NSDictionary *)user {
    User *newUser = [User new];

    NSString *host = @"https://graph.facebook.com";
    NSString *function = @"picture?redirect=1&height=200&type=normal&width=200";

    newUser.email = user[@"email"];
    newUser.facebookId = user[@"id"];
    newUser.name = user[@"name"];
    newUser.avatarUrl = [NSString stringWithFormat:@"%@/%@/%@",host,user[@"id"],function];
    newUser.password = @"";

    NSString *graphPath = [NSString stringWithFormat:@"%@?fields=cover",newUser.facebookId];
    [FBRequestConnection startWithGraphPath:graphPath completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {

        if (result[@"cover"][@"source"]) {
            newUser.coverUrl = result[@"cover"][@"source"];
        } else {
            newUser.coverUrl = @"";
        }
        [newUser saveInWebfactionWithSuccess:^(User *userSaved) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Together instance].user = userSaved;

            [[NSUserDefaults standardUserDefaults] setObject:[userSaved toUserDefaults] forKey:@"user_logged"];
//        UITabBarController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tabPrincipal"];
            [[Together instance] initTabBarController];
//        [self presentViewController:vc animated:YES completion:nil];
        }                            failure:^{
            NSLog(@"falha");
        }];
    }];

}

-(IBAction) loginFb:(id)sender{


    if (FBSession.activeSession.state == FBSessionStateOpen
            || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        [FBSession.activeSession closeAndClearTokenInformation];
    } else {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
                                           allowLoginUI:YES
                                      completionHandler:
                                              ^(FBSession *session, FBSessionState state, NSError *error) {
                                                  if (!error && state == FBSessionStateOpen){
                                                      NSLog(@"Session opened");
                                                      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                                      [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                          if (!error) {
                                                              NSLog(@"user info: %@", result);
                                                              [self loginWithUserDictionary:result];
                                                          } else {
                                                          }
                                                      }];
                                                  }
                                              }];
    }

}

- (IBAction)registerPressed:(id)sender {
    NewUserViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newUserVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)signinPressed:(id)sender {
    SignInViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"signInVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)skipPressed:(id)sender {

    User *user = [User new];
    user.name = @"";
    user.avatarUrl = @"";
    user.facebookId = @"";
    user.email = @"demo";
    user.password = @"demo";
    user.coverUrl = @"";

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [user loginWithSuccess:^(User *userLogged) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [Together instance].user = userLogged;
//        [[NSUserDefaults standardUserDefaults] setObject:[userSaved toUserDefaults] forKey:@"user_logged"];
//        UITabBarController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tabPrincipal"];
        [[Together instance] initTabBarController];

    } failure:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [UIAlertView showAlertWithString:@"Usuario ou senha inválidos"];
    }];

//    [[Together instance] initTabBarController];
}
@end
