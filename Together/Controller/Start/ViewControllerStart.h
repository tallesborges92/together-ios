//
//  ViewControllerStart.h
//  Together
//
//  Created by Talles  Borges  on 7/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBLoginView.h"
#import "OBTabBarController.h"
#import "PlayerView.h"

@interface ViewControllerStart : UIViewController
#pragma mark - VIew
@property (weak, nonatomic) IBOutlet PlayerView *playerView;

#pragma mark - Button
@property IBOutlet UIButton *btnLogin;
@property IBOutlet UIButton *btnNovoCadastro;
@property IBOutlet UIButton *btnEntrarSemRegistro;

#pragma mark - Actions
- (IBAction)registerPressed:(id)sender;
- (IBAction)loginFb:(id)sender;
- (IBAction)signinPressed:(id)sender;
- (IBAction)skipPressed:(id)sender;

@end
