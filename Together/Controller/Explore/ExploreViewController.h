//
//  ExploreViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITableView *exploreTableView;

@end
