//
//  ExploreSearchTableViewController.m
//  Together
//
//  Created by Talles  Borges  on 11/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ExploreSearchTableViewController.h"
#import "Together.h"
#import "User+WebService.h"
#import "TableViewCellProject+Util.h"
#import "User+CellUtils.h"
#import "TableViewControllerProfile.h"

@interface ExploreSearchTableViewController ()

@end

@implementation ExploreSearchTableViewController {
    NSArray *_users;
    NSArray *_projects;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [_searchTextField becomeFirstResponder];

    [self.navigationItem.titleView setTintColor:[UIColor blueColor]];
    
    _users = [NSArray new];
    _projects = [NSArray new];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? _users.count : _projects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        TableViewCellProject *cell = [tableView dequeueReusableCellWithIdentifier:@"cellProject"];
        if (!cell) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableViewCellProject" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = topLevelObjects[0];
        }

        Project *project = _projects[indexPath.row];

        [cell configureWithProject:project];
        cell.viewController = self;
        cell.delegate = self;

        return cell;
    } else {
        TableViewCellUser *cell = [tableView dequeueReusableCellWithIdentifier:@"cellUser"];

        if (!cell) {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableViewCellUser" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = topLevelObjects[0];
        }

        User *user = _users[indexPath.row];
        [user configureUserCell:cell];

        cell.delegate = self;

        return cell;
    }
}


#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSString *search = [textField.text stringByReplacingCharactersInRange:range withString:string];

    [[Together instance].user search:search success:^(NSArray *users, NSArray *projects) {
        _users = users;
        _projects = projects;
        [self.tableView reloadData];
    } failure:^{

    }];

    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0) ? [[User cellSize] floatValue] : [TableViewCellProject defaultHeightCell];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return (section == 0) ? NSLocalizedString(@"users", nil) : NSLocalizedString(@"projects", nil);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        User *user = _users[indexPath.row];
        TableViewControllerProfile *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
        vc.user = user;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark- TableViewCEllProject

- (void)updateProject:(Project *)project {

    for (Project *pj in _projects) {
        if ([pj.objectId isEqualToNumber:project.objectId]) {
            pj.totalLikes = (pj.liked) ? @([pj.totalLikes intValue] - 1) : @([pj.totalLikes intValue] + 1);
            pj.liked = !pj.liked;
            [self.tableView reloadData];
            return;
        }
    }
}

#pragma mark - TableCellUser

- (void)followUser:(User *)user {

    for (User *u in _users) {
        if ([u.objectId isEqualToNumber:user.objectId]) {
            if (u.following) {
                u.totalFollowers = @([u.totalFollowers intValue] - 1);
            } else {
                u.totalFollowers = @([u.totalFollowers intValue] + 1);
            }
            u.following = !u.following;
            [self.tableView reloadData];
            return;
        }
    }

}


@end
