//
//  ExploreSearchTableViewController.h
//  Together
//
//  Created by Talles  Borges  on 11/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellProject.h"
#import "TableViewCellUser.h"

@interface ExploreSearchTableViewController : UITableViewController <UITextFieldDelegate, TableViewCellProjectDelegate, TableViewCellUserDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@end
