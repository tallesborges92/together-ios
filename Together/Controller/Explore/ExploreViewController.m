//
//  ExploreViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ExploreViewController.h"
#import "TBCategory.h"
#import "TBCategory+WebService.h"
#import "CategoryTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CategoryProjectsViewController.h"


@interface ExploreViewController ()

@end

@implementation ExploreViewController {
    NSArray *_categories;
    UIRefreshControl *_refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UITableViewController *tbvController = [UITableViewController new];
    tbvController.tableView = _exploreTableView;

    [self.navigationController.navigationBar setTranslucent:NO];

    _refreshControl = [UIRefreshControl new];
    [_refreshControl addTarget:self action:@selector(refreshControllChanged) forControlEvents:UIControlEventValueChanged];

    tbvController.refreshControl = _refreshControl;

    [self updateCategories];

}

#pragma mark - Utils
- (void)updateCategories {
    [_refreshControl beginRefreshing];
    [TBCategory categoriesWithSuccess:^(NSArray *categories) {
        _categories = categories;
        [_exploreTableView reloadData];
        [_refreshControl endRefreshing];
    } failure:^{
        [_refreshControl endRefreshing];
    }];
}


#pragma mark - Refresh Controll
- (void)refreshControllChanged {
    [self updateCategories];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TBCategory *category = _categories[indexPath.section];

    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell"];
    [cell.categoryNameLabel setText:category.name];
    [cell.categoryBackgroundImage sd_setImageWithURL:[NSURL URLWithString:category.image_url]];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    TBCategory *category = _categories[indexPath.section];

    CategoryProjectsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"categoryProjectsVC"];
    vc.category = category;

    [self.navigationController pushViewController:vc animated:YES];
}


@end
