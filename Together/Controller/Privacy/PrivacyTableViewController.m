//
//  PrivacyTableViewController.m
//  Together
//
//  Created by Talles  Borges  on 12/6/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "PrivacyTableViewController.h"
#import "UserPrivacyTableViewCell.h"
#import "PrivacyTypeTableViewCell.h"
#import "Together.h"
#import "User+WebService.h"

@interface PrivacyTableViewController ()

@end

@implementation PrivacyTableViewController {
    NSArray *_array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.navigationItem setTitle:NSLocalizedString(@"title_privacy", nil)];


    _array = [NSArray array];

    [[Together instance].user followedUsersWithSender:[Together instance].user success:^(NSArray *users) {
        _array = users;
        [self.tableView reloadData];
    } failure:^{

    }];

}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == 0) ? 3 : _array.count;;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        PrivacyTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellType" forIndexPath:indexPath];
        switch (indexPath.row){
            case 0:
                cell.nameLabel.text = NSLocalizedString(@"public", nil);
                [cell.typeImage setImage:[UIImage imageNamed:@"privacy_icons_public"]];
                break;
            case 1:
                cell.nameLabel.text = NSLocalizedString(@"followers", nil);
                [cell.typeImage setImage:[UIImage imageNamed:@"privacy_icons_followers"]];
                break;
            case 2:
                cell.nameLabel.text = NSLocalizedString(@"only_me", nil);
                [cell.typeImage setImage:[UIImage imageNamed:@"privacy_icons_only"]];
                break;

            default:break;
        }
        return cell;
    } else {
        UserPrivacyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellUser" forIndexPath:indexPath];
        User *user = _array[indexPath.row];
        [cell.avatarImage sd_setImageWithURL:[NSURL URLWithString:user.avatarUrl]];
        [cell.nameLabel setText:user.name];

        return cell;
    }

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    return (section == 0) ? @"" : NSLocalizedString(@"followers", nil);
}

#pragma mark - Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                _project.privacyString = NSLocalizedString(@"public", nil);
                break;
            case 1:
                _project.privacyString = NSLocalizedString(@"followers", nil);
                break;
            case 2:
                _project.privacyString = NSLocalizedString(@"only_me", nil);
                break;
            default:break;
        }
        [self.navigationController popViewControllerAnimated:YES];
    } else {

    }

    if (tableView.indexPathsForSelectedRows.count > 0) {
        UIBarButtonItem *buttonItem = [UIBarButtonItem new];
        buttonItem.title = @"OK";
        buttonItem.target = self;
        buttonItem.action = @selector(okPressed);
        self.navigationItem.rightBarButtonItem = buttonItem;
    }

}

- (void)okPressed {
    _project.privacyString = [NSString stringWithFormat:@"%d followers",self.tableView.indexPathsForSelectedRows.count];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.indexPathsForSelectedRows.count == 0) {
        self.navigationItem.rightBarButtonItem = nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0) ? 40 : 50;
}


@end
