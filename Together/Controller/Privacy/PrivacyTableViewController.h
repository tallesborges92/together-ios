//
//  PrivacyTableViewController.h
//  Together
//
//  Created by Talles  Borges  on 12/6/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface PrivacyTableViewController : UITableViewController

@property Project *project;

@end
