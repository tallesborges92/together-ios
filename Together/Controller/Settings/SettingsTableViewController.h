//
//  SettingsTableViewController.h
//  Together
//
//  Created by Talles  Borges  on 12/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SettingsTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

@end
