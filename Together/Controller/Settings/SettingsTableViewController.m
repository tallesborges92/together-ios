//
//  SettingsTableViewController.m
//  Together
//
//  Created by Talles  Borges  on 12/8/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "SettingsTableViewController.h"
#import "AppDelegate.h"
#import "TermsViewController.h"
#import "UIAlertView+Utils.h"
#import "Together.h"

@interface SettingsTableViewController ()

@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {

        case 0:
            switch (indexPath.row) {
                case 0:{
                    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    break;
                }case 1:{
                    TermsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"termsVC"];
                    [self.navigationController pushViewController:vc animated:YES];
                    break;
                }case 2: {
                    if ([MFMailComposeViewController canSendMail]) {
                        MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
                        [vc.navigationBar setTintColor:[UIColor whiteColor]];
                        vc.mailComposeDelegate = self;
                        [vc setToRecipients:@[@"feedback@togetherapp.com.br"]];
                        [self.view.window.rootViewController presentViewController:vc animated:YES completion:nil];
                    }else {
                        [UIAlertView showAlertWithString:NSLocalizedString(@"configure_email", nil)];
                    }

                    break;
                }

                default:break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:{
                    [Together goToMainScreen];
                    break;
                }
                default:break;
            }
            break;

        default:break;
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end
