//
//  ViewControllerFeed.h
//  Together
//
//  Created by Talles  Borges  on 7/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellProject.h"

@interface ViewControllerFeed : UIViewController <UITableViewDataSource,UITableViewDelegate, TableViewCellProjectDelegate>

#pragma mark - TableView
@property IBOutlet UITableView *tableViewFeed;

@end
