//
//  ViewControllerFeed.m
//  Together
//
//  Created by Talles  Borges  on 7/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ViewControllerFeed.h"
#import "Project+Webfaction.h"
#import "TableViewCellProject+Util.h"
#import "Together.h"
#import "User+WebService.h"

@interface ViewControllerFeed ()

@end

@implementation ViewControllerFeed {
    NSMutableArray *_projects;
    UIRefreshControl *_refreshControl;
    NSNumber *_page;
    bool _noMoreProjects;
}

#pragma mark - UIView Methods

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableViewFeed.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = _tableViewFeed;

    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(refreshChanged) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = _refreshControl;
    [self resetConfigTable];
}

- (void)refreshChanged {
    [self resetConfigTable];
}

- (void)resetConfigTable {
    _page = @1;
    _projects = [NSMutableArray new];
    _noMoreProjects = false;
    [self updateFeed];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self resetConfigTable];
}


#pragma mark - Actions
- (void)updateFeed {
    [_refreshControl beginRefreshing];
    if ([Together instance].user) {
        [[Together instance].user feedPage:_page WithSuccess:^(NSArray *projects) {
            _projects = [[_projects arrayByAddingObjectsFromArray:projects] mutableCopy];
            if (projects.count == 0) {
                _noMoreProjects = true;
            }
            [_tableViewFeed reloadData];
            [_refreshControl endRefreshing];
        }                          failure:^{
            [_refreshControl endRefreshing];

        }];
    } else {
        [Project projectsWithSuccess:^(NSArray *projects) {
            _projects = [projects mutableCopy];
            [_tableViewFeed reloadData];
            [_refreshControl endRefreshing];
        } failure:^{
            [_refreshControl endRefreshing];
        }];
    }
}


#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _projects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCellProject *cell = [tableView dequeueReusableCellWithIdentifier:@"cellProject"];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"TableViewCellProject" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"cellProject"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellProject"];
    }

    Project *project = _projects[indexPath.row];

    [cell configureWithProject:project];
    cell.delegate = self;
    cell.viewController = self;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [TableViewCellProject defaultHeightCell];
}


#pragma mark - UITableViewDelegate
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    Project *project = _projects[indexPath.row];
//
//    ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
//    vc.project = project;
//    [self.navigationController pushViewController:vc animated:YES];
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _projects.count - 1) {
        if (!_noMoreProjects) {
            _page = @( [_page intValue] + 1 );
            [self updateFeed];
        }
    }
}


#pragma mark - UIScrollViewDelegate
//- (void)scrollViewDidEndDragging:(UIScrollView *)projectScrollView willDecelerate:(BOOL)decelerate {
//    [self playProjectAtCenter];
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)projectScrollView {
//    [self playProjectAtCenter];
//
//}
//
//- (void)playProjectAtCenter {
//
//    NSArray *indexpaths = [self.tableViewFeed indexPathsForVisibleRows];
//
//    for (NSIndexPath *indexpath in indexpaths) {
//        CGRect cellRect = [self.tableViewFeed rectForRowAtIndexPath:indexpath];
//        cellRect = [self.tableViewFeed convertRect:cellRect toView:self.tableViewFeed.superview];
//        BOOL completelyVisible = CGRectContainsRect(self.tableViewFeed.frame, cellRect);
//        if (completelyVisible) {
//            TableViewCellProject *cell = (TableViewCellProject *) [self.tableViewFeed cellForRowAtIndexPath:indexpath];
//            Project *project = _projects[indexpath.row];
//            if (![project.videoUrl isEqualToString:@""]) {
//                if(cell.playerController.playbackState == MPMoviePlaybackStatePlaying)
//                    return;
//                [cell.playerController setContentURL:[NSURL URLWithString:project.videoUrl]];
//                [cell.playerController play];
//            }
//            break;
//        }
//    }
//
//}

#pragma mark - Rotate

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - TableCellProjectDeleagate

- (void)updateProject:(Project *)project {

    for (Project *pj in _projects) {
        if ([pj.objectId isEqualToNumber:project.objectId]) {
            pj.totalLikes = (pj.liked) ? @([pj.totalLikes intValue] - 1) : @([pj.totalLikes intValue] + 1);
            pj.liked = !pj.liked;
            [self.tableViewFeed reloadData];
            return;
        }
    }
}


@end
