//
//  TermsViewController.h
//  Together
//
//  Created by Talles  Borges  on 12/9/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *termsWebView;

@end
