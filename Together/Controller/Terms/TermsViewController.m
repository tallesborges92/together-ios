//
//  TermsViewController.m
//  Together
//
//  Created by Talles  Borges  on 12/9/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_termsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://togetherapp.com.br/terms.html"]]];

}

@end
