//
//  UploadPreviewViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/24/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import "UploadPreviewViewController.h"
#import "Project+FileUtils.h"
#import "Project+MusicDownload.h"
#import "SCFilterSelectorViewInternal.h"

@interface UploadPreviewViewController (){
    SCPlayer *_player;
}


@end

@implementation UploadPreviewViewController {
    AVMutableComposition *_composition;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    if (![_scene.project.audioUrl isEqualToString:@""] && _scene) {
        if ([_scene.project localAudioExist]) {
            [self loadPreview];
        } else {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Sincronizando Musica";
            [_scene.project downloadMusicWithSuccess:^{
                [self loadPreview];
                [hud hide:YES];
            } andFailure:^{
                [hud hide:YES];
            }];
        }
    } else {
        [self loadPreview];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [_player setItemByAsset:_composition];
}


- (void)loadPreview {
    AVAsset *asset = [AVAsset assetWithURL:_fileUrl];

    NSLog(@"%f", CMTimeGetSeconds(asset.duration));

    _composition = [AVMutableComposition composition];

    AVMutableCompositionTrack *videoTrack = [_composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *audioVideoTrack = [_composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *musicTrack = [_composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];

    NSArray *mediaTypeVideos = [asset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *clipVideoTrack = mediaTypeVideos[0];

    NSArray *mediaTypeAudios = [asset tracksWithMediaType:AVMediaTypeAudio];
    AVAssetTrack *clipAudioTrack = mediaTypeAudios[0];

    CMTimeRange timeRangeInAsset = CMTimeRangeMake(kCMTimeZero,asset.duration);

    [videoTrack insertTimeRange:timeRangeInAsset ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    [audioVideoTrack insertTimeRange:timeRangeInAsset ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];

    if (![_scene.project.audioUrl isEqualToString:@""] && _scene) {
        AVAsset *assetMusica = [AVAsset assetWithURL:[_scene.project pathAudioURL]];

        NSArray *musicTypeAudios = [assetMusica tracksWithMediaType:AVMediaTypeAudio];
        if (musicTypeAudios.count > 0) {
            AVAssetTrack *musicAudioTrack = musicTypeAudios[0];

            CMTimeRange musicRange = CMTimeRangeMake(CMTimeMakeWithSeconds([_scene.start floatValue], NSEC_PER_SEC), asset.duration);

            [musicTrack insertTimeRange:musicRange ofTrack:musicAudioTrack atTime:kCMTimeZero error:nil];
        }
    }

//    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:composition];
//
//    self.previewView.player = [AVPlayer playerWithPlayerItem:item];
//    [self.previewView setPlayer:self.previewView.player];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(playerItemDidReachEnd:)
//                                                 name:AVPlayerItemDidPlayToEndTimeNotification
//                                               object:[self.previewView.player currentItem]];

    /// SCVideo With Filter
    self.previewView.refreshAutomaticallyWhenScrolling = YES;
    self.previewView.contentMode = UIViewContentModeScaleAspectFit;

    self.previewView.filterGroups = @[
            [NSNull null],
            [SCFilterGroup filterGroupWithFilter:[SCFilter filterWithName:@"CIPhotoEffectNoir"]],
            [SCFilterGroup filterGroupWithFilter:[SCFilter filterWithName:@"CIPhotoEffectChrome"]],
            [SCFilterGroup filterGroupWithFilter:[SCFilter filterWithName:@"CIPhotoEffectInstant"]],
            [SCFilterGroup filterGroupWithFilter:[SCFilter filterWithName:@"CIPhotoEffectTonal"]],
            [SCFilterGroup filterGroupWithFilter:[SCFilter filterWithName:@"CIPhotoEffectFade"]],
            // Adding a filter created using CoreImageShop
            [SCFilterGroup filterGroupWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"a_filter" withExtension:@"cisf"]]
    ];

    _player = [SCPlayer player];
    _player.CIImageRenderer = self.previewView;
    _player.delegate = self;

}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [_playButton setHidden:NO];
}

- (IBAction)acceptPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        _savePreview([self.previewView.selectedFilterGroup copy]);
    }];
}

- (IBAction)backPressed:(id)sender {
    _cancelPreview();
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)playPressed:(id)sender {
//    [_previewView.player seekToTime:kCMTimeZero];
//    [_previewView.player play];
    [_player play];
    [_playButton setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_player pause];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (void)dealloc {
    self.previewView = nil;
    [_player pause];
    _player = nil;
}

#pragma mark - PlayerDelegate
- (void)player:(SCPlayer *)player didReachEndForItem:(AVPlayerItem *)item {
    [_playButton setHidden:NO];
    [_player seekToTime:kCMTimeZero];
}

@end
