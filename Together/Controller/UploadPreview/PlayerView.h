//
//  PlayerView.h
//  TesteDeScrollViewTimeLine
//
//  Created by Talles  Borges  on 9/5/14.
//  Copyright (c) 2014 TallesBorges. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PlayerView : UIView

@property AVPlayer *player;

@end
