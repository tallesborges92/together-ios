//
//  PlayerView.m
//  TesteDeScrollViewTimeLine
//
//  Created by Talles  Borges  on 9/5/14.
//  Copyright (c) 2014 TallesBorges. All rights reserved.
//

#import "PlayerView.h"

@implementation PlayerView

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayer*)player {
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)[self layer] setPlayer:player];
    AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self layer];
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
}


@end
