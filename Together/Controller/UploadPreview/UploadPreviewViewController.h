//
//  UploadPreviewViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/24/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerView.h"
#import "Scene.h"
#import "SCSwipeableFilterView.h"

@interface UploadPreviewViewController : UIViewController <SCPlayerDelegate>

#pragma mark - Model
@property Scene *scene;
@property NSURL *fileUrl;

#pragma mark - Blocks
@property (nonatomic, copy) void (^ savePreview)(SCFilterGroup *filterGroup);
@property (nonatomic, copy) void (^ cancelPreview)();

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet SCSwipeableFilterView *previewView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIImageView *turnPhoneImageView;

#pragma mark - Actions
- (IBAction)acceptPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)playPressed:(id)sender;

@end
