//
//  TableViewControllerCreateScene.m
//  Together
//
//  Created by Talles  Borges  on 7/20/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import "TableViewControllerCreateScene.h"
#import "Scene.h"
#import "Scene+Webfaction.h"
#import "Scene+ImageExport.h"
#import "Scene+AmazonS3.h"

@interface TableViewControllerCreateScene ()

@end

@implementation TableViewControllerCreateScene {
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Actions
- (IBAction)salvarCena:(id)sender {

    [self hideKeyBoard];

    Scene *scene = [Scene new];
    scene.name = _tfName.text;
    scene.descriptionText = _tfDescription.text;
    scene.project = _project;
    scene.imageUrl = @"";
    scene.time = @(_duracaoSlide.value);

    NSNumber *newStart = @(0);
    for (Scene *c in _scenes) {
        NSNumber *inicio = c.start;
        NSNumber *duracao = c.time;

        if ([inicio floatValue] + [duracao floatValue] > [newStart floatValue]) {
            newStart = @([inicio floatValue] + [duracao floatValue]);
        }
    }
    scene.start = newStart;

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Criando cena";

    [scene saveInWebfactionSuccess:^(Scene *sceneSaved) {
        NSLog(@"salvo webfaction");
        [sceneSaved saveImageLocal:_imgViewScene.image WithSucess:^{
            NSLog(@"salvo local");
            [sceneSaved saveImageInAmazonS3WithSuccess:^(NSString *imageUrl) {
                NSLog(@"image salva na webfaction");
                [sceneSaved updateInWebfactionSuccess:^(Scene *sceneUpdated) {
                    NSLog(@"cena atualizada");
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hide:YES];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                } failure:^{
                    [hud hide:YES];
                }];
            } andFailure:^{
                [hud hide:YES];
            }];

        } andFailure:^{
            [hud hide:YES];
        }];
    } failure:^{
        [hud hide:YES];
    }];
}

- (void)hideKeyBoard {
    [_tfDescription resignFirstResponder];
    [_tfDescription resignFirstResponder];
}


- (IBAction)adicionarImagem:(id)sender {

    [self hideKeyBoard];

    UIImagePickerController *pickerController = [UIImagePickerController new];
    pickerController.allowsEditing = YES;
    pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerController.delegate = self;

    [self presentViewController:pickerController animated:YES completion:nil];
}

- (IBAction)duracaoChanged:(id)sender {
    _duracaoLabel.text = [NSString stringWithFormat:@"%.2f s", _duracaoSlide.value];
}

#pragma mark - ImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        [_imgViewScene setImage:info[UIImagePickerControllerEditedImage]];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
