//
//  TableViewControllerCreateScene.h
//  Together
//
//  Created by Talles  Borges  on 7/20/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface TableViewControllerCreateScene : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
#pragma mark - Model
@property Project *project;
@property NSArray *scenes;

#pragma mark UIImageViewScene
@property (weak, nonatomic) IBOutlet UIImageView *imgViewScene;

#pragma mark - UITextField
@property IBOutlet UITextField *tfName;
@property IBOutlet UITextField *tfDescription;

#pragma mark - UILabel
@property (weak, nonatomic) IBOutlet UILabel *duracaoLabel;

#pragma mark - UISlider
@property (weak, nonatomic) IBOutlet UISlider *duracaoSlide;

#pragma mark - Actions
-(IBAction) salvarCena:(id)sender;
- (IBAction)adicionarImagem:(id)sender;
- (IBAction)duracaoChanged:(id)sender;

@end
