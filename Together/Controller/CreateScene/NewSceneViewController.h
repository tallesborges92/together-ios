//
//  NewSceneViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/30/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"
#import "Scene.h"
#import "StoryboardViewController.h"

@interface NewSceneViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate, StoryboardViewControllerDelegate, UIActionSheetDelegate>

#pragma mark - Model
@property Project *project;
@property NSArray *scenes;
@property Scene *scene;

#pragma mark - Button
@property (weak, nonatomic) IBOutlet UIButton *addStoryboardButto;
@property (weak, nonatomic) IBOutlet UIButton *createButton;

#pragma mark - TextField
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;

#pragma mark - Label
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewTitleLabel;

#pragma mark - UISlide
@property (weak, nonatomic) IBOutlet UISlider *durationSlider;

#pragma mark - Actions
- (IBAction)backPressed:(id)sender;
- (IBAction)addStoryboardPressed:(id)sender;
- (IBAction)durationChanged:(id)sender;
- (IBAction)createScenePressed:(id)sender;

#pragma mark - ThingsToHideShow
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UILabel *text1Label;
@property (weak, nonatomic) IBOutlet UILabel *text2Label;
@property (weak, nonatomic) IBOutlet UIImageView *imageStoryboard;

@end
