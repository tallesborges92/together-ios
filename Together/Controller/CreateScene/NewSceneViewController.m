//
//  NewSceneViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/30/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "NewSceneViewController.h"
#import "MBProgressHUD.h"
#import "Scene+Webfaction.h"
#import "Scene+AmazonS3.h"
#import "Scene+ImageExport.h"
#import "Together.h"
#import "UIButton+TBButtonFont.h"

@interface NewSceneViewController ()

@end

@implementation NewSceneViewController {
    UIActionSheet *_actionSheet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_scene) {
        [_createButton setTitlePacifico:NSLocalizedString(@"update_scene", nil )];
        [_viewTitleLabel setText:NSLocalizedString(@"edit_scene", nil)];
        _summaryTextView.text = _scene.descriptionText;
        _durationSlider.value = [_scene.time floatValue];
        [self durationChanged:nil];
//        [_addStoryboardButto.layer setMasksToBounds:YES];
//        [_addStoryboardButto.layer setCornerRadius:35];
//        [_addStoryboardButto setImage:[_imageStoryboard imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_imageStoryboard sd_setImageWithURL:[[NSURL alloc] initWithString:_scene.imageUrl]];
        [self hideThings];
        [_durationSlider setEnabled:NO];

        if (![_scene.project.user.objectId isEqualToNumber:[Together instance].user.objectId]) {
            _summaryTextView.editable = false;
            _addStoryboardButto.enabled = false;
            _createButton.enabled = false;
        }

    } else {
        [_createButton setTitlePacifico:NSLocalizedString(@"create_scene", nil )];
    }
}

#pragma mark - Actions
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addStoryboardPressed:(id)sender {


    _actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"select_from_frame", nil)
                                delegate:self
                       cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                  destructiveButtonTitle:nil
                       otherButtonTitles:NSLocalizedString(@"galery", nil), NSLocalizedString(@"frames", nil),nil];

    [_actionSheet showInView:self.view];

}

- (IBAction)durationChanged:(id)sender {
    _durationLabel.text = [NSString stringWithFormat:@"%.1f", _durationSlider.value];
}

- (IBAction)createScenePressed:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"creating_scene", nil);

    if (_scene) {
        _scene.name = @"";
        _scene.descriptionText = _summaryTextView.text;
        if (_imageStoryboard) {
            [_scene saveImageLocal:_imageStoryboard.image WithSucess:^{
                [_scene saveImageInAmazonS3WithSuccess:^(NSString *imageUrl) {
                    [_scene updateInWebfactionSuccess:^(Scene *sceneUpdated) {
                        NSLog(@"cena atualizada");
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hide:YES];
                            [self dismissViewControllerAnimated:YES completion:nil];
                        });
                    } failure:^{
                        [hud hide:YES];
                    }];
                } andFailure:^{

                }];
            } andFailure:^{

            }];
        } else {
            [_scene updateInWebfactionSuccess:^(Scene *sceneUpdated) {
                NSLog(@"cena atualizada");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hide:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
            } failure:^{
                [hud hide:YES];
            }];
        }
    } else {
        Scene *scene = [Scene new];
        scene.name = @"";
        scene.descriptionText = _summaryTextView.text;
        scene.project = _project;
        scene.imageUrl = @"";
        scene.time = @(_durationSlider.value);

        NSNumber *newStart = @(0);
        for (Scene *c in _scenes) {
            NSNumber *inicio = c.start;
            NSNumber *duracao = c.time;

            if ([inicio floatValue] + [duracao floatValue] > [newStart floatValue]) {
                newStart = @([inicio floatValue] + [duracao floatValue]);
            }
        }
        scene.start = newStart;

        [scene saveInWebfactionSuccess:^(Scene *sceneSaved) {
            NSLog(@"salvo webfaction");
            if (!_imageStoryboard.image) {
                _imageStoryboard.image = [UIImage imageNamed:@"bkg_scene"];
            }
            [sceneSaved saveImageLocal:_imageStoryboard.image WithSucess:^{
                NSLog(@"salvo local");
                [sceneSaved saveImageInAmazonS3WithSuccess:^(NSString *imageUrl) {
                    NSLog(@"image salva na webfaction");
                    [sceneSaved updateInWebfactionSuccess:^(Scene *sceneUpdated) {
                        NSLog(@"cena atualizada");
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hide:YES];
                            [self dismissViewControllerAnimated:YES completion:nil];
                        });
                    } failure:^{
                        [hud hide:YES];
                    }];
                } andFailure:^{
                    [hud hide:YES];
                }];
            } andFailure:^{
                [hud hide:YES];
            }];
        } failure:^{
            [hud hide:YES];
        }];
    }

}

#pragma mark - ImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        [self configImageStoryboard:info[@"UIImagePickerControllerEditedImage"]];
    }];
}

#pragma mark - StoryBoardDelegate
- (void)selectedStoryboard:(Storyboard *)storyboard withImage:(UIImage *)image {
    [self configImageStoryboard:image];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 0) {
        UIImagePickerController *pickerController = [UIImagePickerController new];
        pickerController.allowsEditing = YES;
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        pickerController.delegate = self;

        [self presentViewController:pickerController animated:YES completion:nil];
    } else if(buttonIndex == 1) {
        StoryboardViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"storyboardVC"];
        vc.delegate = self;
        [self presentViewController:vc animated:YES completion:nil];
    }

}

-(void) configImageStoryboard:(UIImage *)image{
    _imageStoryboard.image = image;

    [self hideThings];
}

- (void)hideThings {
    _imageStoryboard.hidden = NO;
    _image1.hidden = YES;
    _text1Label.hidden = YES;
    _text2Label.hidden = YES;
}

@end
