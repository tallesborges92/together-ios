//
//  CategoriesViewController.m
//  Together
//
//  Created by Talles  Borges  on 11/5/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "CategoriesViewController.h"
#import "CategoryCollectionViewCell.h"
#import "TBCategory+WebService.h"

@interface CategoriesViewController ()

@end

@implementation CategoriesViewController {
    NSArray *_array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [TBCategory categoriesWithSuccess:^(NSArray *categories) {
        _array = categories;
        [_categoriesCollectionView reloadData];

    } failure:^{

    }];

}


#pragma mark - DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    CategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    TBCategory *category = _array[indexPath.row];

    [cell.categoryImageView sd_setImageWithURL:[NSURL URLWithString:category.image_url]];
    [cell.categoryNameLabel setText:category.name];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    TBCategory *category = _array[indexPath.row];
    _project.category = category;
    [self.navigationController popViewControllerAnimated:YES];
}


@end
