//
//  CategoryProjectsViewController.h
//  Together
//
//  Created by Talles  Borges  on 11/6/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBCategory.h"
#import "TableViewCellProject.h"
#import "CategoryTypeView.h"

@interface CategoryProjectsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, TableViewCellProjectDelegate>


#pragma mark - Model
@property TBCategory *category;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITableView *projectsTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeSegmentControl;

#pragma mark - Actions
- (IBAction)typeProjectChanged:(id)sender;

@end
