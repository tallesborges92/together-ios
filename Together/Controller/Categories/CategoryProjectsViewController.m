//
//  CategoryProjectsViewController.m
//  Together
//
//  Created by Talles  Borges  on 11/6/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "CategoryProjectsViewController.h"
#import "TableViewCellProject+Util.h"
#import "TBCategory+WebService.h"
#import "Together.h"

@interface CategoryProjectsViewController ()

@end

@implementation CategoryProjectsViewController{
    NSArray *_projects;
    UIRefreshControl *_refreshControl;
    NSNumber *_page;
    bool _noMoreProjects;
}


#pragma mark - Views


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UITableViewController *tbvController = [UITableViewController new];
    tbvController.tableView = _projectsTableView;

    _refreshControl = [UIRefreshControl new];
    [_refreshControl addTarget:self action:@selector(refreshControllChanged) forControlEvents:UIControlEventValueChanged];

    tbvController.refreshControl = _refreshControl;

//    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CategoryTypeView" owner:nil options:nil];
//
//    _categoryTypeView = [nibContents lastObject];
//    _categoryTypeView.delegate = self;

    self.navigationItem.title = _category.name;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self resetConfigTable];

}

- (void)resetConfigTable {
    _page = @1;
    _projects = [NSMutableArray new];
    _noMoreProjects = false;
    [self updateProjects];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

}

#pragma mark - Utils

- (void)updateProjects {
    [_refreshControl beginRefreshing];
    [_category doneProjectsPage:_page WithSender:[Together instance].user type:@(_typeSegmentControl.selectedSegmentIndex) success:^(NSArray *projects) {
        _projects = [_projects arrayByAddingObjectsFromArray:projects];
        if (projects.count == 0) {
            _noMoreProjects = true;
        }
        [_refreshControl endRefreshing];
        [_projectsTableView reloadData];
    }                         failure:^{
        [_refreshControl endRefreshing];
    }];
}

#pragma mark - DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _projects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCellProject *cell = [tableView dequeueReusableCellWithIdentifier:@"cellProject"];
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableViewCellProject" owner:self options:nil];
        cell = topLevelObjects[0];
    }

    Project *project = _projects[indexPath.row];

    [cell configureWithProject:project];
    cell.viewController = self;
    cell.delegate = self;

    return cell;
}

#pragma mark - Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [TableViewCellProject defaultHeightCell];
}

- (void)refreshControllChanged {
    [self updateProjects];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _projects.count - 1) {
        if (!_noMoreProjects) {
            _page = @( [_page intValue] + 1 );
            [self updateProjects];
        }
    }
}


//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 44;
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//
//    if (section == 0) {
//        return _categoryTypeView;
//    }
//
//    return nil;
//}


#pragma mark - ProjectCell
- (void)updateProject:(Project *)project {

    for (Project *pj in _projects) {
        if ([pj.objectId isEqualToNumber:project.objectId]) {
            pj.totalLikes = (pj.liked) ? @([pj.totalLikes intValue] - 1) : @([pj.totalLikes intValue] + 1);
            pj.liked = !pj.liked;
            [self.projectsTableView reloadData];
            return;
        }
    }
}

#pragma mark - TypeCategory

//- (void)segmentChanged:(UISegmentedControl *)segmentedControl {
//    [self updateProjects];
//}


- (IBAction)typeProjectChanged:(id)sender {
    [self resetConfigTable];
}
@end
