//
//  CategoriesViewController.h
//  Together
//
//  Created by Talles  Borges  on 11/5/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface CategoriesViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>


#pragma mark - Model
@property Project *project;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UICollectionView *categoriesCollectionView;

@end
