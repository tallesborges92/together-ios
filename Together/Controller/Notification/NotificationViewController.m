//
//  NotificationViewController.m
//  Together
//
//  Created by Talles  Borges  on 11/2/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "Together.h"
#import "User+WebService.h"
#import "TBNotification.h"
#import "UIImageView+WebCache.h"
#import "NSDate+DateTools.h"
#import "TableViewControllerProfile.h"
#import "ViewControllerProject.h"

@interface NotificationViewController ()

@end

@implementation NotificationViewController {
    NSMutableArray *_array;
    UIRefreshControl *_refreshControl;
    NSNumber *_page;
    bool _noMoreProjects;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UITableViewController *tbController = [UITableViewController new];
    tbController.tableView = _notificationsTableView;

    _refreshControl = [UIRefreshControl new];
    [_refreshControl addTarget:self action:@selector(refreshControlChanged) forControlEvents:UIControlEventValueChanged];

    _notificationsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    tbController.refreshControl = _refreshControl;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self resetConfigTable];
}


- (void)resetConfigTable {
    _page = @1;
    _array = [NSMutableArray new];
    _noMoreProjects = false;
    [self updateNotifications];
}


- (void)updateNotifications {
    [_refreshControl beginRefreshing];
    [[Together instance].user notificationsPage:_page WithSuccess:^(NSArray *notifications) {
        _array = [[_array arrayByAddingObjectsFromArray:notifications] mutableCopy];
        if (notifications.count == 0) {
            _noMoreProjects = true;
        }
        [_refreshControl endRefreshing];
        [_notificationsTableView reloadData];
    }                                   failure:^{
    }];
}

- (void)refreshControlChanged {
    [self resetConfigTable];
}

#pragma mark - DataSouce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [self configureBasicCell:cell atIndexPath:indexPath];

    return cell;
}

#pragma mark - Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TBNotification *notification = _array[indexPath.row];

    if ([notification.message isEqualToString:@"like_project"] || [notification.message isEqualToString:@"new_upload"]) {
        Project *project = notification.project;

        ViewControllerProject *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerProject"];
        vc.project = project;

        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        [[Together instance].obTabBarController presentViewController:navigationController animated:YES completion:nil];

    } else if ([notification.message isEqualToString:@"follow"]) {
        TableViewControllerProfile *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
        vc.user = notification.sender;
        [self.navigationController pushViewController:vc animated:YES];
    }

}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _array.count - 1) {
        if (!_noMoreProjects) {
            _page = @( [_page intValue] + 1 );
            [self updateNotifications];
        }
    }
}


#pragma mark - AutoLayoutCell

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

- (void)configureBasicCell:(NotificationTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    TBNotification *notification = _array[indexPath.row];
    cell.timeLabel.text = [notification.creationDate timeAgoSinceDate:notification.serverTime];

    if ([notification.message isEqualToString:@"like_project"]) {
        cell.messageLabel.text = [NSString stringWithFormat:@"%@ %@ %@", notification.sender.name, NSLocalizedString(@"like_project", nil), notification.project.name];
    } else if ([notification.message isEqualToString:@"follow"]) {
        cell.messageLabel.text = [NSString stringWithFormat:@"%@ %@", notification.sender.name, NSLocalizedString(@"follow", nil)];
    } else if ([notification.message isEqualToString:@"new_upload"]) {
        cell.messageLabel.text = [NSString stringWithFormat:@"%@ %@ %@", notification.sender.name, NSLocalizedString(@"send_upload", nil), notification.project.name];
    } else if ([notification.message isEqualToString:@"comment_project"]) {
        cell.messageLabel.text = [NSString stringWithFormat:@"%@ %@ %@", notification.sender.name, NSLocalizedString(@"comment_project", nil), notification.project.name];
    }

    [cell.userImage sd_setImageWithURL:[[NSURL alloc] initWithString:notification.sender.avatarUrl]];
}


@end
