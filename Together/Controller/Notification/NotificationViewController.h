//
//  NotificationViewController.h
//  Together
//
//  Created by Talles  Borges  on 11/2/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *notificationsTableView;

@end
