//
//  LikeViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "LikeViewController.h"
#import "LikeDataSource.h"
#import "Project+Webfaction.h"

@interface LikeViewController ()

@end

@implementation LikeViewController {
    LikeDataSource *_likeDataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _likeDataSource = [LikeDataSource sourceWithArray:[NSArray array] tableView:_likesTableView];

    [_project likesWithSucces:^(NSArray *likes) {
        _likeDataSource.array = likes;
        [_likesTableView reloadData];
    } failure:^{

    }];
}


- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
