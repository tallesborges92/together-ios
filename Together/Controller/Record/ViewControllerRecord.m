//
//  ViewControllerRecord.m
//  Together
//
//  Created by Talles  Borges  on 9/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//


#import <MBProgressHUD/MBProgressHUD.h>
#import "ViewControllerRecord.h"
#import "Together.h"
#import "Upload+Webfaction.h"
#import "Upload+AmazonS3.h"
#import "Upload+ImageGenerator.h"
#import "Upload+AVExport.h"
#import "UploadPreviewViewController.h"

@interface ViewControllerRecord ()

@end

@implementation ViewControllerRecord {
    SCRecorder *_recorder;
}

#pragma mark - UIView
- (void)viewDidLoad
{
    [super viewDidLoad];
    _recorder = [SCRecorder recorder];
    _recorder.sessionPreset = AVCaptureSessionPreset640x480;
    _recorder.audioEnabled = YES;
    _recorder.videoEnabled = YES;
    _recorder.photoEnabled = NO;
    _recorder.delegate = self;
    _recorder.autoSetVideoOrientation = YES;

    _recorder.previewView = _previewView;
    _timeLabel.text = [NSString stringWithFormat:@"%.0f", [_scene.time floatValue]];

    [self prepareCamera];

}

- (BOOL)shouldAutorotate {
    return YES;
}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_recorder previewViewFrameChanged];
}

//
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskLandscape;
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _timeLabel.text = [NSString stringWithFormat:@"%.0f", [_scene.time floatValue]];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];

    [self validInterfaceOrientation];
}

- (void)orientationChanged:(NSNotification *)notification{
    [self validInterfaceOrientation];;
}

-(void) validInterfaceOrientation {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(UIDeviceOrientationIsPortrait(orientation) || orientation ==UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationFaceUp) {
        _turnPhoneImageView.hidden = NO;
        _recordButton.enabled = NO;
    } else {
        _turnPhoneImageView.hidden = YES;
        _recordButton.enabled = YES;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


- (void)prepareCamera {
    [_recorder openSession:^(NSError *sessionError, NSError *audioError, NSError *videoError, NSError *photoError) {
        NSError *error = nil;
        [_recorder setActiveFormatThatSupportsFrameRate:24 width:960 andHeight:540 error:&error];
        _recorder.frameRate = 24;
        NSLog(@"%@", error);

        NSLog(@"==== Opened session ====");
        NSLog(@"Session error: %@", sessionError.description);
        NSLog(@"Audio error : %@", audioError.description);
        NSLog(@"Video error: %@", videoError.description);
        NSLog(@"Photo error: %@", photoError.description);
        NSLog(@"=======================");
        [_recorder startRunningSession];
    }];
}

#pragma mark - Util
- (void)updateTimeLabel {
    CMTime duration = CMTimeMakeWithSeconds([_scene.time floatValue], NSEC_PER_SEC);
    CMTime currentTime = _recorder.recordSession.currentRecordDuration;

    CMTime restanteTime = CMTimeSubtract(duration, currentTime);

    _timeLabel.text = [NSString stringWithFormat:@"%.0f", CMTimeGetSeconds(restanteTime)];
}


#pragma mark - Actions
- (IBAction)recordPressed:(id)sender {
    SCRecordSession *recordSession = [SCRecordSession recordSession];

    recordSession.suggestedMaxRecordDuration = CMTimeMakeWithSeconds([_scene.time floatValue] + 0.1, NSEC_PER_SEC);
    _recorder.recordSession = recordSession;

    [_recorder record];
}

- (IBAction)cancelPressed:(id)sender {
    [_recorder closeSession];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)rotateCameraPressed:(id)sender {
    [_recorder switchCaptureDevices];
}

#pragma mark - SCRecorderDelegate
- (void)recorder:(SCRecorder *)recorder didCompleteRecordSession:(SCRecordSession *)recordSession {
    NSLog(@"didCompleteRecordSession");
    [recorder closeSession];
    recorder.recordSession = nil;
    [recordSession endSession:^(NSError *error) {
        if (error == nil) {
            NSURL *fileUrl = recordSession.outputUrl;

            UploadPreviewViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"previewVC"];
            vc.scene = _scene;
            vc.fileUrl = fileUrl;
            vc.savePreview = ^(SCFilterGroup *filterGroup) {
                Upload *upload = [Upload new];
                upload.scene =_scene;
                upload.user = [Together instance].user;
                upload.videoUrl = [fileUrl absoluteString];
                upload.imageUrl = @"";

                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.labelText = NSLocalizedString(@"sending_video", nil);

                [upload exportWithFilter:filterGroup success:^{
                    [upload saveInWebfactionWithSuccess:^(Upload *uploadSaved) {
                        [upload generateThumbImageAtSecond:1 withSuccess:^{
                            [upload saveImageInAmazonS3WithSuccess:^{
                                [upload updateInWebfactionWithSuccess:^(Upload *uploadSaved) {
                                    [hud hide:YES];
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                } failure:^{
                                    NSLog(@"pau quando foi atualizar");
                                }];
                            } andFailure:^{
                                NSLog(@"pau quando foi salvar imagem");
                            }];
                        } andFailure:^{
                            NSLog(@"pau quando foi gerar thumb");
                        }];
                    } failure:^{
                        NSLog(@"pau quando foi salvar na webfaction");
                        [hud hide:YES];
                    }];
                } andFailure:^{
                    NSLog(@"pau quando foi exportar");
                    [hud hide:YES];
                }];
            };

            vc.cancelPreview = ^{
                [self prepareCamera];
            };

            [self presentViewController:vc animated:YES completion:nil];

        } else {
            // Handle the error
        }
    }];

}

- (void)recorder:(SCRecorder *)recorder didAppendVideoSampleBuffer:(SCRecordSession *)recordSession {
    [self updateTimeLabel];
}

- (void)recorder:(SCRecorder *)recorder didInitializeAudioInRecordSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"error = %@", error);
}

- (void)recorder:(SCRecorder *)recorder didInitializeVideoInRecordSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"error = %@", error);
}


@end
