//
//  ViewControllerRecord.h
//  Together
//
//  Created by Talles  Borges  on 9/18/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <SCRecorder/SCRecorder.h>
#import <UIKit/UIKit.h>
#import "Scene.h"

@interface ViewControllerRecord : UIViewController <SCRecorderDelegate>

#pragma mark - Model
@property Scene *scene;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *turnPhoneImageView;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

#pragma mark - Action
- (IBAction)recordPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;
- (IBAction)rotateCameraPressed:(id)sender;

@end
