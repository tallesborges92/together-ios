//
//  ProjectInfoTableViewController.m
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import "ProjectInfoTableViewController.h"
#import "Project+Webfaction.h"
#import "Together.h"

@interface ProjectInfoTableViewController ()

@end

@implementation ProjectInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _nameCreatorLabel.text = _project.user.name;
    _projectDescriptionLabel.text = _project.descriptionText;
    
}

- (IBAction)deleteProjectPressed:(id)sender {


    if (![[Together instance].user.objectId isEqualToNumber:_project.user.objectId]) {
        UIAlertView *alertView = [UIAlertView new];
        alertView.title = @"Erro";
        alertView.message = @"Só o criador do projeto pode deletar";
        [alertView addButtonWithTitle:@"OK"];
        [alertView show];
        return;
    }

    [_project deleteInWebServiceWithSuccess:^{
        UIAlertView *alertView = [UIAlertView new];
        alertView.title = @"Sucesso";
        alertView.message = @"O projeto foi deletado.";
        [alertView addButtonWithTitle:@"OK"];
        [alertView show];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    }                               failure:^{

    }];

}


@end
