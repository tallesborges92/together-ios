//
//  ViewControllerProject.m
//  Together
//
//  Created by Talles  Borges  on 7/20/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//


#import <MediaPlayer/MediaPlayer.h>
#import "OBTabBarController.h"
#import "SceneDelegate.h"
#import "ViewControllerProject.h"
#import "Together.h"
#import "Audio.h"
#import "Project+Webfaction.h"
#import "MBProgressHUD.h"
#import "Upload+Downloader.h"
#import "Upload+FileUtils.h"
#import "Project+AmazonS3.h"
#import "Project+AVExport.h"
#import "Project+ImageGenerate.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "TableViewControllerText.h"
#import "NewTextViewController.h"
#import "Text.h"
#import "TableViewControllerAudio.h"
#import "NewSceneViewController.h"
#import "SceneViewController.h"
#import "UIAlertView+Utils.h"
#import "NewAudioViewController.h"
#import "Text+Webfaction.h"
#import "Audio+Webfaction.h"
#import "Scene+Webfaction.h"
#import "NewProjectViewController.h"

@interface ViewControllerProject ()

@end

@implementation ViewControllerProject {
    NSArray *_scenes;
    NSArray *_audios;
    NSArray *_texts;
//    MPMoviePlayerController *_playerController;
//    SceneDataSource *_sceneDataSource;
//    SceneDelegate *_sceneDelegate;
//    AudioDataSource *_audioDataSource;
    UIImageView *_imageViewTimeline;
    UIActionSheet *_actionSheetText;
    UIActionSheet *_actionSheetScene;
    UIActionSheet *_actionSheetAudio;
    LMMediaPlayerView *_lmMediaplayerView;
}
#pragma mark - UIViewMethods
- (void)viewDidLoad
{
    [super viewDidLoad];
    _scenes = [NSMutableArray array];
    _audios = [NSMutableArray array];
    _texts =  [NSMutableArray array];

    [_userAvatarImageView sd_setImageWithURL:[NSURL URLWithString:_project.user.avatarUrl]];
    [_userAvatarImageView.layer setMasksToBounds:YES];
    [_userAvatarImageView.layer setCornerRadius:10];
    [_userAvatarImageView.layer setBorderWidth:1];
    [_userAvatarImageView.layer setBorderColor:[UIColor whiteColor].CGColor];

    [_userNameLabel setText:_project.user.name];
    [_categoryLabel setText:_project.category.name];

    [self.navigationItem setTitle:_project.name];

    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"ic_voltar"]];

    _timeLineScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 320);
    _timeLineScrollView.alwaysBounceHorizontal = YES;

    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTapgGestureCaptured:)];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [_timeLineScrollView addGestureRecognizer:singleTap];
    [_timeLineScrollView addGestureRecognizer:longPressGestureRecognizer];


//    _sceneDataSource = [SceneDataSource sourceWithScenes:_scenes collectionView:_collectionViewScenes];
//    _sceneDelegate = [SceneDelegate delegateWithViewController:self dataSource:_sceneDataSource];
//    _collectionViewScenes.delegate = _sceneDelegate;
//
//    _audioDataSource = [AudioDataSource sourceWithAudios:_audios collectionView:_collectionViewAudios];

    //Desativa o botao se nao for quem criou o projeto
//    UIBarButtonItem *buttonItemProjectInfo = [[UIBarButtonItem alloc] initWithTitle:@"?" style:UIBarButtonItemStylePlain target:self action:@selector(projectInfo:)];

    UIImage *imageRightButton;
    if ([self isUserCreator]) {
        imageRightButton = [UIImage imageNamed:@"btn_config_profile"];
    } else {
        imageRightButton = [UIImage imageNamed:@"bt_icon_info"];
        [_sceneButton setImage:[UIImage imageNamed:@"ic_timeline_video"] forState:UIControlStateNormal];
        [_textButton setImage:[UIImage imageNamed:@"ic_timeline_texto"] forState:UIControlStateNormal];
        [_voiceButton setImage:[UIImage imageNamed:@"ic_timeline_voice"] forState:UIControlStateNormal];
        [_musicButton setImage:[UIImage imageNamed:@"ic_timeline_musica"] forState:UIControlStateNormal];
    }
    UIBarButtonItem *buttonItemProjectInfo = [[UIBarButtonItem alloc] initWithImage:imageRightButton style:UIBarButtonItemStylePlain target:self action:@selector(projectInfo:)];

    NSArray *rightButtons = @[buttonItemProjectInfo];
    if (![self isUserCreator]) {
//        UIBarButtonItem *buttonItemGerarVideo = [[UIBarButtonItem alloc] initWithTitle:@"Gerar" style:UIBarButtonItemStylePlain target:self action:@selector(gerarVideo:)];
//        rightButtons = @[buttonItemGerarVideo,buttonItemProjectInfo];
        [_composeButton setHidden:YES];
        [_composeLabel setHidden:YES];
    }

    self.navigationItem.rightBarButtonItems = rightButtons;


    //Imagem fundo blur
    [_imageViewFundoProjeto sd_setImageWithURL:[NSURL URLWithString:_project.imageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_imageViewFundoProjeto setImage:[image applyLightEffect]];
    }];

    // Player
//    _playerController = [[MPMoviePlayerController alloc] init];
//    [_playerController setControlStyle:MPMovieControlStyleEmbedded];
//    [_playerController.view setFrame:_viewPlayer.bounds];
//    [_viewPlayer addSubview:_playerController.view];

    //Create media item with URL.
//    LMMediaItem *item = [[LMMediaItem alloc] initWithInfo:@{
//            LMMediaItemInfoURLKey:[NSURL URLWithString:_project.videoUrl],
//            LMMediaItemInfoContentTypeKey:@(LMMediaItemContentTypeVideo)
//    }];
//
//    [_lmMediaplayerView.mediaPlayer addMedia:item];


    [_imageViewThumb sd_setImageWithURL:[NSURL URLWithString:_project.imageUrl] placeholderImage:[UIImage imageNamed:@"bg_project_video"]];

    //TimeLineScrollView
    [_timeLineScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"photography"]]];
    _timeLineScrollView.delegate = self;

    _imageViewTimeline = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_timeline_timeLine"]];
    _imageViewTimeline.bounds = CGRectMake(0, 0, 2348.5, 15);
    [_timeLineScrollView addSubview:_imageViewTimeline];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!_lmMediaplayerView) {
        _lmMediaplayerView = [LMMediaPlayerView create];
        _lmMediaplayerView.delegate = self;
        _lmMediaplayerView.frame = _viewPlayer.bounds;
        [_viewPlayer addSubview:_lmMediaplayerView];
    }
    _buttonPlay.hidden = [_project.videoUrl isEqualToString:@""];
    if(![_project.videoUrl isEqualToString:@""]) {
        LMMediaItem *item = [[LMMediaItem alloc] initWithInfo:@{
                LMMediaItemInfoURLKey:[NSURL URLWithString:_project.videoUrl],
                LMMediaItemInfoContentTypeKey:@(LMMediaItemContentTypeVideo)
        }];

        [_lmMediaplayerView.mediaPlayer playMedia:item];
        [_lmMediaplayerView.mediaPlayer pause];
    }

    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;

    [_project textsWithSuccess:^(NSArray *texts) {
        _texts = texts;
        _project.texts = _texts;
        [self drawTimeLine];
    } failure:^{

    }];

    [_project audiosWithSuccess:^(NSArray *audios) {
        _audios = audios;
        _project.audios = _audios;
        [self drawTimeLine];
    } failure:^{

    }];

    [MBProgressHUD showHUDAddedTo:self.viewEdicao animated:YES];
    [_project scenesWithSuccess:^(NSArray *scenes) {
        [MBProgressHUD hideHUDForView:self.viewEdicao animated:YES];
        _scenes = scenes;
        _project.scenes = _scenes;
        [self drawTimeLine];
    } failure:^{

    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_lmMediaplayerView.mediaPlayer pause];
}


#pragma mark - Utils
- (void)playProject {
    if (![_project.videoUrl isEqualToString:@""]) {
//        [_playerController setContentURL:[NSURL URLWithString:_project.videoUrl]];
//        [_playerController setScalingMode:MPMovieScalingModeAspectFill];
//        [_playerController play];
        LMMediaItem *item = [[LMMediaItem alloc] initWithInfo:@{
                LMMediaItemInfoURLKey:[NSURL URLWithString:_project.videoUrl],
                LMMediaItemInfoContentTypeKey:@(LMMediaItemContentTypeVideo)
        }];
        [_lmMediaplayerView.mediaPlayer playMedia:item];
    }
}

-(BOOL)isUserCreator {
    User *user = [[Together instance] user];
    return (user && [_project.user.objectId isEqualToNumber:user.objectId]);
}

#pragma mark - Actions
- (IBAction)addScenePressed:(id)sender {
    if ([self isUserCreator]) {
        [self pushCreateScene];
    } else {
        [UIAlertView showAlertWithString:NSLocalizedString(@"need_be_creator", nil)];
    }

}

- (IBAction)addMusicPressed:(id)sender {
    if ([self isUserCreator]) {
        [self presentMediaPickerController];
    } else {
        [UIAlertView showAlertWithString:NSLocalizedString(@"need_be_creator", nil)];
    }
}

- (IBAction)actionPlayProject:(id)sender {
    [self playProject];
    [_buttonPlay setHidden:YES];
    [_imageViewThumb setHidden:YES];
}

- (IBAction)addTextPressed:(id)sender {
    if ([self isUserCreator]) {
        [self presentTextViewController];
    } else {
        [UIAlertView showAlertWithString:NSLocalizedString(@"need_be_creator", nil)];
    }
}

- (IBAction)addAudioPressed:(id)sender {
    if ([self isUserCreator]) {
        [self presentAudioViewController];
    } else {
        [UIAlertView showAlertWithString:NSLocalizedString(@"need_be_creator", nil)];
    }
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)composePressed:(id)sender {
    if ([self isUserCreator]) {
        [self gerarVideo];
    } else {
        [UIAlertView showAlertWithString:NSLocalizedString(@"need_be_creator", nil)];
    }
}

#pragma mark - Utils

-(void) presentAudioViewController {

    NewAudioViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newAudioVC"];
    vc.project = _project;
    vc.start = @(_timeLineScrollView.contentOffset.x / 10);

    [self presentViewController:vc animated:YES completion:nil];

//    TableViewControllerAudio *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"audioVC"];
//    vc.project = _project;
//    vc.start = @(_timeLineScrollView.contentOffset.x / 10);
//
//    [self.navigationController pushViewController:vc animated:YES];
//    [self presentViewController:vc animated:YES completion:nil];
}

-(void) presentTextViewController {
//    TableViewControllerText *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"textVC"];

    NewTextViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newTextVC"];
    vc.project = _project;
    vc.start = @(_timeLineScrollView.contentOffset.x / 10);

    [self presentViewController:vc animated:YES completion:nil];

//    [self.navigationController pushViewController:vc animated:YES];
}

-(void) presentMediaPickerController {
    MPMediaPickerController *vc = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
    vc.showsCloudItems = NO;
    vc.delegate = self;

    [self presentViewController:vc animated:YES completion:^{
        NSLog(@"here we go!");
    }];
}

- (void)pushCreateScene {
//    TableViewControllerCreateScene *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerCreateScene"];
//    vc.project = _project;
//    vc.scenes = _scenes;
//    [self.navigationController pushViewController:vc animated:YES];
    NewSceneViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newSceneVC"];
    vc.project = _project;
    vc.scenes = _scenes;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)projectInfo:(id)projectInfo {
    NewProjectViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newVideoVC"];
//    ProjectInfoTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"projectInfoVC"];
    vc.project = _project;
    vc.popViewBackAction = true;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)gerarVideo{
    // Verifica se tem cenas criada
    if (_scenes.count == 0) {
        NSString *msgErro = [NSString stringWithFormat:NSLocalizedString(@"first_create_scene", nil)];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msgErro delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }

    // Verifica se todas as cenas tem videos baixados
//    for (Scene *scene in _scenes) {
//        if (!scene.upload) {
//            NSString *msgErro = [NSString stringWithFormat:@"Selecione um video para a cena %@.", scene.name];
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção" message:msgErro delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//            return;
//        }
//    }

    // Baixa os videos selecionados
//    NSMutableArray *uploads = [NSMutableArray new];


    if (![_project canExport]) {
        [self downloadScenes];
        return;
    }

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    for (Scene *scene in _scenes) {
//        if(![scene.upload uploadFileExists]) {
//            hud.labelText = [NSString stringWithFormat:@"Baixando %@", scene.name];
//            [scene.upload downloadWithSuccess:^{
//                [hud hide:YES];
//            }                      andFailure:^{
//                [hud hide:YES];
//            }];
//            return;
//        }
//        [uploads addObject:scene.upload];
//    }

//    if(![_project localAudioExist]) {
//        NSString *msgErro = @"Adicione uma musica";
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção" message:msgErro delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//    }


    hud.labelText = NSLocalizedString(@"composing_video", nil);
    // TODO: Refatorar para apenas 1 metodo
    [_project exportWithSuccess:^{ // Novo metodo export
        [_project generateThumbImageAtSecond:2 withSuccess:^{
            [_project saveImageInAmazonS3WithSuccess:^{
                hud.labelText = NSLocalizedString(@"uploading_video", nil);
                [_project saveVideoInAmazonS3WithSuccess:^() {
                    [_project updateInWebfactionWithSuccess:^(Project *projectUpdated) {
                        _project = projectUpdated;
                        _lmMediaplayerView = nil;
                        [hud hide:YES];
                        [self viewWillAppear:YES];
                        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"success", nil) message:NSLocalizedString(@"project_updated", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }                               failure:^{
                        NSLog(@"Falha ao atualizar projeto");
                        [hud hide:YES];
                    }];
                }                             andFailure:^{
                    NSLog(@"Falha ao fazer upload do video");
                    [hud hide:YES];
                }];
            } andFailure:^{
                NSLog(@"Falha ao fazer upload da imagem");
                [hud hide:YES];
            }];
        } andFailure:^{
            NSLog(@"Falha ao fazer salvar imagem");
            [hud hide:YES];
        }];
    }                   failure:^{
        NSLog(@"falha ao exportar");
        [hud hide:YES];
    }];
}

- (void)downloadScenes {
    for (int i = 0; i < _scenes.count; i++) {
        Scene *scene = _scenes[i];
        if (scene.upload) {
            if(![scene.upload uploadFileExists]) {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.labelText = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"downloading", nil), scene.name];
                [scene.upload downloadWithSuccess:^{
                    [hud hide:YES];
                    [self downloadScenes];

                }                      andFailure:^{
                }];
                return;
            }
        }
    }
    [self gerarVideo];
}

#pragma mark - MPMediaPickerControllerDelegate
- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker {
    [mediaPicker dismissViewControllerAnimated:YES completion:^{

    }];
}

- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection {
    [mediaPicker dismissViewControllerAnimated:YES completion:^{
        MPMediaItem *mediaItem = mediaItemCollection.items[0];

        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = NSLocalizedString(@"importing_music", nil);
        [_project exportMediaItem:mediaItem withSuccess:^(NSURL *audioUrl) {
            hud.labelText = NSLocalizedString(@"uploading_music", nil);
            [_project saveAudioInAmazonS3WithSuccess:^{
                hud.labelText = NSLocalizedString(@"updating_project", nil);
                [_project updateInWebfactionWithSuccess:^(Project *projectUpdated) {
                    _project = projectUpdated;
                    [self viewWillAppear:YES];
                    [hud hide:YES];
                } failure:^{
                    NSLog(@"falha ao atualizar projeto");
                    [hud hide:YES];
                }];
            } andFailure:^{
                [hud hide:YES];
                NSLog(@"falha ao salvar audi na amazon");
            }];
        } andFailure:^{
            [hud hide:YES];
            NSLog(@"falha ao exportar audio");
        }];
    }];
}


#pragma mark - timeLineScrollView

- (void)longTapgGestureCaptured:(UILongPressGestureRecognizer *)longTapgGestureCaptured {
    CGPoint touchPoint=[longTapgGestureCaptured locationInView:_timeLineScrollView];

    if (![_project.user.objectId isEqualToNumber:[Together instance].user.objectId]) {
        return;
    }

    [_scenes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Scene *scene = obj;
        if(CGRectContainsPoint([self rectiWithStart:scene.start time:scene.time y:@(17)], touchPoint)) {

            if (longTapgGestureCaptured.state == UIGestureRecognizerStateBegan) {
                if (!_actionSheetScene || !_actionSheetScene.isVisible) {
                    _actionSheetScene = [[UIActionSheet alloc] initWithTitle:nil
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                                      destructiveButtonTitle:NSLocalizedString(@"delete_scene", nil)
                                                           otherButtonTitles:NSLocalizedString(@"edit_scene", nil), NSLocalizedString(@"clone_scene", nil), nil];
                    _actionSheetScene.tag = idx;
                    [_actionSheetScene showInView:self.view];
                }
            }
            *stop = true;
        }
    }];

    [_texts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Text *text = obj;
        if(CGRectContainsPoint([self rectiWithStart:text.start time:text.time y:@(80)], touchPoint)) {

            if (longTapgGestureCaptured.state == UIGestureRecognizerStateBegan) {
                if (!_actionSheetText || !_actionSheetText.isVisible) {
                    _actionSheetText = [[UIActionSheet alloc] initWithTitle:nil
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                                     destructiveButtonTitle:NSLocalizedString(@"delete_text", nil)
                                                          otherButtonTitles:nil];
                    _actionSheetText.tag = idx;
                    [_actionSheetText showInView:self.view];
                }
            }
            *stop = true;
        }
    }];

    [_audios enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Audio *audio = obj;
        if(CGRectContainsPoint([self rectiWithStart:audio.start time:audio.time y:@(144)], touchPoint)) {
            if (longTapgGestureCaptured.state == UIGestureRecognizerStateBegan) {
                if (!_actionSheetAudio || !_actionSheetAudio.isVisible) {
                    _actionSheetAudio = [[UIActionSheet alloc] initWithTitle:nil
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                                      destructiveButtonTitle:NSLocalizedString(@"delete_audio", nil)
                                                           otherButtonTitles:nil];
                    _actionSheetAudio.tag = idx;
                    [_actionSheetAudio showInView:self.view];
                }
            }
            *stop = true;
        }
    }];

}


- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture {
    CGPoint touchPoint=[gesture locationInView:_timeLineScrollView];

    for (Scene *scene in _scenes) {
        if (CGRectContainsPoint([self rectiWithStart:scene.start time:scene.time y:@(20)], touchPoint)) {

            SceneViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewSceneVC"];
            vc.scene = scene;

            [self presentViewController:vc animated:YES completion:nil];

//            ViewControllerScene *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewControllerScene"];
//            vc.scene = scene;
//
//            [self.navigationController pushViewController:vc animated:YES];

            break;
        }
    }
}

-(CGRect) rectiWithStart:(NSNumber *) start time:(NSNumber *) time y:(NSNumber *) y {
    CGRect cgRect = CGRectMake([start floatValue] * 10 + TIME_LINE_STEP, [y floatValue], [time floatValue] * 10, 40);

    return cgRect;
}

-(CGSize) tamanhoScrollView {
    NSNumber *number = @(0);
    for (Scene *s in _scenes) {
        number = @(([s.time floatValue] * 10) + [number floatValue]);
    }
    return CGSizeMake([number floatValue], 252);
}

-(void) drawScenes {
    for (Scene *cena in _scenes) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:0.980f green:0.737f blue:0.071f alpha:0.5f];
        view.layer.borderColor = [UIColor colorWithRed:0.894f green:0.000f blue:0.000f alpha:1.00f].CGColor;
        view.layer.borderWidth = 2.0;
        view.layer.cornerRadius = 8.0;
        [view.layer setMasksToBounds:YES];
        [view setClipsToBounds:YES];

        view.frame = [self rectiWithStart:cena.start time:cena.time y:@(17)];

        [_timeLineScrollView addSubview:view];

        UIImageView *imageScene = [[UIImageView alloc] initWithFrame:CGRectInset(view.bounds, 0, 0)];
        imageScene.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:imageScene];
//        [imageScene sd_setImageWithURL:[[NSURL alloc] initWithString:cena.imageUrl]];
        NSString *imageSceneUrl = @"bkg_scene";
        if (cena.upload) {
            imageSceneUrl = cena.upload.imageUrl;
        }

        [imageScene sd_setImageWithURL:[[NSURL alloc] initWithString:imageSceneUrl] placeholderImage:[UIImage imageNamed:@"bkg_scene"] options:SDWebImageRefreshCached];
    }
}

-(void) drawTexts {
    for (Text *text in _texts) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:1.000f green:0.533f blue:0.173f alpha:0.5f];
        view.frame = [self rectiWithStart:text.start time:text.time y:@(80)];
        view.layer.borderColor = [UIColor colorWithRed:1.000f green:0.533f blue:0.173f alpha:1.0f].CGColor;
        view.layer.borderWidth = 2.0;
        view.layer.cornerRadius = 8.0;

        UILabel *label = [UILabel new];
        label.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:12];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        [label setText:text.text];
        label.frame = CGRectInset(view.bounds, 0, 0);

        [view addSubview:label];

        [_timeLineScrollView addSubview:view];
    }
}

-(void) drawAudios{
    for (Audio *audio in _audios) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:0.894f green:0.000f blue:0.000f alpha:0.5f] ;
        view.frame = [self rectiWithStart:audio.start time:audio.time y:@(144)];
        view.layer.borderColor = [UIColor colorWithRed:0.894f green:0.000f blue:0.000f alpha:1.00f].CGColor;
        view.layer.borderWidth = 2.0;
        view.layer.cornerRadius = 8.0;

        [_timeLineScrollView addSubview:view];
    }
}

-(void) drawMusic{
    if (![_project.audioUrl isEqualToString:@""]) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:0.000f green:0.694f blue:0.773f alpha:0.5f];
        view.frame = [self rectiWithStart:@(0) time:@([self tamanhoScrollView].width / 10) y:@(204)];
        view.layer.borderColor = [UIColor colorWithRed:0.000f green:0.694f blue:0.773f alpha:1.00f].CGColor;
        view.layer.borderWidth = 2.0;
        view.layer.cornerRadius = 8.0;
        [_timeLineScrollView addSubview:view];
    }
}


-(void) drawTimeLine {
    [_timeLineScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    [self drawScenes];
    [self drawMusic];
    [self drawTexts];
    [self drawAudios];
    [_timeLineScrollView addSubview:_imageViewTimeline];
    _timeLineScrollView.contentSize = [self tamanhoScrollView];
}

#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (actionSheet == _actionSheetText) {
        if (buttonIndex == 0) {
            Text *text = _texts[_actionSheetText.tag];
            [text deleteInWebfactionWithSuccess:^{
                [self viewWillAppear:YES];
            }                           failure:^{

            }];
        }
        return;
    }

    if(actionSheet == _actionSheetScene) {
        if (buttonIndex == 0) {
            Scene *scene = _scenes[_actionSheetScene.tag];
            [scene deleteInWebfactionWithSuccess:^{
                [self viewWillAppear:YES];
            } failure:^{

            }];
        }else if (buttonIndex == 1) {
            Scene *scene = _scenes[_actionSheetScene.tag];
            NewSceneViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"newSceneVC"];
            vc.scene = scene;

            [self presentViewController:vc animated:YES completion:nil];
        }else if (buttonIndex == 2) {
            Scene *sceneSelected = _scenes[_actionSheetScene.tag];
            Scene *scene = [Scene new];
            scene.project = sceneSelected.project;
            scene.name = sceneSelected.name;
            scene.descriptionText = sceneSelected.descriptionText;
            scene.imageUrl = sceneSelected.imageUrl;
            scene.time = sceneSelected.time;
            NSNumber *newStart = @(0);
            for (Scene *c in _scenes) {
                NSNumber *inicio = c.start;
                NSNumber *duracao = c.time;

                if ([inicio floatValue] + [duracao floatValue] > [newStart floatValue]) {
                    newStart = @([inicio floatValue] + [duracao floatValue]);
                }
            }
            scene.start = newStart;
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [scene saveInWebfactionSuccess:^(Scene *sceneSaved) {
                [UIAlertView showAlertWithString:NSLocalizedString(@"scene_created", nil)];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self viewWillAppear:YES];
            } failure:^{
                [UIAlertView showAlertWithString:NSLocalizedString(@"error_creating_scene", nil)];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
        return;
    }

    if (actionSheet == _actionSheetAudio) {
        if (buttonIndex == 0) {
            Audio *audio = _audios[_actionSheetAudio.tag];
            [audio deleteInWebfactionWithSuccess:^{
                [self viewWillAppear:YES];
            }                            failure:^{

            }];
        }
        return;
    }
}


#pragma mark - LMMediaPlayerDelegate

- (BOOL)mediaPlayerViewWillStartPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media {
    return YES;
}

- (void)mediaPlayerViewDidChangeCurrentTime:(LMMediaPlayerView *)playerView {
    if (_timeLineScrollView.panGestureRecognizer.state != UIGestureRecognizerStateChanged) {
        CGPoint cgPoint = _timeLineScrollView.contentOffset;
        [_timeLineScrollView setContentOffset:CGPointMake(@(playerView.mediaPlayer.currentTimeSeconds * 10).floatValue, cgPoint.y) animated:YES];
    }
}

- (void)mediaPlayerViewWillChangeState:(LMMediaPlayerView *)playerView state:(LMMediaPlaybackState)state {
    NSLog(@"state = %d", state);
}

- (void)mediaPlayerViewDidChangeFullscreenMode:(BOOL)fullscreen {
    NSLog(@"fullscreen = %d", fullscreen);
    if(!fullscreen) _lmMediaplayerView.frame = _viewPlayer.bounds;
}

- (void)mediaPlayerViewDidFinishPlaying:(LMMediaPlayerView *)playerView media:(LMMediaItem *)media {
    NSLog(@"media = %@", media);
    [[playerView mediaPlayer] pause];
    [[playerView mediaPlayer] seekTo:0];
}


#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    switch (scrollView.panGestureRecognizer.state) {
        case UIGestureRecognizerStatePossible:
            break;
        case UIGestureRecognizerStateBegan:
            [_lmMediaplayerView.mediaPlayer pause];
            break;
        case UIGestureRecognizerStateChanged: {
            if (![_project.videoUrl isEqualToString:@""]) {
                if (!_buttonPlay.hidden) {
                    [self actionPlayProject:nil];
                }
                if (_lmMediaplayerView.mediaPlayer.playbackState == LMMediaPlaybackStatePlaying) [_lmMediaplayerView.mediaPlayer pause];
                double d = _timeLineScrollView.contentOffset.x / 10.0;
                [_lmMediaplayerView.mediaPlayer seekTo:d];
            }
            break;
        }
        case UIGestureRecognizerStateEnded:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}

#pragma mark - Rotation

- (void)mediaPlayerViewWillChangeFullscreenMode:(BOOL)fullscreen {
    NSNumber *value = @(UIInterfaceOrientationPortrait);

    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}



#pragma mark - Other

- (void)dealloc {
    NSLog(@"Dealloc View Controller Project");
}


@end
