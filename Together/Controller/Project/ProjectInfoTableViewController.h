//
//  ProjectInfoTableViewController.h
//  Together
//
//  Created by Talles  Borges  on 9/19/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"

@interface ProjectInfoTableViewController : UITableViewController

#pragma mark - Model
@property Project *project;

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UILabel *projectDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameCreatorLabel;

#pragma mark - Actions
- (IBAction)deleteProjectPressed:(id)sender;


@end
