//
//  ViewControllerProject.h
//  Together
//
//  Created by Talles  Borges  on 7/20/14.
//  Copyright (c) 2014 Talles  Borges . All rights reserved.
//


#import <MediaPlayer/MediaPlayer.h>
#import <LMMediaPlayer/LMMediaPlayerView.h>
#import "Project.h"

static const float TIME_LINE_STEP = 160.0;

@interface ViewControllerProject : UIViewController<MPMediaPickerControllerDelegate, UIActionSheetDelegate, LMMediaPlayerViewDelegate, UIScrollViewDelegate>

#pragma mark - Actions
-(IBAction)addScenePressed:(id)sender;
-(IBAction)addMusicPressed:(id)sender;
-(IBAction)actionPlayProject:(id)sender;
- (IBAction)addTextPressed:(id)sender;
- (IBAction)addAudioPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)composePressed:(id)sender;


#pragma mark - Buttons
@property (weak, nonatomic) IBOutlet UIButton *sceneButton;
@property (weak, nonatomic) IBOutlet UIButton *musicButton;
@property (weak, nonatomic) IBOutlet UIButton *textButton;
@property (weak, nonatomic) IBOutlet UIButton *voiceButton;

#pragma mark - UIView
@property (weak, nonatomic) IBOutlet UIView *viewPlayer;
@property (weak, nonatomic) IBOutlet UIView *viewEdicao;

#pragma mark - UIButton
@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;
@property (weak, nonatomic) IBOutlet UIButton *composeButton;


#pragma mark - ImageView
@property (weak, nonatomic) IBOutlet UIImageView *imageViewFundoProjeto;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewThumb;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatarImageView;

#pragma mark - Label
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *composeLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

#pragma mark - Model
@property Project *project;

#pragma mar - UICollectionView
@property (weak, nonatomic) IBOutlet UIScrollView *timeLineScrollView;

@end
